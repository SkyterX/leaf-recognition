﻿using System.Runtime.Serialization;
using Emgu.CV.Structure;
using LeafRecognition.Recognition;
using LeafRecognition.Util;

namespace LeafRecognition.ImageCharacteristics
{
	[DataContract(Namespace = "LeafRecognition")]
	[Version(2)]
	public class BorderHuMoments : Characteristic<double[]>
	{
		public override string Description
		{
			get { return "Hu invariants of the leaf border."; }
		}

		protected override double[] CreateValue(ImageInfo imageInfo)
		{
			var shape = imageInfo.LeafBorder.Value;
			var moments = shape.Image
				.Convert<Gray, byte>()
				.Not()
				.GetMoments(true);
			var h = moments.GetHuMoment();
			return new[] { h.hu1, h.hu2, h.hu3, h.hu4, h.hu5, h.hu6, h.hu7 };
		}
	}
}