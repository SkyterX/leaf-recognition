﻿using System.Drawing;
using System.Runtime.Serialization;
using LeafRecognition.Algorithms;
using LeafRecognition.Recognition;
using LeafRecognition.Util;

namespace LeafRecognition.ImageCharacteristics
{
	[Version(0)]
	[DataContract(Namespace = "LeafRecognition")]
	public class FitEllipseSize : Characteristic<SizeF>
	{
		public override string Description
		{
			get { return "Size of an ellipse fitted to the convex hull."; }
		}

		protected override SizeF CreateValue(ImageInfo imageInfo)
		{
			return FitEllipse.Instance.Apply(imageInfo.ConvexHull.Value).size;
		}
	}
}