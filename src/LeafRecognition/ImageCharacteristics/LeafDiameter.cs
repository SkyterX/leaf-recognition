﻿using System.Runtime.Serialization;
using LeafRecognition.Recognition;
using LeafRecognition.Util;

namespace LeafRecognition.ImageCharacteristics
{
	[Version(0)]
	[DataContract(Namespace = "LeafRecognition")]
	public class LeafDiameter : Characteristic<double>
	{
		public override string Description
		{
			get { return "Leaf diameter"; }
		}

		protected override double CreateValue(ImageInfo imageInfo)
		{
			return imageInfo.ConvexHull.Value.GetDiameter();
		}
	}
}