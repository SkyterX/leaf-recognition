﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using LeafRecognition.CssInvestivation;
using LeafRecognition.Recognition;
using LeafRecognition.Util;


namespace LeafRecognition.ImageCharacteristics
{
    [DataContract(Namespace = "LeafRecognition")]
    [Version(1)]
    public class CssFeatures : Characteristic<double[]>
    {
        private static readonly MCvScalar white = new Bgr(Color.White).MCvScalar;

        public override string Description
        {
            get { return "All CSS features"; }
        }

        protected override double[] CreateValue(ImageInfo imageInfo)
        {
            var t1 = DateTime.Now;
            List<double> featureValues = new List<double>();

            var img = imageInfo.LeafShapeWithoutStem.Value.Image
                .Convert<Gray, byte>()
                .Resize(0.3, INTER.CV_INTER_CUBIC); 
            var halftonedImage = new Image<Gray, byte>(img.Size + new Size(2, 2));
            CvInvoke.cvCopyMakeBorder(img, halftonedImage, new Point(1, 1),
                BORDER_TYPE.CONSTANT, white); // for edge cases
            
            var contourFinder = new ContourFinder(halftonedImage);
            var t2 = DateTime.Now;
            contourFinder.ChooseTheFirstContour();
            var contourPoints = contourFinder.ChosedContourPoints();
            var t3 = DateTime.Now;

            var cssCreator = new CssCreator(CssParameters.DefaultCssParameters, contourPoints);
            cssCreator.CreateCss();
            var t4 = DateTime.Now;
            cssCreator.FilterCriticalPoints();
            HashSet<Point>[] criticalPointsArray = cssCreator.CriticalPointsArray;
            foreach (var critPoints in criticalPointsArray)
            {
                var scales = critPoints.Select(p => (double)p.Y);
                featureValues.AddRange(scales.LogHistogam());
            }
            var t5 = DateTime.Now;
            Console.WriteLine("whole {0}", (t5-t1).Seconds);
            Console.WriteLine("convert {0}", (t2-t1).Seconds);
            Console.WriteLine("contour {0}", (t3-t2).Seconds);
            Console.WriteLine("css {0}", (t4-t3).Seconds);
            Console.WriteLine("-----------------");
            return featureValues.ToArray();
        }
    }
}
