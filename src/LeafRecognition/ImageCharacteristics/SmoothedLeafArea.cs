﻿using System.Runtime.Serialization;
using LeafRecognition.Algorithms;
using LeafRecognition.Filters;
using LeafRecognition.Recognition;
using LeafRecognition.Util;

namespace LeafRecognition.ImageCharacteristics
{
	[Version(1)]
	[DataContract(Namespace = "LeafRecognition")]
	public class SmoothedLeafArea : Characteristic<double>
	{
		public override string Description
		{
			get { return "Smoothed leaf area"; }
		}

		protected override double CreateValue(ImageInfo imageInfo)
		{
			return imageInfo.LeafShapeWithoutStem.Value
				.Apply(MeanFilter.Default)
				.Apply(LeafShapeGetter.Instance)
				.CountBlack();
		}
	}
}