﻿using System.Runtime.Serialization;
using LeafRecognition.Recognition;
using LeafRecognition.Util;

namespace LeafRecognition.ImageCharacteristics
{
	[Version(0)]
	[DataContract(Namespace = "LeafRecognition")]
	public class ConvexHullArea : Characteristic<double>
	{
		public override string Description
		{
			get { return "Convex Hull Area"; }
		}

		protected override double CreateValue(ImageInfo imageInfo)
		{
			return imageInfo.ConvexHull.Value.Area();
		}
	}
}