﻿using System.Runtime.Serialization;
using LeafRecognition.Algorithms;
using LeafRecognition.Recognition;
using LeafRecognition.Util;

namespace LeafRecognition.ImageCharacteristics
{
	[Version(0)]
	[DataContract(Namespace = "LeafRecognition")]
	public class LeafArea : Characteristic<double>
	{
		public override string Description
		{
			get { return "Leaf Area"; }
		}

		protected override double CreateValue(ImageInfo imageInfo)
		{
			return imageInfo.LeafShapeWithoutStem.Value.CountBlack();
		}
	}
}