﻿using System.Runtime.Serialization;
using LeafRecognition.Recognition;
using LeafRecognition.Util;

namespace LeafRecognition.ImageCharacteristics
{
	[DataContract(Namespace = "LeafRecognition")]
	public abstract class Characteristic<TValue> : IDescribable
	{
		private object createLock = new object();
		[DataMember] private Boxed boxedValue;
		protected ImageInfo ImageInfo;

		public abstract string Description { get; }
		protected abstract TValue CreateValue(ImageInfo imageInfo);

		public void Initialize(ImageInfo imageInfo)
		{
			ImageInfo = imageInfo;
		}

		public TValue Value
		{
			get
			{
				if (boxedValue == null)
				{
					lock (createLock)
					{
						if(boxedValue == null)
							boxedValue = new Boxed(CreateValue(ImageInfo));
					}
				}
				return boxedValue.Value;
			}
			set
			{
				lock (createLock)
				{
					boxedValue = new Boxed(value);
				}
			}
		}

		#region Serialization

		[DataMember] private int version;

		[OnSerializing]
		private void OnSerializing(StreamingContext context)
		{
			version = GetType().GetVersion();
		}

		[OnDeserialized]
		private void OnDeserialized(StreamingContext context)
		{
			if (version != GetType().GetVersion())
				boxedValue = null;
			createLock = new object();
		}

		[DataContract(Namespace = "LeafRecognition")]
		private class Boxed
		{
			[DataMember] public readonly TValue Value;

			public Boxed(TValue value)
			{
				Value = value;
			}
		}

		#endregion
	}
}