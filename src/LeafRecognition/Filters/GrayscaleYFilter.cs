using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Emgu.CV;
using Emgu.CV.Structure;
using LeafRecognition.Images;

namespace LeafRecognition.Filters
{
    public class GrayscaleYFilter : IFilter
    {
        private const double RedCoefficient = 0.229;
        private const double GreenCoefficient = 0.587;
        private const double BlueCoefficient = 0.114;
        public static readonly GrayscaleYFilter Default = new GrayscaleYFilter(2);

        protected GrayscaleYFilter(int parallelParts)
        {
            ParallelParts = parallelParts;
        }

        public static int ParallelParts { get; set; }

        public RgbImage Apply(RgbImage image)
        {
            RgbImage result = image.Clone();
            return result.Image.Convert<Gray, byte>().Convert<Rgb, byte>().ToRgbImage();

            var tasks = new List<Task>();
            int dw = image.Width/ParallelParts;
            int dh = image.Height/ParallelParts;
            for (int i = 0; i < ParallelParts; ++i)
            {
                for (int j = 0; j < ParallelParts; ++j)
                {
                    int x = i;
                    int y = j;
                    tasks.Add(
                        Task.Factory.StartNew(() => ThreadProc(dw*x, dh*y, dw*(x + 1), dh*(y + 1), image, result)));
                }
            }
            Task.WaitAll(tasks.ToArray());
            return result;
        }

		private static Rgb PixelToGrayScale(Rgb color)
        {
            var brightness = (byte) (Convert.ToByte(color.Red*RedCoefficient) +
                                     Convert.ToByte(color.Green*GreenCoefficient) +
                                     Convert.ToByte(color.Blue*BlueCoefficient));
			return new Rgb(brightness, brightness, brightness);
        }

        private static void ThreadProc(int xl, int yl, int xr, int yr, RgbImage sourceData, RgbImage result)
        {
            for (int i = xl; i < xr; ++i)
            {
                for (int j = yl; j < yr; ++j)
                {
                    result.SetPixel(i, j, PixelToGrayScale(sourceData.GetPixel(i, j)));
                }
            }
        }
    }
}