using LeafRecognition.Images;

namespace LeafRecognition.Filters
{
	public interface IFilter
	{
		RgbImage Apply(RgbImage image);
	}

	public static class FilterExtensions
	{
		public static RgbImage Apply(this RgbImage image, IFilter filter)
		{
			return filter.Apply(image);
		}
	}
}