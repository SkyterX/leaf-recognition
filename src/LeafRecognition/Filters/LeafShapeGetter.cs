﻿using LeafRecognition.Images;

namespace LeafRecognition.Filters
{
	public class LeafShapeGetter : IFilter
	{
		public static readonly LeafShapeGetter Instance = new LeafShapeGetter();

		private LeafShapeGetter()
		{
		}

		public RgbImage Apply(RgbImage image)
		{
//            return image
//                .Apply(GrayscaleYFilter.Default)
//                .Apply(Binarization.Default)
//                .Apply(LargerComponentOnly.MaxArea)
//                .Apply(Inverse.Default)
//                .Apply(Dilation.Default)
//                .Apply(LargerComponentOnly.Simple)
//                .Apply(Erosion.Default)
//                .Apply(Inverse.Default);
			return image
				.Apply(FloodFillBinarization.Instance)
				.Apply(FillLargestComponent.MaxArea);
		}
	}
}