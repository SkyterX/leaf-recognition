using System;
using System.Drawing;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using LeafRecognition.Images;

namespace LeafRecognition.Filters
{
	public class FillLargestComponent : IFilter
	{
		public const double CornerToSizeRatio = 0.01;
		public const int MaxCornerSize = 25;
		private static readonly Rgb TempFill = new Rgb(128, 128, 128);
		private static readonly Rgb Threshold = new Rgb(100, 100, 100);
		
		private readonly bool cornerRestricted;
		private readonly Func<MCvConnectedComp, double> criteria;

		public static readonly FillLargestComponent MaxArea = new FillLargestComponent(comp => comp.area, true);

		public FillLargestComponent(Func<MCvConnectedComp, double> criteria, bool cornerRestricted = false)
		{
			this.cornerRestricted = cornerRestricted;
			this.criteria = criteria;
		}

		public RgbImage Apply(RgbImage image)
		{
			var result = image.Clone();

			if (cornerRestricted)
				ClearCorners(result);

			double largestComponent = -1;
			int componentX = 0;
			int componentY = 0;

			for (var y = 0; y < result.Height; y++)
			{
				for (var x = 0; x < result.Width; x++)
				{
					if (!result.IsBlack(x, y)) continue;
					var component = result.Image.FillComponent(x, y, TempFill);
					var size = criteria(component);
					if (size > largestComponent)
					{
						largestComponent = size;
						componentX = x;
						componentY = y;
					}
				}
			}

			if (largestComponent >= 0)
				result.Image.FillComponent(componentX, componentY, RgbColors.Black);
			result.Image._ThresholdBinary(Threshold, RgbColors.White);
			return result;
		}

		private void ClearCorners(RgbImage image)
		{
			int w = GetCornerSize(image.Width);
			int h = GetCornerSize(image.Height);
			var corners = new[]
			{
				new Rectangle(0, 0, w, h),
				new Rectangle(image.Width - w, 0, w, h),
				new Rectangle(0, image.Height - h, w, h),
				new Rectangle(image.Width - w, image.Height - h, w, h)
			};
			foreach (var cornerRect in corners)
			{
				var corner = image.Image.GetSubRect(cornerRect);
				corner.SetValue(RgbColors.Black);
				corner.FillComponent(w / 2, h / 2, RgbColors.White);
			}
		}

		private int GetCornerSize(int size)
		{
			size = (int) (size*CornerToSizeRatio);
			return size > MaxCornerSize ? MaxCornerSize : Math.Max(size, 1);
		}
	}
}