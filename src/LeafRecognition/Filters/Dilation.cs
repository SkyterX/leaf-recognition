﻿using LeafRecognition.Images;

namespace LeafRecognition.Filters
{
	public class Dilation : IFilter
	{
		public static readonly Dilation Default = new Dilation(7);
		private readonly int size;

		public Dilation(int size)
		{
			this.size = size;
		}

		public RgbImage Apply(RgbImage image)
		{
			var result = image.Image.Clone().ToRgbImage();
			result.Image._Dilate(2);
			return result;

			for (var x = 0; x < image.Width; ++x)
			{
				for (var y = 0; y < image.Height; ++y)
				{
					result.SetPixel(x, y, RgbColors.White);
				}
			}

			for (var x = 0; x < image.Width; ++x)
			{
				for (var y = 0; y < image.Height; ++y)
				{
					if (image.IsWhite(x, y))
						continue;

					for (var i = -size/2; i <= size/2; ++i)
					{
						for (var j = -size/2; j <= size/2; ++j)
						{
							if (!image.Contains(x + i, y + j))
								continue;
							result.SetPixel(x + i, y + j, RgbColors.Black);
						}
					}
				}
			}
			return result;
		}
	}
}