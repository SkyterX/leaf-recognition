﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LeafRecognition.Images;

namespace LeafRecognition.Filters
{
	public class Inverse : IFilter
	{
		public static readonly Inverse Default = new Inverse(2);

		public Inverse(int parallelParts)
		{
			ParallelParts = parallelParts;
		}

		public static int ParallelParts { get; private set; }

		public RgbImage Apply(RgbImage image)
		{
			var result = image.Clone();
			result.Image._Not();
			return result;

			var tasks = new List<Task>();
			var dw = image.Width/ParallelParts;
			var dh = image.Height/ParallelParts;
			for (var i = 0; i < ParallelParts; ++i)
			{
				for (var j = 0; j < ParallelParts; ++j)
				{
					var x = i;
					var y = j;
					tasks.Add(
						Task.Factory.StartNew(() => ThreadProc(dw*x, dh*y, dw*(x + 1), dh*(y + 1), image, result)));
				}
			}
			Task.WaitAll(tasks.ToArray());

			return result;
		}

		private void ThreadProc(int xl, int yl, int xr, int yr, RgbImage bitmap, RgbImage result)
		{
			for (var i = xl; i < xr; ++i)
			{
				for (var j = yl; j < yr; ++j)
				{
					result.SetPixel(i, j,
						bitmap.GetPixel(i, j).Brightness() == 0
							? RgbColors.White
							: RgbColors.Black);
				}
			}
		}
	}
}