﻿using LeafRecognition.Images;

namespace LeafRecognition.Filters
{
	public class LeafBorderGetter : IFilter
	{
		public static readonly LeafBorderGetter Instance = new LeafBorderGetter();

		private LeafBorderGetter()
		{
		}

		public RgbImage Apply(RgbImage bitmap)
		{
			return bitmap
				.Apply(LeafShapeGetter.Instance)
				.Apply(SimpleEdgeDetection.Default);
		}
	}
}