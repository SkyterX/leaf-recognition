﻿using Emgu.CV.Structure;
using LeafRecognition.Images;

namespace LeafRecognition.Filters
{
	public class MeanFilter : IFilter
	{
		public static MeanFilter Default = new MeanFilter(31);

		public MeanFilter(int threshold)
		{
			Threshold = threshold;
		}

		public static int Threshold { get; private set; }

		public RgbImage Apply(RgbImage image)
		{
			var result = image.Clone();

			return result.Image.SmoothMedian(Threshold).ToRgbImage();

			for (var i = 0; i < image.Width; ++i)
			{
				for (var j = 0; j < image.Height; ++j)
				{
					result.SetPixel(i, j, GetPixelColor(i, j, image));
				}
			}

			return result;
		}

		private static Rgb GetPixelColor(int x, int y, RgbImage bitmap)
		{
			byte sum = 0;
			for (var i = -2; i <= 2; ++i)
			{
				for (var j = -2; j <= 2; ++j)
				{
					var tox = x + i;
					var toy = y + j;
					var value = bitmap.Contains(tox, toy) ? bitmap.GetPixel(tox, toy).Brightness() : (byte) 0;
					sum += (byte) (value == 255 ? 1 : 0);
				}
			}
			return sum > Threshold ? RgbColors.White : RgbColors.Black;
		}
	}
}