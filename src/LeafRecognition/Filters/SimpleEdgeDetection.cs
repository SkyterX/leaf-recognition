﻿using Emgu.CV;
using Emgu.CV.Structure;
using LeafRecognition.Images;

namespace LeafRecognition.Filters
{
	public class SimpleEdgeDetection : IFilter
	{
		public static readonly SimpleEdgeDetection Default = new SimpleEdgeDetection(3);
		private readonly int size;

		public SimpleEdgeDetection(int size)
		{
			this.size = size;
		}

		public RgbImage Apply(RgbImage image)
		{
			var result = image.Clone();
			return
				(result.Image - result.Image.Erode(1)).ToRgbImage().Apply(Inverse.Default);

			for (var x = 0; x < image.Width; ++x)
			{
				for (var y = 0; y < image.Height; ++y)
				{
					if (image.IsWhite(x, y))
					{
						result.SetPixel(x, y, RgbColors.White);
						continue;
					}

					var needBlack = false;
					for (var i = -size/2; i <= size/2; ++i)
					{
						for (var j = -size/2; j <= size/2; ++j)
						{
							if (!image.Contains(x + i, y + j))
								continue;
							if (image.IsWhite(x + i, y + j))
								needBlack = true;
						}
					}
					result.SetPixel(x, y, needBlack ? RgbColors.Black : RgbColors.White);
				}
			}
			return result;
		}
	}
}