﻿using System.Linq;
using Emgu.CV;
using Emgu.CV.Structure;
using LeafRecognition.Images;

namespace LeafRecognition.Filters
{
    public class KernelFilter : IFilter
    {
        public static readonly KernelFilter Unsharp = new KernelFilter(new[,]
            {
                {1, 4, 6, 4, 1},
                {4, 16, 24, 16, 4},
                {6, 24, -476, 24, 6},
                {4, 16, 24, 16, 4},
                {1, 4, 6, 4, 1}
            }, 5);

        public static readonly KernelFilter GaussianBlur = new KernelFilter(new[,]
            {
                {1, 2, 1},
                {2, 4, 2},
                {1, 2, 1}
            }, 3);

        public static readonly KernelFilter Sharpen = new KernelFilter(new[,]
            {
                {0, -1, 0},
                {-1, 5, -1},
                {0, -1, 0}
            }, 3);

        private readonly int coefficient;
        private readonly int[,] kernel;
        private readonly int size;

        private KernelFilter(int[,] kernel, int size)
        {
            this.kernel = kernel;
            int sum = kernel.Cast<int>().Sum();
            coefficient = sum == 0 ? 1 : sum;
            this.size = size;
        }

        public RgbImage Apply(RgbImage image)
        {
            RgbImage result = GrayscaleYFilter.Default.Apply(image);

            for (int x = 0; x < image.Width; ++x)
            {
                for (int y = 0; y < image.Height; ++y)
                {
                    int sum = 0;
                    for (int i = 0; i < size; ++i)
                    {
                        for (int j = 0; j < size; ++j)
                        {
                            int tox = x - (size/2) + i;
                            int toy = y - (size/2) + j;

                            if (!image.Contains(tox, toy))
                                continue;

                            Rgb color = image.GetPixel(tox, toy);
                            sum += color.Brightness()*kernel[i, j];
                        }
                    }
                    sum /= coefficient;
                    sum = sum < 0 ? 0 : sum;
                    result.SetPixel(x, y, new Rgb((byte) sum, (byte) sum, (byte) sum));
                }
            }

            return result;
        }
    }
}