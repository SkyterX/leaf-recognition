﻿using LeafRecognition.Images;

namespace LeafRecognition.Filters
{
	public class Erosion : IFilter
	{
		public static readonly Erosion Default = new Erosion(7);
		private readonly int size;

		public Erosion(int size)
		{
			this.size = size;
		}

		public RgbImage Apply(RgbImage image)
		{
			var result = image.Clone();
			result.Image._Erode(2);
			return result;

			for (var x = 0; x < image.Width; ++x)
			{
				for (var y = 0; y < image.Height; ++y)
				{
					var needWhite = false;
					for (var i = -size/2; i <= size/2; ++i)
					{
						for (var j = -size/2; j <= size/2; ++j)
						{
							if (!image.Contains(x + i, y + j))
								continue;
							if (image.IsWhite(x + i, y + j))
								needWhite = true;
						}
					}
					result.SetPixel(x, y, needWhite ? RgbColors.White : RgbColors.Black);
				}
			}
			return result;
		}
	}
}