using System;
using System.Drawing;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using LeafRecognition.Algorithms;
using LeafRecognition.Images;

namespace LeafRecognition.Filters
{
	public class StemRemoval : IFilter
	{
		public static readonly StemRemoval Instance = new StemRemoval();

		private StemRemoval()
		{
		}

		public RgbImage Apply(RgbImage image)
		{
			double[] min, max;
			Point[] minP, maxP;

			var grayImage = image.Image.Convert<Gray, byte>();

			grayImage.MinMax(out min, out max, out minP, out maxP);
			if (min[0] > 128) return image;

			var component = grayImage.FillComponent(minP[0].X, minP[0].Y, RgbColors.Black);
			var imageArea = image.Width*image.Height;

			var cnt = Math.Max(image.Width, image.Height)/1000.0;
			var compToImage = component.area*1.0/imageArea;

			cnt *= compToImage > 0.3 ? 4 : 2;
			cnt = Math.Round(cnt);

			var fillComponent = new FillLargestComponent(comp =>
			{
				var len = Math.Max(comp.rect.Width, comp.rect.Height);
				return len > cnt*10 ? len : -1;
			});


			var stem = image.Image
				.MorphologyEx(new StructuringElementEx(7, 7, 3, 3, CV_ELEMENT_SHAPE.CV_SHAPE_ELLIPSE), 
				CV_MORPH_OP.CV_MOP_BLACKHAT, (int) cnt)
				.ToRgbImage();
			stem.Image._Not();
			stem = stem.Apply(fillComponent);
			var stemSize = stem.CountBlack();
			stem.Image._And(image.Image.Not());
			stem.Image._Not();
			stem = stem.Apply(FillLargestComponent.MaxArea);
//			var result = image.Image.Not().And(stem.Image).ToRgbImage();
//			result.Image._Not();
//			result = result.Apply(FillLargestComponent.MaxArea);

			var diff = stem.Image.Xor(image.Image);

			var diffSize = diff.CountNonzero()[0];
			if (diffSize > stemSize*7)
				return image;
//			image.Image.SetValue(new Rgb(Color.Gray), image.Image.Xor(stem.Image).Convert<Gray,byte>());
//			return image;
			return stem;
		}
	}
}