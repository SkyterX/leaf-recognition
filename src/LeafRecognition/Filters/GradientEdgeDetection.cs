﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Emgu.CV;
using Emgu.CV.Structure;
using LeafRecognition.Images;

namespace LeafRecognition.Filters
{
    public class GradientEdgeDetection : IFilter
    {
        private static readonly int[][] SobelOperatorX = {new[] {-1, -2, -1}, new[] {0, 0, 0}, new[] {1, 2, 1}};
        private static readonly int[][] SobelOperatorY = {new[] {-1, 0, 1}, new[] {-2, 0, 2}, new[] {-1, 0, 1}};

        public static readonly GradientEdgeDetection Default = new GradientEdgeDetection(100, 2);

	    public GradientEdgeDetection(int threshold, int parallelParts)
        {
            Threshold = threshold;
            ParallelParts = parallelParts;
        }

        public static int Threshold { get; set; }
        public static int ParallelParts { get; set; }

        public RgbImage Apply(RgbImage image)
        {
	        var result = image.Clone();

            for (int i = 0; i < image.Width; ++i)
            {
                result.SetPixel(i, 0, RgbColors.White);
				result.SetPixel(i, image.Height - 1, RgbColors.White);
            }
            for (int i = 0; i < image.Height; ++i)
            {
				result.SetPixel(0, i, RgbColors.White);
				result.SetPixel(image.Width - 1, i, RgbColors.White);
            }

            var tasks = new List<Task>();
            int dw = image.Width/ParallelParts;
            int dh = image.Height/ParallelParts;
            for (int i = 0; i < ParallelParts; ++i)
            {
                for (int j = 0; j < ParallelParts; ++j)
                {
                    int x = i;
                    int y = j;
                    tasks.Add(
                        Task.Factory.StartNew(() => ThreadProc(dw*x, dh*y, dw*(x + 1), dh*(y + 1), image, result)));
                }
            }
            Task.WaitAll(tasks.ToArray());
            return result;
        }

        private static Rgb GetGradientLength(RgbImage bitmap, int x, int y)
        {
            double dx = GetDifferencial(bitmap, x, y, SobelOperatorX);
            double dy = GetDifferencial(bitmap, x, y, SobelOperatorY);
            int differencialLenght = Convert.ToInt32(Math.Sqrt(dx*dx + dy*dy));
			return differencialLenght > Threshold ? RgbColors.Black : RgbColors.White;
        }

        private static double GetDifferencial(RgbImage bitmap, int x, int y, int[][] mask)
        {
            double ans = 0;
            for (int i = -1; i <= 1; ++i)
            {
                for (int j = -1; j <= 1; ++j)
                {
                    ans += bitmap.GetPixel(x + i, y + j).Red*mask[i + 1][j + 1];
                }
            }
            return ans;
        }

        private static void ThreadProc(int xl, int yl, int xr, int yr, RgbImage sourceData, RgbImage result)
        {
            for (int i = xl; i < xr; ++i)
            {
                if (i < 1 || i >= sourceData.Width - 1)
                    continue;
                for (int j = yl; j < yr; ++j)
                {
                    if (j < 1 || j >= sourceData.Height - 1)
                        continue;
                    result.SetPixel(i, j, GetGradientLength(sourceData, i, j));
                }
            }
        }
    }
}