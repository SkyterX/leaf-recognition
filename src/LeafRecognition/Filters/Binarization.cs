﻿using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using LeafRecognition.Images;

namespace LeafRecognition.Filters
{
	public class Binarization : IFilter
	{
		public static readonly Binarization Default = new Binarization(140, 2);

		public Binarization(int threshold, int parallelParts)
		{
			Threshold = threshold;
			ParallelParts = parallelParts;
		}

		public static int ParallelParts { get; private set; }
		public static int Threshold { get; private set; }

		public RgbImage Apply(RgbImage image)
		{
			return image.Image.ThresholdAdaptive(new Rgb(Color.White),
				ADAPTIVE_THRESHOLD_TYPE.CV_ADAPTIVE_THRESH_GAUSSIAN_C, THRESH.CV_THRESH_BINARY, 101, new Rgb(4, 4, 4))
				.ToRgbImage();

			var result = image.Clone();
			var tasks = new List<Task>();
			var dw = image.Width/ParallelParts;
			var dh = image.Height/ParallelParts;
			for (var i = 0; i < ParallelParts; ++i)
			{
				for (var j = 0; j < ParallelParts; ++j)
				{
					var x = i;
					var y = j;
					tasks.Add(
						Task.Factory.StartNew(() => ThreadProc(dw*x, dh*y, dw*(x + 1), dh*(y + 1), image, result)));
				}
			}
			Task.WaitAll(tasks.ToArray());

			return result;
		}

		private static void ThreadProc(int xl, int yl, int xr, int yr, RgbImage bitmap, RgbImage result)
		{
			for (var i = xl; i < xr; ++i)
			{
				for (var j = yl; j < yr; ++j)
				{
					result.SetPixel(i, j,
						bitmap.GetPixel(i, j).Brightness() < Threshold
							? RgbColors.Black
							: RgbColors.White);
				}
			}
		}
	}
}