﻿using System;
using System.Drawing;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using LeafRecognition.Images;

namespace LeafRecognition.Filters
{
	public class FloodFillBinarization : IFilter
	{
		public static readonly FloodFillBinarization Instance = new FloodFillBinarization();

		private const int StructureSize = 7;
		private const int StructureCenter = StructureSize / 2;
		private readonly StructuringElementEx structuringElement = new StructuringElementEx(
			StructureSize, StructureSize, StructureCenter, StructureCenter, CV_ELEMENT_SHAPE.CV_SHAPE_RECT);

		private FloodFillBinarization()
		{
		}

		public RgbImage Apply(RgbImage baseImage)
		{
			var image = baseImage.Image.Copy();

			var whiteScalar = new MCvScalar(255);
			
			var w = image.Width;
			var h = image.Height;
			var gradientRuns = Math.Max(5, Math.Max(w, h) / 100);

			var mask = new Image<Gray, byte>(w + 4, h + 4);
			var fillMask = mask.GetSubRect(new Rectangle(1, 1, w + 2, h + 2));
			var imgMask = mask.GetSubRect(new Rectangle(2, 2, w, h));
			CvInvoke.cvCvtColor(image.Ptr, imgMask.Ptr, COLOR_CONVERSION.RGB2GRAY);
			imgMask._MorphologyEx(structuringElement, CV_MORPH_OP.CV_MOP_GRADIENT, gradientRuns);
			CvInvoke.cvThreshold(mask.Ptr, mask.Ptr, 0, 254, THRESH.CV_THRESH_BINARY | THRESH.CV_THRESH_OTSU);

			MCvConnectedComp comp;
			CvInvoke.cvFloodFill(fillMask.Ptr, new Point(0, 0),
				whiteScalar, whiteScalar, whiteScalar,
				out comp, CONNECTIVITY.EIGHT_CONNECTED, FLOODFILL_FLAG.DEFAULT, mask.Ptr);
			imgMask._ThresholdToZero(new Gray(254));

			var res = new Image<Gray, byte>(w, h, new Gray(0));
			var channels = baseImage.Image.Split();
			var average = image.GetAverage(imgMask).MCvScalar.ToArray();
			var weights = new[] { 0.15, 0.25, 0.5 };
			for (int cIdx = 0; cIdx < channels.Length; cIdx++)
			{
				var channel = channels[cIdx];
				channel.SetValue(average[cIdx], imgMask);
				CvInvoke.cvThreshold(channel.Ptr, channel.Ptr, 0, 255, THRESH.CV_THRESH_BINARY_INV | THRESH.CV_THRESH_OTSU);
				res = res.AddWeighted(channel, 1, weights[cIdx], 0);
			}

			res.SetValue(new Gray(0), imgMask);
			CvInvoke.cvThreshold(res.Ptr, res.Ptr, 0, 254, THRESH.CV_THRESH_BINARY | THRESH.CV_THRESH_OTSU);
			mask.SetValue(0);
			res.CopyTo(imgMask);

			CvInvoke.cvFloodFill(fillMask, new Point(0, 0),
				whiteScalar, whiteScalar, whiteScalar,
				out comp, CONNECTIVITY.EIGHT_CONNECTED, FLOODFILL_FLAG.DEFAULT, mask.Ptr);
			imgMask._ThresholdToZero(new Gray(254));

			return imgMask.Convert<Rgb, byte>().ToRgbImage();
		}
	}
}