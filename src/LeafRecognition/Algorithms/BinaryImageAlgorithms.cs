﻿using LeafRecognition.Images;

namespace LeafRecognition.Algorithms
{
	public static class BinaryImageAlgorithms
	{
		public static int CountBlack(this RgbImage image)
		{
			return image.Width*image.Height - image.Image.CountNonzero()[0];
		}
	}
}