﻿using LeafRecognition.Geometry;
using LeafRecognition.Images;

namespace LeafRecognition.Algorithms
{
	public class ConvexHullExtractor
	{
		public static readonly ConvexHullExtractor Instance = new ConvexHullExtractor();

		private ConvexHullExtractor()
		{
		}

		public Polygon2D Find(RgbImage image)
		{
			return image.ToPoint2DSet().ToPolygon2D().GetConvexHull();
		}
	}
}