﻿using System.Linq;
using Emgu.CV;
using Emgu.CV.Structure;
using LeafRecognition.Geometry;

namespace LeafRecognition.Algorithms
{
	public class FitEllipse
	{
		public static readonly FitEllipse Instance = new FitEllipse();

		private FitEllipse()
		{
		}

		public MCvBox2D Apply(Polygon2D convexHull)
		{
			var hull = convexHull.ToArray();
			var cvPoints = new Matrix<int>(hull.Length, 2);
			for (var i = 0; i < hull.Length; i++)
			{
				var point = hull[i];
				cvPoints[i, 0] = point.X;
				cvPoints[i, 1] = point.Y;
			}

			var ellipse = CvInvoke.cvFitEllipse2(cvPoints.Ptr);
			ellipse.angle += 90;
			return ellipse;
		}
	}
}