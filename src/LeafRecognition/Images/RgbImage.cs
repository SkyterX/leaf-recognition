﻿using System.Drawing;
using Emgu.CV;
using Emgu.CV.Structure;

namespace LeafRecognition.Images
{
	public class RgbImage
	{
		public readonly Image<Rgb, byte> Image;
		private Size size;

		public RgbImage(Image<Rgb, byte> image)
		{
			Image = image;
			size = image.Size;
		}

		public int Width
		{
			get { return size.Width; }
		}

		public int Height
		{
			get { return size.Height; }
		}

		public Rgb GetPixel(int x, int y)
		{
			return Image[y, x];
		}

		public void SetPixel(int x, int y, Rgb value)
		{
			Image[y, x] = value;
		}

		public bool Contains(int x, int y)
		{
			return x >= 0 && y >= 0 &&
				   x < Width && y < Height;
		}

		public bool IsBlack(int x, int y)
		{
			var col = Image[y, x];
			return col.Red < 0.5 && col.Green < 0.5 && col.Blue < 0.5;
		}

		public bool IsWhite(int x, int y)
		{
			var col = Image[y, x];
			return col.Red > 254.5 && col.Green > 254.5 && col.Blue > 254.5;
		}

		public RgbImage Clone()
		{
			return new RgbImage(Image.Copy());
		}
	}
}