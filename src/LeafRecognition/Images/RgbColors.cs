﻿using System;
using System.Drawing;
using Emgu.CV.Structure;

namespace LeafRecognition.Images
{
	public static class RgbColors
	{
		public static readonly Rgb Black = new Rgb(Color.Black);
		public static readonly Rgb White = new Rgb(Color.White);

		public static byte Brightness(this Rgb color)
		{
			return (byte) ((color.Red + color.Green + color.Blue)/3);
		}

		public static bool Is(this Rgb color, Rgb otherColor)
		{
			return Math.Abs(color.Red - otherColor.Red) < 0.5 &&
				   Math.Abs(color.Green - otherColor.Green) < 0.5 &&
				   Math.Abs(color.Blue - otherColor.Blue) < 0.5;
		}
	}
}