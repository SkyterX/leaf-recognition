﻿using System;
using System.Drawing;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;

namespace LeafRecognition.Images
{
	public static class ImageExtensions
	{

		public static RgbImage ToRgbImage(this Image<Rgb, byte> image)
		{
			return new RgbImage(image);
		}

		private static readonly MCvScalar Zero = new MCvScalar();
		public static MCvConnectedComp FillComponent<TColor>(this Image<TColor, byte> image, int x, int y, Rgb color) 
			where TColor : struct, IColor
		{
			MCvConnectedComp component;
			CvInvoke.cvFloodFill(image.Ptr, new Point(x, y),
				color.MCvScalar, Zero, Zero, out component,
				CONNECTIVITY.EIGHT_CONNECTED, FLOODFILL_FLAG.DEFAULT,
				IntPtr.Zero);
			return component;
		}
	}
}