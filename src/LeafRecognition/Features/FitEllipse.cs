﻿using System;
using System.Runtime.Serialization;
using LeafRecognition.Recognition;

namespace LeafRecognition.Features
{
	[DataContract(Namespace = "LeafRecognition")]
	public class FitEllipseWidth : IFeature
	{
		public string Description
		{
			get { return "Width of an ellipse fitted to the convex hull."; }
		}

		public double Calculate(ImageInfo imageInfo)
		{
			return imageInfo.Characteristics.FitEllipseSize.Value.Width;
		}
	}

	[DataContract(Namespace = "LeafRecognition")]
	public class FitEllipseHeight : IFeature
	{
		public string Description
		{
			get { return "Height of an ellipse fitted to the convex hull."; }
		}

		public double Calculate(ImageInfo imageInfo)
		{
			return imageInfo.Characteristics.FitEllipseSize.Value.Height;
		}
	}

	[DataContract(Namespace = "LeafRecognition")]
	public class FitEllipseArea : IFeature
	{
		public string Description
		{
			get { return "Area of an ellipse fitted to the convex hull."; }
		}

		public double Calculate(ImageInfo imageInfo)
		{
			var size = imageInfo.Characteristics.FitEllipseSize.Value;
			return size.Width*size.Height/4*Math.PI;
		}
	}

	[DataContract(Namespace = "LeafRecognition")]
	public class FitEllipseEccentricity : IFeature
	{
		public string Description
		{
			get { return "Eccentricity of an ellipse fitted to the convex hull."; }
		}

		public double Calculate(ImageInfo imageInfo)
		{
			var size = imageInfo.Characteristics.FitEllipseSize.Value;
			var ratio = size.Height/size.Width;
			return Math.Sqrt(1 - ratio*ratio);
		}
	}
}