﻿using System.Runtime.Serialization;
using LeafRecognition.Recognition;

namespace LeafRecognition.Features
{
	[DataContract(Namespace = "LeafRecognition")]
	public class Square : IFeature
	{
		[DataMember] private readonly IFeature feature;

		public Square(IFeature feature)
		{
			this.feature = feature;
		}

		public double Calculate(ImageInfo imageInfo)
		{
			var value = feature.Calculate(imageInfo);
			return value*value;
		}

		public string Description
		{
			get { return string.Format("Square of '{0}'", feature.Description); }
		}
	}
}