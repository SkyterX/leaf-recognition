﻿using System.Runtime.Serialization;
using LeafRecognition.Recognition;
using LeafRecognition.Recognition.Vectorization;
using LeafRecognition.Util;

namespace LeafRecognition.Features
{
	public interface IFeatureArray : IDescribable
	{
		double[] Calculate(ImageInfo imageInfo);
		int Length { get; }
	}

	public static class FeatureArrayExtensions
	{
		public static IFeatureArray ToFeatureArray(this IVectorizer vectorizer)
		{
			return new VectorizerAdapter(vectorizer);
		}

		[DataContract(Namespace = "LeafRecognition")]
		private class VectorizerAdapter : IFeatureArray
		{
			[DataMember] private readonly IVectorizer vectorizer;

			public VectorizerAdapter(IVectorizer vectorizer)
			{
				this.vectorizer = vectorizer;
			}

			public int Length
			{
				get { return vectorizer.VectorLength; }
			}

			public string Description
			{
				get { return vectorizer.Description; }
			}

			public double[] Calculate(ImageInfo imageInfo)
			{
				return vectorizer.ToVector(imageInfo);
			}
		}
	}
}