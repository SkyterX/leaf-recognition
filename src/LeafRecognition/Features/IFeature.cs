﻿using LeafRecognition.Recognition;
using LeafRecognition.Util;

namespace LeafRecognition.Features
{
    public interface IFeature : IDescribable
    {
        double Calculate(ImageInfo imageInfo);
    }

	public static class FeatureExtensions
	{
		public static IFeature Square(this IFeature feature)
		{
			return new Square(feature);
		}
	}
}