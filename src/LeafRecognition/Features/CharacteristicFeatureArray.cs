using System;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using LeafRecognition.ImageCharacteristics;
using LeafRecognition.Recognition;

namespace LeafRecognition.Features
{
	[DataContract(Namespace = "LeafRecognition")]
	public class CharacteristicFeatureArray : IFeatureArray
	{
		private static readonly CharacteristicsInfo Empty = new CharacteristicsInfo();
		[DataMember] public readonly int Id;
		[DataMember] private readonly int length;
		public Func<CharacteristicsInfo, Characteristic<double[]>> FieldGetter { get; private set; }

		public CharacteristicFeatureArray(int id, int length, Func<CharacteristicsInfo, Characteristic<double[]>> fieldGetter)
		{
			Id = id;
			this.length = length;
			FieldGetter = fieldGetter;
		}

		public int Length
		{
			get { return length; }
		}

		public string Description
		{
			get { return FieldGetter(Empty).Description; }
		}

		public double[] Calculate(ImageInfo imageInfo)
		{
			return FieldGetter(imageInfo.Characteristics).Value;
		}

		#region Serialization

		[OnDeserialized]
		private void OnDeserialized(StreamingContext context)
		{
			var feature = typeof (Characteristics)
				.GetFields(BindingFlags.Public | BindingFlags.Static)
				.Select(fieldInfo => fieldInfo.GetValue(null) as CharacteristicFeatureArray)
				.Single(field => field != null && field.Id == Id);
			FieldGetter = feature.FieldGetter;
		}

		#endregion
	}
}