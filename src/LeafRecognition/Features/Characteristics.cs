﻿namespace LeafRecognition.Features
{
	public static class Characteristics
	{
		public static readonly IFeature LeafArea = new CharacteristicFeature(0, attrs => attrs.LeafArea);
		public static readonly IFeature LeafDiameter = new CharacteristicFeature(1, attrs => attrs.LeafDiameter);
		public static readonly IFeature LeafPerimeter = new CharacteristicFeature(2, attrs => attrs.LeafPerimeter);
		public static readonly IFeature ConvexHullArea = new CharacteristicFeature(3, attrs => attrs.ConvexHullArea);
		public static readonly IFeature SmoothedLeafArea = new CharacteristicFeature(4, attrs => attrs.SmoothedLeafArea);

		public static readonly IFeatureArray ShapeHuMoments = new CharacteristicFeatureArray(0, 7, attrs => attrs.ShapeHuMoments);
        public static readonly IFeatureArray BorderHuMoments = new CharacteristicFeatureArray(1, 7, attrs => attrs.BorderHuMoments);
        public static readonly IFeatureArray CssFeatures = new CharacteristicFeatureArray(3, 30, attrs => attrs.CssFeatures);

	}
}