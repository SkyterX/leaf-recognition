﻿using System.Runtime.Serialization;
using LeafRecognition.Recognition;

namespace LeafRecognition.Features
{
	[DataContract(Namespace = "LeafRecognition")]
	public class Ratio : IFeature
	{
		private const double Epsilon = 1e-12;

		[DataMember] private readonly IFeature numerator;
		[DataMember] private readonly IFeature denominator;

		public Ratio(IFeature numerator, IFeature denominator)
		{
			this.numerator = numerator;
			this.denominator = denominator;
		}

		public double Calculate(ImageInfo imageInfo)
		{
			var b = denominator.Calculate(imageInfo);
			if (b < Epsilon)
				return -1;
			var a = numerator.Calculate(imageInfo);
			return a/b;
		}

		public string Description
		{
			get { return string.Format("Ratio of '{0}' to '{1}'", numerator.Description, denominator.Description); }
		}
	}
}