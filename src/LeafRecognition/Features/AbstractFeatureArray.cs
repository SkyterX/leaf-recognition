﻿using System.Runtime.Serialization;
using LeafRecognition.Recognition;

namespace LeafRecognition.Features
{
	[DataContract(Namespace = "LeafRecognition")]
	public abstract class AbstractFeatureArray : IFeatureArray
	{
		[DataMember] private readonly int length;
		[DataMember] private readonly string description;

		protected AbstractFeatureArray(int length, string description)
		{
			this.length = length;
			this.description = description;
		}

		public int Length
		{
			get { return length; }
		}

		public string Description
		{
			get { return description; }
		}

		public abstract double[] Calculate(ImageInfo imageInfo);
	}
}