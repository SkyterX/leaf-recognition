﻿using System;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using LeafRecognition.ImageCharacteristics;
using LeafRecognition.Recognition;

namespace LeafRecognition.Features
{
	[DataContract(Namespace = "LeafRecognition")]
	public class CharacteristicFeature : IFeature
	{
		private static readonly CharacteristicsInfo Empty = new CharacteristicsInfo();
		[DataMember] public readonly int Id;
		public Func<CharacteristicsInfo, Characteristic<double>> FieldGetter { get; private set; }

		public CharacteristicFeature(int id, Func<CharacteristicsInfo, Characteristic<double>> fieldGetter)
		{
			Id = id;
			FieldGetter = fieldGetter;
		}

		public string Description
		{
			get { return FieldGetter(Empty).Description; }
		}

		public double Calculate(ImageInfo imageInfo)
		{
			return FieldGetter(imageInfo.Characteristics).Value;
		}

		#region Serialization

		[OnDeserialized]
		private void OnDeserialized(StreamingContext context)
		{
			var feature = typeof (Characteristics)
				.GetFields(BindingFlags.Public | BindingFlags.Static)
				.Select(fieldInfo => fieldInfo.GetValue(null) as CharacteristicFeature)
				.Single(field => field != null && field.Id == Id);
			FieldGetter = feature.FieldGetter;
		}

		#endregion
	}
}