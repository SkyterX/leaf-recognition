﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeafRecognition.CssInvestivation
{
    public static class HistogramHelper
    {
        /// <summary>
        /// outputs counts of values in ln scale.
        /// Normalized values that are  >= 10 mapped in 9;
        /// 
        /// </summary>
        /// <param name="values"></param>
        /// <returns> 10 length </returns>
        public static double[] LogHistogam(this IEnumerable<double> values)
        {
            var d = new double[10];
            foreach (var value in values)
            {
                var logVal = Math.Log(value);
                int ind = (int)Math.Floor(logVal);
                if (ind > 9)
                {
                    ind = 9;
                }
                d[ind]++;
            }
            return d;
        }
    }
}
