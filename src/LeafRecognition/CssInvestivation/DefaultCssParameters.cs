﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;

namespace LeafRecognition.CssInvestivation
{
    public partial class  CssParameters
    {
        public static readonly CssParameters DefaultCssParameters = new CssParameters
        {
            AmountOfCriticalPointsTypes = 3,
            ChooseFuncs = new Func<double, bool>[]
                {
                    new LessThen(-0.003).ChooseFunc(),
                    new AbsLessThen(05e-5).ChooseFunc(),
                    new MoreThen(0.006).ChooseFunc()
                },
            IsCssNormalizerUnique = true,
            CssNormailzerGenerateFunc = obj => new NullCssNormalizer(),
            Maxsigma = 1000,
            StepSigma = 1,
            StartSigma = 3,
            LenDelta = 3,
            CriticalPointsAbsorbRate = 0.7,
            IsMaxSigmaAdaptive = true,
            AdaptiveMaxSigmaGenerateFunction = len => ((int)len) / 2
        };
    }
}
