using System;
using System.Drawing;
using System.Linq;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;

namespace LeafRecognition.CssInvestivation
{
    public class ContourFinder
    {
        private readonly Image<Gray, byte> image;
        private readonly MemStorage storage;
        // ����� ����� ����� ������� �� �������� �� ����� ����� ������, �� ����� MemStorage
        // ����� ������
        //System.AccessViolationException :
        //������� ������ ��� ������ � ���������� ������. 
        //��� ����� ��������������� � ���, ��� ������ ������ ����������.
        private Contour<Point> choosedContour;
        public ContourFinder(Image<Gray, byte> halftonedImage)
        {
            image = halftonedImage;
            this.choosedContour = null;
            storage = new MemStorage();
        }

        ~ContourFinder()
        {
            storage.Dispose();
        }
        //TODO ������� ������ ��������� ������ �������.
        /// <summary>
        /// This solution is not ideal. One can simple forget to run this method. Maybe static constructor method?
        /// </summary>
        public void ChooseTheFirstContour()
        {
            var maxCount = 0;
            var maxwidth = 0;
            Contour<Point> contourEnum = image.FindContours(CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_NONE,
                RETR_TYPE.CV_RETR_LIST, storage);
            while (contourEnum != null)
            {
//                Contour<Point> approxContour = contourEnum.ApproxPoly(0.08/*contourEnum.Perimeter * 0.015*/, storage);
                var curContourWidth = contourEnum.BoundingRectangle.Width;
//                if (curContourWidth > 20)
//                {
                    var count = contourEnum.Count();
                    if (count > maxCount/*curContourWidth > maxwidth && curContourWidth < image.Width - 10*/)
                    {
                        maxCount = count;
                        maxwidth = curContourWidth;
//                        contourEnum.ToArray();
                        choosedContour = contourEnum;
//                    }
                }
                contourEnum = contourEnum.HNext;
            }
            
        }
        public void ChooseTheSecondContour()
        {
            var maxCount = 0;
            var maxwidth = 0;
            Contour<Point> secondContour = null;
            Contour<Point> contourEnum = image.FindContours(CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_NONE,
                RETR_TYPE.CV_RETR_LIST, storage);
            while (contourEnum != null)
            {
                Contour<Point> approxContour = contourEnum.ApproxPoly(contourEnum.Perimeter * 0.015, storage);
                var curContourWidth = approxContour.BoundingRectangle.Width;
                if (curContourWidth > 20)
                {
                    var count = contourEnum.Count();
                    if (curContourWidth > maxwidth && curContourWidth < image.Width - 10)
                    {
                        maxCount = count;
                        maxwidth = curContourWidth;
                        contourEnum.ToArray();
                        choosedContour = secondContour;
                        secondContour = contourEnum;
                    }
                }
                contourEnum = contourEnum.HNext;
            }

        }
        public void ChooseTheThirdContour()
        {
            var maxCount = 0;
            var maxwidth = 0;
            Contour<Point> secondContour = null;
            Contour<Point> thirdContour = null;
            Contour<Point> contourEnum = image.FindContours(CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_NONE,
                RETR_TYPE.CV_RETR_LIST, storage);
            while (contourEnum != null)
            {
                Contour<Point> approxContour = contourEnum.ApproxPoly(contourEnum.Perimeter * 0.015, storage);
                var curContourWidth = approxContour.BoundingRectangle.Width;
                if (curContourWidth > 20)
                {
                    var count = contourEnum.Count();
                    if (curContourWidth > maxwidth && curContourWidth < image.Width - 10)
                    {
                        maxCount = count;
                        maxwidth = curContourWidth;
                        contourEnum.ToArray();
                        choosedContour = secondContour;
                        secondContour = thirdContour;
                        thirdContour = contourEnum;
                    }
                }
                contourEnum = contourEnum.HNext;
            }

        }
        public Point[] ChosedContourPoints()
        {
            if (choosedContour == null)
            {
                throw new Exception("Choose the contour first");
            }
            return choosedContour.ToArray();
        }
        public Point[] ChosedApproximatedContourPoints(double approxAccuracy = 1)
        {
            if (choosedContour == null)
            {
                throw new Exception("Choose the contour first");
            }
            return choosedContour.ApproxPoly(approxAccuracy, 0, storage).ToArray();
        }
    }
}