using System;
using System.Drawing;
using System.Linq;
using Emgu.CV;
using Emgu.CV.Structure;

namespace LeafRecognition.CssInvestivation
{
    public class CurvatureOfClosedLine
    {
        private readonly Image<Gray, float> xImg;
        private readonly Image<Gray, float> yImg;
        private readonly int kernelSize;
        private readonly int contourLen;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="contourPoints"></param>
        /// <param name="kernelSize">must be odd, validated inside</param>
        public CurvatureOfClosedLine(Point[] contourPoints, int kernelSize)
        {
            xImg = contourPoints.Select(p => p.X).FuncToImgDoubled(contourPoints.Length);
            yImg = contourPoints.Select(p => p.Y).FuncToImgDoubled(contourPoints.Length);
            this.kernelSize = kernelSize.CloseOdd();
            this.contourLen = contourPoints.Length;
        }

        public CurvatureOfClosedLine(Point[] contourPoints) 
            : this(contourPoints, contourPoints.Length)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sigma">bigger --- smoother</param>
        /// <returns></returns>
        public double[] GetCurveMeasureInSmoothedContour(int sigma)
        {
            var lineGaussianKernel = Helper.CreateLineGaussianKernel(kernelSize, sigma);

            var dGKernel = new ConvolutionKernelF(lineGaussianKernel
                .LinMatToFloats()
                .FuncToImg(kernelSize)
                .Sobel(1, 0, 3)
                .ImgToFunc(kernelSize)
                .FloatsToLinMat(kernelSize))
                .Transpose();
            var ddGKernel = new ConvolutionKernelF(lineGaussianKernel
                .LinMatToFloats()
                .FuncToImg(kernelSize)
                .Sobel(2, 0, 3)
                .ImgToFunc(kernelSize)
                .FloatsToLinMat(kernelSize))
                .Transpose();


            var dxs = xImg.Convolution(dGKernel).ImgToFuncDoubled(contourLen);
            var ddxs = xImg.Convolution(ddGKernel).ImgToFuncDoubled(contourLen);
            var dys = yImg.Convolution(dGKernel).ImgToFuncDoubled(contourLen);
            var ddys = yImg.Convolution(ddGKernel).ImgToFuncDoubled(contourLen);

            var ks = new[] { dxs, ddxs, dys, ddys }.JaggedPivot().Select(l =>
            {
                //                    l = l.Select(c => c*50).ToList();
                var dx = (double)l[0];
                var ddx = (double)l[1];
                var dy = (double)l[2];
                var ddy = (double)l[3];
                var denum = dx * dx + dy * dy; //1;
                var enumerator = dx * ddy - ddx * dy;
                return enumerator / Math.Sqrt(denum * denum * denum); // curve equation http://en.wikipedia.org/wiki/Curvature
            }).ToArray();

            return ks;
        }
    }
}