using System.Collections.Generic;
using System.Drawing;
using Emgu.CV;
using Emgu.CV.Structure;

namespace LeafRecognition.CssInvestivation
{
    public class CssDrawer
    {
        public readonly Image<Bgr, byte> CSSImage;
        public CssDrawer(int width, int height)
            
        {
            CSSImage = new Image<Bgr, byte>(width, height, new Bgr(255, 255, 255));
        }

        public  void DrawPoints(IEnumerable<Point> points, Bgr color, int thickness=1)
        {

            foreach (var point in points)
            {
                DrawPoint(point, color, thickness);
            }
        }

        private void DrawPoint(Point point, Bgr color, int thickness)
        {
            CSSImage.Draw(new CircleF(point, 0), color, thickness);
        }

        public void DrawPoints(bool[,] pointMask, Bgr color, int thickness = 1)
        {
            var height = pointMask.GetLength(0);
            var width = pointMask.GetLength(1);
            for (var i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    if (pointMask[i, j])
                    {
                        DrawPoint(new Point(j, i), color, thickness);
                    }
                }
                
            }


        }
    }
}