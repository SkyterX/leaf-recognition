using System;
using System.Drawing;
using Rhino.Mocks;

namespace LeafRecognition.CssInvestivation
{
    public partial class CssParameters
    {
        public int Maxsigma;
        public int StepSigma;

        public int AmountOfCriticalPointsTypes;
        public Func<double, bool>[] ChooseFuncs;

        public Func<Object, ICssNormalizer> CssNormailzerGenerateFunc; // �� ����� �������� ��������� :(
        public bool IsCssNormalizerUnique; // false ����� ���������� ������� �� ������ ��������
        public int LenDelta;
        public int StartSigma;
        public double CriticalPointsAbsorbRate;
        public bool IsMaxSigmaAdaptive;
        public Function<int, int> AdaptiveMaxSigmaGenerateFunction;
    }
}