using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace LeafRecognition.CssInvestivation
{
    public class CssCreator
    {
        public readonly bool[][,] CSSMatrixesArray;
        public readonly HashSet<Point>[] CriticalPointsArray;
        public readonly CssParameters P;
        public readonly Point[] ContourPoints;
        public readonly int LenContour;

        public CssCreator(CssParameters p, Point[] contourPoints)
        {
            this.P = p;
            this.ContourPoints = contourPoints;
            var amountOfCriticalPointsTypes = P.AmountOfCriticalPointsTypes;
            LenContour = contourPoints.Length;
            if (P.IsMaxSigmaAdaptive)
            {
                P.Maxsigma = P.AdaptiveMaxSigmaGenerateFunction(LenContour);
            }

            CSSMatrixesArray = new bool[amountOfCriticalPointsTypes][,];
            CriticalPointsArray = new HashSet<Point>[amountOfCriticalPointsTypes];
            for (int i = 0; i < amountOfCriticalPointsTypes; i++)
            {
                CSSMatrixesArray[i] = new bool[P.Maxsigma, LenContour];
                CriticalPointsArray[i] = new HashSet<Point>();
            }
        }

        public void CreateCss()
        {
            var ksize = LenContour.CloseOdd(); 

            CurvatureOfClosedLine c = new CurvatureOfClosedLine(ContourPoints, ksize);
            
            ICssNormalizer normalizer = null;
            if (P.IsCssNormalizerUnique)
            {
                normalizer = P.CssNormailzerGenerateFunc(
                    Math.Abs(c.GetCurveMeasureInSmoothedContour(P.Maxsigma/2).Max())); // ���� �� �������
            }
            Console.WriteLine(LenContour);
            for (int sigma = P.StartSigma; sigma < P.Maxsigma; sigma += P.StepSigma)
            {
                var ks = c.GetCurveMeasureInSmoothedContour(sigma);
                if (!P.IsCssNormalizerUnique)
                {
                    normalizer = P.CssNormailzerGenerateFunc(ks); // ���� ����� ��� ������� ���� ���������
                }
                var funcsArray = normalizer.GetFuncs();
                var chooseFuncsArray = P.ChooseFuncs;
                var lens = new int[P.AmountOfCriticalPointsTypes];
                for (int i=0; i < P.AmountOfCriticalPointsTypes; i++)
                {
                    var normalizedTuples = funcsArray[i].Invoke(ks)
                        .Select((k, ind) => Tuple.Create(k, ind))
                        .ToArray();
                    var chooseFunc = chooseFuncsArray[i];
                    var criticalPointsIndexes = normalizedTuples
                        .Where(t => chooseFunc(t.Item1))
                        .Select(t => t.Item2)
                        .ToArray();
                    var cssMatrix = CSSMatrixesArray[i];
                    foreach (var ind in criticalPointsIndexes)
                    {
                        cssMatrix[sigma, ind] = true;
                    }
                    lens[i] = criticalPointsIndexes.Length;
                    var critPoints = CriticalPointsArray[i];
                    //finding critical points on image
                    ProcessCriticalPoints(criticalPointsIndexes, cssMatrix, sigma, LenContour, P.LenDelta, critPoints);
                }

//                Console.WriteLine("sigma = {0}; zeroes = ({1})", sigma, String.Join(", ", lens));

                if (lens.All(l => l == 0) || lens.Any(l => l > 0.7*LenContour))
                {
                    break;
                }

//                logger.Log("sigma = {0}", sigma);
//                logger.Log("Curvature equation point's statistics:");
//                logger.Log("Min     = {0}", ks.Min().ToString("0.################")); // I dont want scientific notation
//                logger.Log("Max     = {0}", ks.Max().ToString("0.################"));
//                logger.Log("Average = {0}", ks.Average().ToString("0.################"));
//                logger.Log("Abs Min = {0}", ks.Select(Math.Abs).Min().ToString("0.################"));
//                logger.Log("Abs Max = {0}", ks.Select(Math.Abs).Max().ToString("0.################"));
//                logger.Log("");
            }
        }
        private void ProcessCriticalPoints(int[] zeroIndxs, bool[,] cssPoints, int sigma, int len,
           int lenDelta, HashSet<Point> criticalPoints)
        {
            var used = new HashSet<int>();
            foreach (var x in zeroIndxs.OrderBy(v => v))
            {
                if (cssPoints[sigma, x] && !used.Contains(x))
                {
                    var xleft = x;
                    var xright = x;
                    while (xright + 1 < len && cssPoints[sigma, xright + 1])
                    {
                        xright++;
                        used.Add(xright);
                    }

                    var xlen = xright - xleft + lenDelta;
                    bool hasLeft = false;
                    bool hasRight = false;
                    var previousSigma = sigma - 1;

                    for (int xi = xleft - xlen; xi < xleft; xi++)
                    {
                        if (cssPoints[previousSigma, (xi + 2 * len) % len])
                        {
                            hasLeft = true;
                        }
                    }
                    for (int xi = xright + 1; xi < xright + xlen + 1; xi++)
                    {
                        if (cssPoints[previousSigma, (xi + 2 * len) % len])
                        {
                            hasRight = true;
                        }
                    }
                    if (hasLeft && hasRight)
                    {
                        var xmiddle = (xleft + xright) / 2;

                        for (int i = xmiddle - lenDelta; i <= xmiddle + lenDelta; i++)
                        {
                            int ci = (i + 2 * len) % len;
                            if (cssPoints[previousSigma, ci])
                            {
                                var prevPoint = new Point(ci, previousSigma);
                                criticalPoints.Remove(prevPoint);
                            }
                        }
                        var point = new Point(xmiddle, sigma);
                        criticalPoints.Add(point);
                    }
                }
            }
        }
        public  void FilterCriticalPoints()
        {
            foreach (var criticalPoints in CriticalPointsArray)
            {
                //TODO can be optimized
                var orderedPoints = criticalPoints.OrderByDescending(point => point.Y).ToArray();
                var maxX = LenContour - 1;

                for (int i = 0; i < orderedPoints.Length; i++)
                {
                    var scale = orderedPoints[i].Y;
                    var loc = orderedPoints[i].X;
                    for (int j = i + 1; j < orderedPoints.Length; j++)
                    {
                        if (!criticalPoints.Contains(orderedPoints[j]))
                        {
                            continue;
                        }
                        var u = orderedPoints[j].X;
                        var usigma = orderedPoints[j].Y;
                        var le = loc - scale;
                        var re = loc + scale;
                        if ((le < u && u < re)
                            ||  (le < 0 && maxX + le  < u)  // ���� ������������� len - |rl| - �.�. ��� ��� ������� ����� ������� ���� ��������� (+/-) 1
                            ||  (re > maxX && u < re - maxX)
                            ) //������ �����������
                        {
                            if (usigma <= scale && usigma > scale * P.CriticalPointsAbsorbRate) // ���� ����� �� ����������� ���
                            {
                                criticalPoints.Remove(orderedPoints[j]);
                            }
                        }
                    }
                }
            }
        }
    }
}