using System;
using System.Collections.Generic;

namespace LeafRecognition.CssInvestivation
{
    public class NullCssNormalizer : ICssNormalizer
    {
        public IEnumerable<double> NormalizeLess(IEnumerable<double> values)
        {
            return values;
        }

        public IEnumerable<double> NormalizeZero(IEnumerable<double> values)
        {
            return values;
        }

        public IEnumerable<double> NormalizeMore(IEnumerable<double> values)
        {
            return values;
        }

        public Func<IEnumerable<double>, IEnumerable<double>>[] GetFuncs()
        {
            return new Func<IEnumerable<double>, IEnumerable<double>>[]{NormalizeLess, NormalizeZero, NormalizeMore};
        }
    }
}