using System;

namespace LeafRecognition.CssInvestivation
{
    public interface IValueChooser
    {
        Func<double, bool> ChooseFunc();
    }

    public class AbsMoreThen : IValueChooser
    {
        public double Epsilon { get; set; }

        public AbsMoreThen(double epsilon)
        {
            this.Epsilon = epsilon;
        }

        public Func<double, bool> ChooseFunc()
        {
            return v => Math.Abs(v) > Epsilon;
        }
    }
    public class MoreThen : IValueChooser
    {
        public double Epsilon { get; set; }

        public MoreThen(double epsilon)
        {
            this.Epsilon = epsilon;
        }

        public Func<double, bool> ChooseFunc()
        {
            return v => v > Epsilon;
        }
    }
    public class AbsLessThen : IValueChooser
    {
        public double Epsilon { get; set; }

        public AbsLessThen(double epsilon)
        {
            this.Epsilon = epsilon;
        }

        public Func<double, bool> ChooseFunc()
        {
            return v => Math.Abs(v) < Epsilon;
        }
    }
    public class LessThen : IValueChooser
    {
        public double Epsilon { get; set; }

        public LessThen(double epsilon)
        {
            this.Epsilon = epsilon;
        }

        public Func<double, bool> ChooseFunc()
        {
            return v => (v) < Epsilon;
        }
    }
}