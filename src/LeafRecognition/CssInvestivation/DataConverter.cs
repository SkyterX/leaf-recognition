using System;
using System.Collections.Generic;
using System.Linq;
using Emgu.CV;
using Emgu.CV.Structure;

namespace LeafRecognition.CssInvestivation
{
    public static class DataConverter
    {
        public static IEnumerable<float> LinMatToFloats(this float[,] mat)
        {
            var len = mat.GetLength(0);
            for (int i = 0; i < len; i++)
            {
                yield return mat[i, 0];
            }
        }

        public static float[,] FloatsToLinMat(this IEnumerable<float> vals, int len)
        {
            var mat = new float[len, 1];
            int i = 0;
            foreach (var val in vals)
            {
                mat[i++, 0] = val;
            }
            return mat;
        }

        public static Image<Gray, float> FuncToImg(this IEnumerable<int> intencities, int len)
        {
            return FuncToImg(intencities.Select(i => (float)i), len);
        }

        public static Image<Gray, float> FuncToImg(this IEnumerable<float> intencities, int len)
        {
            var img = new Image<Gray, float>(len, 1);
            int i = 0;
            foreach (var intencity in intencities)
            {
                img[0, i++] = new Gray(intencity);
            }
            return img;
        }

        public static IEnumerable<float> ImgToFunc(this Image<Gray, float> img, int len)
        {
            for (int i = 0; i < len; i++)
            {
                yield return (float)img[0, i].MCvScalar.v0;
            }
        }

        /// <summary>
        /// Use if you use gaussian kernel of size len and you work with closed curves.
        /// Method simply copies half of array to the left and half to the right
        /// </summary>
        /// <param name="intencities"></param>
        /// <param name="len"></param>
        /// <returns></returns>
        public static Image<Gray, float> FuncToImgDoubled(this IEnumerable<int> intencities, int len)
        {
            return FuncToImgDoubled(intencities.Select(i => (float)i), len);
        }

        /// <summary>
        /// Use if you use gaussian kernel of size len and you work with closed curves.
        /// Method simply copies half of array to the left and half to the right
        /// </summary>
        /// <param name="intencities"></param>
        /// <param name="len"></param>
        /// <returns></returns>
        public static Image<Gray, float> FuncToImgDoubled(this IEnumerable<float> intencities, int len)
        {
            var dlen = len*2;
            var img = new Image<Gray, float>(dlen, 1);
            int i = len/2;
            foreach (var intencity in intencities)
            {
                img[0, i] = new Gray(intencity);
                img[0, (i+len)%dlen] = new Gray(intencity);
                i++;
            }
            return img;
        }

        public static IEnumerable<float> ImgToFuncDoubled(this Image<Gray, float> img, int len)
        {
            var qlen = len/2;
            for (int i = 0; i < len; i++)
            {
                yield return (float)img[0, i + qlen].MCvScalar.v0;
            }
        }

        public static IEnumerable<int> FloatToInt(this IEnumerable<float> vals)
        {
            return vals.Select(v => (int) Math.Round(v));
        }
    }
}