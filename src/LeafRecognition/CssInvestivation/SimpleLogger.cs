using System;
using System.IO;

namespace LeafRecognition.CssInvestivation
{
    public class SimpleLogger
    {
        private readonly StreamWriter writer;

        public SimpleLogger(string filepath, bool append = false)
        {
            writer = new StreamWriter(filepath, append);
        }

        public void Log(String msg)
        {
            writer.WriteLine(msg);
        }
        public void Log(String msg, params object[] objs)
        {
            writer.WriteLine(msg, objs);
        }

//
//        ~SimpleLogger()
//        {
//            writer.Close();
//        }
    }
}