using System;
using System.Collections.Generic;

namespace LeafRecognition.CssInvestivation
{
    public interface ICssNormalizer // �� �������� ��� �� �����, ����� � ����� �������
    {
        IEnumerable<double> NormalizeLess(IEnumerable<double> values);
        IEnumerable<double> NormalizeZero(IEnumerable<double> values);
        IEnumerable<double> NormalizeMore(IEnumerable<double> values);
        Func<IEnumerable<double>, IEnumerable<double>>[] GetFuncs();
    }
}