using System;
using System.Collections.Generic;
using System.Linq;

namespace LeafRecognition.CssInvestivation
{
    public class MultipleDenumNormalizer : ICssNormalizer
    {
        public readonly double DenumLess;
        public readonly double DenumZero;
        public readonly double DenumMore;

        public MultipleDenumNormalizer(double denumLess, double denumZero, double denumMore)
        {
            this.DenumLess = denumLess;
            this.DenumZero = denumZero;
            this.DenumMore = denumMore;
        }

        public MultipleDenumNormalizer(double generalDenum)
            : this(generalDenum, generalDenum, generalDenum)
        {
        }

        public IEnumerable<double> NormalizeLess(IEnumerable<double> values)
        {
            return values.Select(v => v / DenumLess);
        }

        public IEnumerable<double> NormalizeZero(IEnumerable<double> values)
        {
            return values.Select(v => v / DenumZero);
        }

        public IEnumerable<double> NormalizeMore(IEnumerable<double> values)
        {
            return values.Select(v => v / DenumMore);
        }

            
        public  Func<IEnumerable<double>, IEnumerable<double>>[] GetFuncs()
        {
            return new Func<IEnumerable<double>, IEnumerable<double>>[]
            {
                NormalizeLess, NormalizeZero, NormalizeMore
            };
        }
    }
}