using System;
using System.Collections.Generic;
using System.Linq;

namespace LeafRecognition.CssInvestivation
{
    public static class Helper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ksize">must be odd</param>
        /// <param name="sigma"></param>
        /// <returns></returns>
        public static float[,] CreateLineGaussianKernel(int ksize, float sigma)
        {
            float[,] kernel = new float[ksize, 1];
            var m = (ksize - 1) / 2;
            var alpha = 1 / (sigma * Math.Sqrt(2 * Math.PI));
            for (int i = 0; i < ksize; i++)
            {
                var q = (i - m) / (sigma);
                float val = (float)(alpha * Math.Exp(-q * q / 2));
                kernel[i, 0] = val;
            }
            return kernel;
        }
        public static int CloseOdd(this int val)
        {
            return val-(val&1 - 1);
        }

        public static IEnumerable<List<T>> JaggedPivot<T>(
            this IEnumerable<IEnumerable<T>> source)
        {
            List<IEnumerator<T>> originalEnumerators = source
                .Select(x => x.GetEnumerator())
                .ToList();

            try
            {
                List<IEnumerator<T>> enumerators = originalEnumerators
                    .Where(x => x.MoveNext()).ToList();

                while (enumerators.Any())
                {
                    List<T> result = enumerators.Select(x => x.Current).ToList();
                    yield return result;
                    enumerators = enumerators.Where(x => x.MoveNext()).ToList();
                }
            }
            finally
            {
                originalEnumerators.ForEach(x => x.Dispose());
            }
        }
    }
}