﻿namespace LeafRecognition.Util
{
	public interface IDescribable
	{
		string Description { get; }
	}
}