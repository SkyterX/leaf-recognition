﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Xml;

namespace LeafRecognition.Util
{
	public static class Serializer
	{
		public static readonly IDictionary<string, string> DefaultNamespaceMappings = new Dictionary<string, string>
		{
			{"array", "http://schemas.microsoft.com/2003/10/Serialization/Arrays"}
		};

		public static IEnumerable<Type> GetAllKnownTypes(Assembly assembly = null)
		{
			assembly = assembly ?? Assembly.GetExecutingAssembly();
			return assembly
				.GetTypes()
				.Where(type => type.GetCustomAttribute<DataContractAttribute>() != null)
				.Where(type => !type.IsInterface && !type.IsGenericType);
		}

		public static void Serialize<TObject>(TObject obj, Stream stream)
		{
			var serializer = new DataContractSerializer(typeof (TObject), GetAllKnownTypes(typeof (TObject).Assembly));
			using (var zipStream = new GZipStream(stream, CompressionLevel.Optimal, true))
			using (var writer = XmlWriter.Create(zipStream))
			{
				serializer.WriteStartObject(writer, obj);
				foreach (var pair in DefaultNamespaceMappings)
				{
					writer.WriteAttributeString("xmlns", pair.Key, null, pair.Value);
				}
				serializer.WriteObjectContent(writer, obj);
				serializer.WriteEndObject(writer);
			}
		}

		public static void Serialize<TObject>(TObject obj, string filePath)
		{
			using (var fileStream = new FileStream(filePath, FileMode.Create))
				Serialize(obj, fileStream);
		}

		public static TObject Deserialize<TObject>(Stream stream)
		{
			var serializer = new DataContractSerializer(typeof (TObject), GetAllKnownTypes(typeof (TObject).Assembly));
			using (var zipStream = new GZipStream(stream, CompressionMode.Decompress, true))
			using (var xmlReader = XmlReader.Create(zipStream))
			{
				return (TObject) serializer.ReadObject(xmlReader);
			}
		}

		public static TObject TryDeserialize<TObject>(Stream stream)
			where TObject : class
		{
			try
			{
				return Deserialize<TObject>(stream);
			}
			catch (Exception)
			{
				return null;
			}
		}

		public static TObject Deserialize<TObject>(string filePath)
		{
			using (var fileStream = new FileStream(filePath, FileMode.Open))
				return Deserialize<TObject>(fileStream);
		}

		public static TObject TryDeserialize<TObject>(string filePath)
			where TObject : class
		{
			try
			{
				return Deserialize<TObject>(filePath);
			}
			catch (Exception)
			{
				return null;
			}
		}
	}
}