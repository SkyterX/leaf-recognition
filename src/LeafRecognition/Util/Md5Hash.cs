﻿using System.Linq;
using System.Text;

namespace LeafRecognition.Util
{
	public static class Md5Hash
	{
		/// <returns>hex string</returns>
		public static string Hash(byte[] fileData)
		{
			return System.Security.Cryptography.MD5.Create()
				.ComputeHash(fileData)
				.Select(b => b.ToString("X2"))
				.AggregateToString();
		}

		public static string MD5(this string s)
		{
			return Hash(Encoding.UTF8.GetBytes(s));
		}
	}
}