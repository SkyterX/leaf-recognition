﻿using System;
using System.Reflection;

namespace LeafRecognition.Util
{
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum, 
		Inherited = false, AllowMultiple = false)]
	public class VersionAttribute : Attribute
	{
		public readonly int Version;

		public VersionAttribute(int version)
		{
			Version = version;
		}
	}

	public static class VersionExtensions
	{
		public static int GetVersion(this Type t)
		{
			var attribute = t.GetCustomAttribute<VersionAttribute>();
			return attribute == null ? 0 : attribute.Version;
		}
	}
}