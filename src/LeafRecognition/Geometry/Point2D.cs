﻿using System;

namespace LeafRecognition.Geometry
{
    public class Point2D : IEquatable<Point2D>, IComparable<Point2D>
    {
        public Point2D()
        {
            X = Y = 0;
        }

        public Point2D(int x, int y)
        {
            X = x;
            Y = y;
        }

        public int X { get; private set; }
        public int Y { get; private set; }

        public int CompareTo(Point2D other)
        {
            if (Y < other.Y ||
                Y == other.Y && X < other.X)
                return -1;
            return Equals(other) ? 0 : 1;
        }

        public bool Equals(Point2D other)
        {
            return X == other.X &&
                   Y == other.Y;
        }

        public static Point2D operator +(Point2D a, Point2D b)
        {
            return new Point2D(a.X + b.X, a.Y + b.Y);
        }

        public static Point2D operator -(Point2D a, Point2D b)
        {
            return new Point2D(a.X - b.X, a.Y - b.Y);
        }

        public static int DotProduct(Point2D a, Point2D b)
        {
            return a.X*b.X + a.Y*b.Y;
        }

        public static int CrossProduct(Point2D a, Point2D b)
        {
            return a.X*b.Y - a.Y*b.X;
        }

        public int SquaredLength()
        {
            return DotProduct(this, this);
        }

        public double Length()
        {
            return Math.Sqrt(SquaredLength());
        }
    }
}