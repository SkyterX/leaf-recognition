﻿using System.Collections.Generic;
using Emgu.CV;
using Emgu.CV.Structure;
using LeafRecognition.Checkers;
using LeafRecognition.Filters;
using LeafRecognition.Images;

namespace LeafRecognition.Geometry
{
    public static class ImageGeometryExtensions
    {
        public static IEnumerable<Point2D> ToPoint2DSet(this RgbImage image)
        {
            RgbImage internalImage = image;
            if (!HalftonesChecker.Instance.Check(image))
            {
                internalImage = Binarization.Default.Apply(internalImage);
            }

            for (int i = 0; i < internalImage.Width; ++i)
            {
                for (int j = 0; j < internalImage.Height; ++j)
                {
                    if (internalImage.IsBlack(i, j))
                        yield return new Point2D(i, j);
                }
            }
        }

	    public static Polygon2D ToPolygon2D(this IEnumerable<Point2D> points)
	    {
		    return new Polygon2D(points);
	    }
    }
}