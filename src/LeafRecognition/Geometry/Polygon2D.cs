﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace LeafRecognition.Geometry
{
    public class Polygon2D : IEnumerable<Point2D>
    {
        private readonly List<Point2D> points;

        public Polygon2D()
        {
            points = new List<Point2D>();
        }

        public Polygon2D(IEnumerable<Point2D> points)
        {
            this.points = points.ToList();
        }

        public int Count
        {
            get { return points.Count; }
        }

        public IEnumerator<Point2D> GetEnumerator()
        {
            return points.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable) points).GetEnumerator();
        }

        public void Add(Point2D point)
        {
            points.Add(point);
        }

        public Point2D GetPointAt(int index)
        {
            return points[index%points.Count];
        }

        public Polygon2D GetConvexHull()
        {
            if (Count < 3)
                return new Polygon2D(points);

            Point2D pivotPoint = points.Min();
            List<Point2D> sortedPoints = points.ToList();
            sortedPoints.Sort(new AngleComparer(pivotPoint));

            var stack = new List<Point2D> {sortedPoints[0], sortedPoints[1]};

            for (int i = 2; i < Count; ++i)
            {
                while (stack.Count >= 2 && Clockwise(stack[stack.Count - 2], stack[stack.Count - 1], sortedPoints[i]))
                    stack.RemoveAt(stack.Count - 1);
                stack.Add(sortedPoints[i]);
            }

            return new Polygon2D(stack);
        }

        public double GetDiameter() //for convex only
        {
            int ptr1 = 0;
            int ptr2 = 0;
            double diameter = 0.0;

            for (; ptr1 < points.Count; ++ptr1)
            {
                double currentLength = (points[ptr1] - points[ptr2]).Length();
                while ((points[ptr1] - points[(ptr2 + 1)%points.Count]).Length() > currentLength)
                {
                    ptr2 = (ptr2 + 1)%points.Count;
                    currentLength = (points[ptr1] - points[ptr2]).Length();
                }
                diameter = Math.Max(diameter, currentLength);
            }

            return diameter;
        }

        public double Area()
        {
            if (points.Count < 3)
                return 0;
            double area = 0.0;
            Point2D a = points[0];
            for (int i = 1; i < points.Count - 1; ++i)
            {
                Point2D b = points[i];
                Point2D c = points[i + 1];
                area += Point2D.CrossProduct(b - a, c - a)/2.0;
            }
            return Math.Abs(area);
        }

        private static bool Clockwise(Point2D a, Point2D b, Point2D c)
        {
            return Point2D.CrossProduct(b - a, c - a) <= 0;
        }
    }
}