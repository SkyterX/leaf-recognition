﻿using System;
using System.Collections.Generic;

namespace LeafRecognition.Geometry
{
    public class AngleComparer : IComparer<Point2D>
    {
        private readonly Point2D pivotPoint;

        public AngleComparer()
        {
            pivotPoint = new Point2D();
        }

        public AngleComparer(Point2D pivotPoint)
        {
            this.pivotPoint = pivotPoint;
        }

        public int Compare(Point2D a, Point2D b)
        {
            Point2D normalizedA = a - pivotPoint;
            Point2D normalizedB = b - pivotPoint;
            double cosA = normalizedA.X/normalizedA.Length();
            double cosB = normalizedB.X/normalizedB.Length();

            if (Compare(normalizedA.Length(), 0.0) == 0 &&
                Compare(normalizedB.Length(), 0.0) == 0)
                return 0;

            if (Compare(normalizedA.Length(), 0.0) == 0 &&
                Compare(normalizedB.Length(), 0.0) != 0)
                return -1;

            if (Compare(normalizedA.Length(), 0.0) != 0 &&
                Compare(normalizedB.Length(), 0.0) == 0)
                return 1;

            if (Compare(cosA, cosB) < 0 ||
                Compare(cosA, cosB) == 0 &&
                Compare(normalizedA.Length(), normalizedB.Length()) > 0)
                return 1;

            if (Compare(cosA, cosB) == 0 &&
                Compare(normalizedA.Length(), normalizedB.Length()) == 0)
                return 0;
            return -1;
        }

        private static int Compare(double a, double b)
        {
            const double eps = 1e-8;
            if (Math.Abs(a - b) < eps)
                return 0;
            if (a < b)
                return -1;
            return 1;
        }
    }
}