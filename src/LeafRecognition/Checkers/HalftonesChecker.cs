﻿using System;
using Emgu.CV.Structure;
using LeafRecognition.Images;

namespace LeafRecognition.Checkers
{
	public class HalftonesChecker : IChecker
	{
		public static readonly HalftonesChecker Instance = new HalftonesChecker();

		private HalftonesChecker()
		{
		}

		public bool Check(RgbImage image)
		{
			for (var i = 0; i < image.Width; ++i)
			{
				for (var j = 0; j < image.Height; ++j)
				{
					if (!GoodPixel(image.GetPixel(i, j)))
						return false;
				}
			}
			return true;
		}

		private static bool GoodPixel(Rgb pixel)
		{
			return Math.Abs(pixel.Red - pixel.Green) < 0.5 &&
				   Math.Abs(pixel.Green - pixel.Blue) < 0.5 &&
				   pixel.Red < 5 || pixel.Red > 250;
		}
	}
}