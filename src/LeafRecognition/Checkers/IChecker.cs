﻿using LeafRecognition.Images;

namespace LeafRecognition.Checkers
{
	public interface IChecker
	{
		bool Check(RgbImage image);
	}
}