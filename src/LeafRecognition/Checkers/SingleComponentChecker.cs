﻿using System;
using System.Collections.Generic;
using System.Linq;
using LeafRecognition.Images;

namespace LeafRecognition.Checkers
{
	public class SingleComponentChecker : IChecker
	{
		private const int AdjacentNumber = 8;
		private static readonly int[] Dx = {1, 0, -1, 0, 1, 1, -1, -1};
		private static readonly int[] Dy = {0, 1, 0, -1, 1, -1, 1, -1};

		public static readonly SingleComponentChecker Instance = new SingleComponentChecker();

		private SingleComponentChecker()
		{
		}

		public bool Check(RgbImage bitmap)
		{
			var componentNumber = new bool[bitmap.Width][];
			for (var i = 0; i < bitmap.Width; ++i)
			{
				componentNumber[i] = new bool[bitmap.Height];
			}

			var firstTime = true;
			for (var i = 0; i < bitmap.Width; ++i)
			{
				for (var j = 0; j < bitmap.Height; ++j)
				{
					if (bitmap.IsBlack(i, j))
					{
						if (!componentNumber[i][j] && !firstTime)
							return false;
						firstTime = false;
						RunBfs(i, j, bitmap, componentNumber);
					}
				}
			}

			return true;
		}

		// TODO use flood fill
		private static void RunBfs(int x, int y, RgbImage bitmap, bool[][] componentNumber)
		{
			componentNumber[x][y] = true;

			var queue = new Queue<Tuple<int, int>>();
			queue.Enqueue(Tuple.Create(x, y));

			while (queue.Any())
			{
				var tuple = queue.Dequeue();
				var vx = tuple.Item1;
				var vy = tuple.Item2;

				for (var i = 0; i < AdjacentNumber; ++i)
				{
					var tox = vx + Dx[i];
					var toy = vy + Dy[i];

					if (!bitmap.Contains(tox, toy) ||
						!bitmap.IsBlack(tox, toy) ||
						componentNumber[tox][toy])
						continue;

					componentNumber[tox][toy] = true;

					queue.Enqueue(Tuple.Create(tox, toy));
				}
			}
		}
	}
}