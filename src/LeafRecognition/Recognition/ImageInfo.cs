﻿using System;
using LeafRecognition.Algorithms;
using LeafRecognition.Filters;
using LeafRecognition.Geometry;
using LeafRecognition.Images;

namespace LeafRecognition.Recognition
{
	public class ImageInfo
	{
		public readonly Lazy<RgbImage> Image;
		public readonly Lazy<RgbImage> LeafShape;
		public readonly Lazy<RgbImage> LeafShapeWithoutStem;
		public readonly Lazy<RgbImage> LeafBorder;
		public readonly Lazy<Polygon2D> ConvexHull;
		public readonly CharacteristicsInfo Characteristics;

		public ImageInfo(RgbImage image, CharacteristicsInfo characteristics = null)
			: this(new Lazy<RgbImage>(() => image), characteristics)

		{
		}

		public ImageInfo(Lazy<RgbImage> image, CharacteristicsInfo characteristics = null)
		{
			Image = image;
			LeafShape = new Lazy<RgbImage>(() => LeafShapeGetter.Instance.Apply(image.Value));
			LeafShapeWithoutStem = new Lazy<RgbImage>(() => StemRemoval.Instance.Apply(LeafShape.Value));
			LeafBorder = new Lazy<RgbImage>(() => SimpleEdgeDetection.Default.Apply(LeafShapeWithoutStem.Value));
			ConvexHull = new Lazy<Polygon2D>(() => ConvexHullExtractor.Instance.Find(LeafBorder.Value));
			Characteristics = characteristics ?? new CharacteristicsInfo();
			Characteristics.Initialize(this);
		}
	}
}