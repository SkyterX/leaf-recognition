﻿using System.Runtime.Serialization;
using LeafRecognition.Features;
using LeafRecognition.Util;

namespace LeafRecognition.Recognition.Vectorization
{
	public interface IVectorizer : IDescribable
	{
		double[] ToVector(ImageInfo imageInfo);
		int VectorLength { get; }
	}

	public static class VectorizerExtensions
	{
		public static IVectorizer ToVectorizer(this IFeatureArray featureArray)
		{
			return new FeatureArrayAdapter(featureArray);
		}

		[DataContract(Namespace = "LeafRecognition")]
		private class FeatureArrayAdapter : IVectorizer
		{
			[DataMember] private readonly IFeatureArray featureArray;

			public int VectorLength
			{
				get { return featureArray.Length; }
			}

			public FeatureArrayAdapter(IFeatureArray featureArray)
			{
				this.featureArray = featureArray;
			}

			public string Description
			{
				get { return featureArray.Description; }
			}

			public double[] ToVector(ImageInfo imageInfo)
			{
				return featureArray.Calculate(imageInfo);
			}
		}
	}
}