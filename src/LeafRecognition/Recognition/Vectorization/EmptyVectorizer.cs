﻿using LeafRecognition.Features;

namespace LeafRecognition.Recognition.Vectorization
{
	public partial class Vectorizer
	{
		public static readonly Vectorizer Empty = new Vectorizer(new IFeature[0]);
	}
}