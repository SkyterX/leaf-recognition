﻿using LeafRecognition.Features;

namespace LeafRecognition.Recognition.Vectorization
{
	public partial class Vectorizer
	{
		public static readonly Vectorizer Default = new Vectorizer(new IFeature[]
		{
			new Ratio(Characteristics.LeafArea, Characteristics.LeafPerimeter),
			new Ratio(Characteristics.LeafArea, Characteristics.LeafPerimeter.Square()),
			new Ratio(Characteristics.LeafArea, Characteristics.ConvexHullArea),
			new Ratio(Characteristics.LeafPerimeter, Characteristics.LeafDiameter),
			new Ratio(Characteristics.LeafArea, Characteristics.LeafDiameter),
			new Ratio(Characteristics.LeafArea, Characteristics.LeafDiameter.Square()),
			new Ratio(Characteristics.LeafArea, Characteristics.SmoothedLeafArea),

			new FitEllipseEccentricity(), 
			new Ratio(new FitEllipseArea(), Characteristics.LeafArea), 
			new Ratio(new FitEllipseWidth(), Characteristics.LeafDiameter), 
		}, 
		Characteristics.ShapeHuMoments, 
		Characteristics.BorderHuMoments
//        ,Characteristics.CssFeatures
		);
	}
}