﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using LeafRecognition.Features;
using LeafRecognition.Util;

namespace LeafRecognition.Recognition.Vectorization
{
	[DataContract(Namespace = "LeafRecognition")]
	public partial class Vectorizer : IVectorizer
	{
		[DataMember] private IFeature[] features;
		[DataMember] private IFeatureArray[] featureArrays;

		private int? vectorLength;

		public int VectorLength
		{
			get
			{
				if (!vectorLength.HasValue)
					vectorLength = features.Length + featureArrays.Sum(array => array.Length);
				return vectorLength.Value;
			}
		}

		public Vectorizer(params IFeature[] features)
		{
			this.features = features;
			featureArrays = new IFeatureArray[0];
		}

		public Vectorizer(IFeature[] features, params IFeatureArray[] featureArrays)
		{
			this.features = features;
			this.featureArrays = featureArrays;
		}

		public double[] ToVector(ImageInfo imageInfo)
		{
			var vector = new double[VectorLength];
			var idx = 0;
			for (; idx < features.Length; idx++)
			{
				vector[idx] = features[idx].Calculate(imageInfo);
			}
			foreach (var featureArray in featureArrays)
			{
				var subVector = featureArray.Calculate(imageInfo);
				foreach (var val in subVector)
				{
					vector[idx++] = val;
				}
			}
			return vector;
		}

		public string Description
		{
			get
			{
				var sb = new StringBuilder();
				sb.AppendFormat("Features : {0} {{{1}", VectorLength, Environment.NewLine);
				var featureDescriptions = features
					.Select(feature => feature.Description + ";" + Environment.NewLine)
					.AggregateToString();
				sb.Append(featureDescriptions);
				var featureArrayDescriptions = featureArrays
					.Select(featureArray => Environment.NewLine + featureArray.Description + ";" + Environment.NewLine)
					.AggregateToString();
				sb.Append(featureArrayDescriptions);
				sb.Append("}");
				return sb.ToString();
			}
		}

		#region Serialization

		[OnDeserialized]
		private void OnDeserialized(StreamingContext context)
		{
			featureArrays = featureArrays ?? new IFeatureArray[0];
		}

		#endregion
	}
}