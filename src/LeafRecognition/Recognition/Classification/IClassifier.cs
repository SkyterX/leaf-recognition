﻿using LeafRecognition.Util;

namespace LeafRecognition.Recognition.Classification
{
	public interface IClassifier : IDescribable
	{
		ClassificationResult Classify(double[] vector);
		string Name { get; }
	}
}