﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using LeafRecognition.Recognition.Tools;
using LeafRecognition.Util;

namespace LeafRecognition.Recognition.Classification
{
	[DataContract(Namespace = "LeafRecognition")]
	public class KNearestNeighbors : IClassifier
	{
		[DataMember] private readonly int kNeighbors;
		[DataMember] private readonly int nClasses;
		[DataMember] private readonly ClassifiedVector[] neighbors;
		[DataMember] private readonly MeanNormalizer normalizer;

		public KNearestNeighbors(int kNeighbors, int nClasses, IList<ClassifiedVector> neighbors)
		{
			normalizer = new MeanNormalizer(neighbors.Select(cv => cv.Vector).ToList());
			this.kNeighbors = kNeighbors;
			this.nClasses = nClasses;
			this.neighbors = neighbors
				.Select(cv => new ClassifiedVector(normalizer.Normalize(cv.Vector), cv.Label, cv.Name))
				.ToArray();
		}

		public string Name
		{
			get { return kNeighbors + "-NearestNeighbors"; }
		}

		public string Description
		{
			get { return string.Format("{0} - Nearest Neighbors classifier", kNeighbors); }
		}

		public ClassificationResult Classify(double[] vector)
		{
			vector = normalizer.Normalize(vector);
			var nearest = neighbors
				.OrderBy(cv => Distance(cv.Vector, vector))
				.Take(kNeighbors);
			var scores = new double[nClasses];
			foreach (var classifiedVector in nearest)
			{
				var distance = Distance(classifiedVector.Vector, vector);
				scores[classifiedVector.Label] += 1.0/(1.0 + distance);
			}
			var label = scores.IndexOfMax();
			var sumScores = scores.Sum();
			scores.ConvertThis(score => score/sumScores);
			return new ClassificationResult(label, scores);
		}

		private double Distance(double[] x, double[] y)
		{
			double sum = 0.0;
			for (var i = 0; i < x.Length; i++)
			{
				var diff = (x[i] - y[i]);
				sum += diff*diff;
			}
			return Math.Sqrt(sum);
		}
	}
}