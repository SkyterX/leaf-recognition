﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using LeafRecognition.Util;

namespace LeafRecognition.Recognition.Classification
{
	[DataContract(Namespace = "LeafRecognition")]
	public class ClassifiedVector : IEquatable<ClassifiedVector>
	{
		[DataMember] public readonly double[] Vector;
		[DataMember] public readonly int Label;
		[DataMember] public readonly string Name;

		public ClassifiedVector(double[] vector, int label, string name)
		{
			Vector = vector;
			Label = label;
			Name = name;
		}

		public override string ToString()
		{
			return string.Format("{{{0}, [{1}]}} {2}", Label, Vector.AggregateToString(";"), Name);
		}

		#region Equality members

		public bool Equals(ClassifiedVector other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return Name == other.Name && Label == other.Label
				   && Vector.Zip(other.Vector, (x, y) => Math.Abs(x - y) < 1e-12).All(x => x);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != GetType()) return false;
			return Equals((ClassifiedVector) obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				return (Label*397) ^ (Vector != null ? Vector.GetHashCode() : 0);
			}
		}

		public static bool operator ==(ClassifiedVector left, ClassifiedVector right)
		{
			return Equals(left, right);
		}

		public static bool operator !=(ClassifiedVector left, ClassifiedVector right)
		{
			return !Equals(left, right);
		}

		#endregion
	}
}