﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using LeafRecognition.Util;

namespace LeafRecognition.Recognition.Classification
{
	[DataContract(Namespace = "LeafRecognition")]
	public class BaselineClassifier : IClassifier
	{
		[DataMember] private readonly ClassificationResult result;

		public BaselineClassifier(int nClasses, IEnumerable<ClassifiedVector> vectors)
		{
			var confidence = new double[nClasses];
			foreach (var vector in vectors)
			{
				confidence[vector.Label]++;
			}
			confidence.ConvertThis(x => x/nClasses);
			result = new ClassificationResult(confidence);
		}

		public ClassificationResult Classify(double[] vector)
		{
			return result;
		}

		public string Description
		{
			get { return "Majority rule classifier"; }
		}

		public string Name
		{
			get { return "Baseline"; }
		}
	}
}