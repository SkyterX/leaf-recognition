using System;

namespace LeafRecognition.Recognition.Classification.RDFLib
{
	internal static class RDFMath
	{
		//public static System.Random RndObject = new System.Random(System.DateTime.Now.Millisecond);

		public const double MaxRealNumber = 1E300;
		public const double MinRealNumber = 1E-300;
		public const double Epsilon = 5e-16;
		public const int VectorMaxPrecision = 15;

		private static int randomSeed = DateTime.Now.Millisecond + 1000*DateTime.Now.Second + 60*1000*DateTime.Now.Minute;
		public static int RandomSeed
		{
			get { return randomSeed; }
			set
			{
				randomSeed = value;
				randomObject = new Random(randomSeed);
			}
		}

		private static Random randomObject = new Random(randomSeed);

		public static bool IsFinite(double d)
		{
			return !Double.IsNaN(d) && !Double.IsInfinity(d);
		}

		public static double RandomReal()
		{
			double r;
			lock (randomObject)
			{
				r = randomObject.NextDouble();
			}
			return r;
		}

		public static int RandomInteger(int n)
		{
			return randomObject.Next(n);
		}

		public static double Sqr(double x)
		{
			return x*x;
		}
	}
}