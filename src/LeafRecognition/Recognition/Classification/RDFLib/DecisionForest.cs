using System;
using System.Runtime.Serialization;

namespace LeafRecognition.Recognition.Classification.RDFLib
{
	public class DecisionForest
	{
		public int BufferSize;
		public int NClasses;
		public int NTrees;
		public int NVars;
		public double[] Trees;

		public DecisionForest()
		{
			Trees = new double[0];
		}
	};

	[DataContract(Namespace = "LeafRecognition")]
	public class DFReport
	{
		public double AverageCrossEntropy;
		public double AverageError;
		public double AverageRelativeError;
		public double RelativeClassificationError;
		public double RootMeanSquareError;
		public double OOBAverageCrossEntropy;
		public double OOBAverageError;
		public double OOBAverageRelativeError;
		public double OOBRelativeClassificationError;
		public double OOBRootMeanSquareError;
	};

	internal class InternalBuffers
	{
		public int[] ClassIBuffer;
		public bool[] EvsBin;
		public double[] EvsSplits;
		public int[] IdxBuffer;
		public int[] SortIBuffer;
		public double[] SortRBuffer;
		public double[] SortRBuffer2;
		public int[] TempBufferI;
		public double[] TempBufferR;
		public double[] TempBufferR2;
		public double[] TreeBuffer;
		public int[] VarPool;

		public InternalBuffers()
		{
			TreeBuffer = new double[0];
			IdxBuffer = new int[0];
			TempBufferR = new double[0];
			TempBufferR2 = new double[0];
			TempBufferI = new int[0];
			ClassIBuffer = new int[0];
			SortRBuffer = new double[0];
			SortRBuffer2 = new double[0];
			SortIBuffer = new int[0];
			VarPool = new int[0];
			EvsBin = new bool[0];
			EvsSplits = new double[0];
		}
	};

	public static class DFExtensions
	{
		private const int serializationCode = 1; // scodes.getrdfserializationcode
		private const int innerNodeWidth = 3;
		private const int leafNodeWidth = 2;
		private const int useStrongSplits = 1;
		private const int useEvs = 2;
		private const int firstVersion = 0;


		/*************************************************************************
		This subroutine builds random decision forest.

		INPUT PARAMETERS:
			XY          -   training set
			NPoints     -   training set size, NPoints>=1
			NVars       -   number of independent variables, NVars>=1
			NClasses    -   task type:
							* NClasses=1 - regression task with one
										   dependent variable
							* NClasses>1 - classification task with
										   NClasses classes.
			NTrees      -   number of trees in a forest, NTrees>=1.
							recommended values: 50-100.
			R           -   percent of a training set used to build
							individual trees. 0<R<=1.
							recommended values: 0.1 <= R <= 0.66.

		OUTPUT PARAMETERS:
			Info        -   return code:
							* -2, if there is a point with class number
								  outside of [0..NClasses-1].
							* -1, if incorrect parameters was passed
								  (NPoints<1, NVars<1, NClasses<1, NTrees<1, R<=0
								  or R>1).
							*  1, if task has been solved
			DF          -   model built
			Rep         -   training report, contains error on a training set
							and out-of-bag estimates of generalization error.

		  -- ALGLIB --
			 Copyright 19.02.2009 by Bochkanov Sergey
		*************************************************************************/

		public static void BuildRandomDecisionForest(double[,] xy,
		                                             int npoints,
		                                             int nvars,
		                                             int nclasses,
		                                             int ntrees,
		                                             double r,
		                                             out int info,
		                                             out DecisionForest df,
		                                             out DFReport rep)
		{
			df = new DecisionForest();
			rep = new DFReport();
			df.BuildRandomDecisionForest(xy, npoints, nvars, nclasses, ntrees, r, out info, rep);
		}

		private static void BuildRandomDecisionForest(this DecisionForest df,
		                                             double[,] xy,
		                                             int npoints,
		                                             int nvars,
		                                             int nclasses,
		                                             int ntrees,
		                                             double r,
		                                             out int info,
		                                             DFReport rep)
		{
			if ((r) <= (0) || (r) > (1))
			{
				info = -1;
				return;
			}
			int samplesize = Math.Max((int) Math.Round(r*npoints), 1);
			df.BuildInternal(xy, npoints, nvars, nclasses, ntrees, samplesize, Math.Max(nvars/2, 1), useStrongSplits + useEvs, out info, rep);
		}


		/*************************************************************************
		This subroutine builds random decision forest.
		This function gives ability to tune number of variables used when choosing
		best split.

		INPUT PARAMETERS:
			XY          -   training set
			NPoints     -   training set size, NPoints>=1
			NVars       -   number of independent variables, NVars>=1
			NClasses    -   task type:
							* NClasses=1 - regression task with one
										   dependent variable
							* NClasses>1 - classification task with
										   NClasses classes.
			NTrees      -   number of trees in a forest, NTrees>=1.
							recommended values: 50-100.
			NRndVars    -   number of variables used when choosing best split
			R           -   percent of a training set used to build
							individual trees. 0<R<=1.
							recommended values: 0.1 <= R <= 0.66.

		OUTPUT PARAMETERS:
			Info        -   return code:
							* -2, if there is a point with class number
								  outside of [0..NClasses-1].
							* -1, if incorrect parameters was passed
								  (NPoints<1, NVars<1, NClasses<1, NTrees<1, R<=0
								  or R>1).
							*  1, if task has been solved
			DF          -   model built
			Rep         -   training report, contains error on a training set
							and out-of-bag estimates of generalization error.

		  -- ALGLIB --
			 Copyright 19.02.2009 by Bochkanov Sergey
		*************************************************************************/

		public static void BuildRandomDecisionForestX1(double[,] xy,
		                                               int npoints,
		                                               int nvars,
		                                               int nclasses,
		                                               int ntrees,
		                                               int nrndvars,
		                                               double r,
		                                               out int info,
		                                               out DecisionForest df,
		                                               out DFReport rep)
		{
			df = new DecisionForest();
			rep = new DFReport();
			df.BuildRandomDecisionForestX1(xy, npoints, nvars, nclasses, ntrees, nrndvars, r, out info, rep);
		}

		private static void BuildRandomDecisionForestX1(this DecisionForest df,
		                                               double[,] xy,
		                                               int npoints,
		                                               int nvars,
		                                               int nclasses,
		                                               int ntrees,
		                                               int nrndvars,
		                                               double r,
		                                               out int info,
		                                               DFReport rep)
		{
			if ((r) <= (0) || (r) > (1))
			{
				info = -1;
				return;
			}
			if (nrndvars <= 0 || nrndvars > nvars)
			{
				info = -1;
				return;
			}
			int samplesize = Math.Max((int) Math.Round(r*npoints), 1);
			df.BuildInternal(xy, npoints, nvars, nclasses, ntrees, samplesize, nrndvars, useStrongSplits + useEvs, out info, rep);
		}


		private static void BuildInternal(this DecisionForest df,
		                                 double[,] xy,
		                                 int npoints,
		                                 int nvars,
		                                 int nclasses,
		                                 int ntrees,
		                                 int samplesize,
		                                 int nfeatures,
		                                 int flags,
		                                 out int info,
		                                 DFReport rep)
		{
			int i;
			int j;
			int k;
			int tmpi;
			var bufs = new InternalBuffers();
			double v;
			int i1;


			//
			// Test for inputs
			//
			if ((((((npoints < 1 || samplesize < 1) || samplesize > npoints) || nvars < 1) || nclasses < 1) || ntrees < 1) ||
			    nfeatures < 1)
			{
				info = -1;
				return;
			}
			if (nclasses > 1)
			{
				for (i = 0; i <= npoints - 1; i++)
				{
					if ((int) Math.Round(xy[i, nvars]) < 0 || (int) Math.Round(xy[i, nvars]) >= nclasses)
					{
						info = -2;
						return;
					}
				}
			}
			info = 1;

			//
			// Flags
			//
			bool useevs = flags/useEvs%2 != 0;

			//
			// Allocate data, prepare header
			//
			int treesize = 1 + innerNodeWidth*(samplesize - 1) + leafNodeWidth*samplesize;
			var permbuf = new int[npoints - 1 + 1];
			bufs.TreeBuffer = new double[treesize - 1 + 1];
			bufs.IdxBuffer = new int[npoints - 1 + 1];
			bufs.TempBufferR = new double[npoints - 1 + 1];
			bufs.TempBufferR2 = new double[npoints - 1 + 1];
			bufs.TempBufferI = new int[npoints - 1 + 1];
			bufs.SortRBuffer = new double[npoints];
			bufs.SortRBuffer2 = new double[npoints];
			bufs.SortIBuffer = new int[npoints];
			bufs.VarPool = new int[nvars - 1 + 1];
			bufs.EvsBin = new bool[nvars - 1 + 1];
			bufs.EvsSplits = new double[nvars - 1 + 1];
			bufs.ClassIBuffer = new int[2*nclasses - 1 + 1];
			var oobbuf = new double[nclasses*npoints - 1 + 1];
			var oobcntbuf = new int[npoints - 1 + 1];
			df.Trees = new double[ntrees*treesize - 1 + 1];
			var xys = new double[samplesize - 1 + 1,nvars + 1];
			var x = new double[nvars - 1 + 1];
			var y = new double[nclasses - 1 + 1];
			for (i = 0; i <= npoints - 1; i++)
			{
				permbuf[i] = i;
			}
			for (i = 0; i <= npoints*nclasses - 1; i++)
			{
				oobbuf[i] = 0;
			}
			for (i = 0; i <= npoints - 1; i++)
			{
				oobcntbuf[i] = 0;
			}

			//
			// Prepare variable pool and EVS (extended variable selection/splitting) buffers
			// (whether EVS is turned on or not):
			// 1. detect binary variables and pre-calculate splits for them
			// 2. detect variables with non-distinct values and exclude them from pool
			//
			for (i = 0; i <= nvars - 1; i++)
			{
				bufs.VarPool[i] = i;
			}
			int nvarsinpool = nvars;
			if (useevs)
			{
				for (j = 0; j <= nvars - 1; j++)
				{
					double vmin = xy[0, j];
					double vmax = vmin;
					for (i = 0; i <= npoints - 1; i++)
					{
						v = xy[i, j];
						vmin = Math.Min(vmin, v);
						vmax = Math.Max(vmax, v);
					}
					if (Math.Abs((vmin) - (vmax)) < RDFMath.Epsilon)
					{
						//
						// exclude variable from pool
						//
						bufs.VarPool[j] = bufs.VarPool[nvarsinpool - 1];
						bufs.VarPool[nvarsinpool - 1] = -1;
						nvarsinpool = nvarsinpool - 1;
						continue;
					}
					bool bflag = false;
					for (i = 0; i <= npoints - 1; i++)
					{
						v = xy[i, j];
						if (Math.Abs((v) - (vmin)) > RDFMath.Epsilon && Math.Abs((v) - (vmax)) > RDFMath.Epsilon)
						{
							bflag = true;
							break;
						}
					}
					if (bflag)
					{
						//
						// non-binary variable
						//
						bufs.EvsBin[j] = false;
					}
					else
					{
						//
						// Prepare
						//
						bufs.EvsBin[j] = true;
						bufs.EvsSplits[j] = 0.5*(vmin + vmax);
						if ((bufs.EvsSplits[j]) <= (vmin))
						{
							bufs.EvsSplits[j] = vmax;
						}
					}
				}
			}

			//
			// RANDOM FOREST FORMAT
			// W[0]         -   size of array
			// W[1]         -   version number
			// W[2]         -   NVars
			// W[3]         -   NClasses (1 for regression)
			// W[4]         -   NTrees
			// W[5]         -   trees offset
			//
			//
			// TREE FORMAT
			// W[Offs]      -   size of sub-array
			//     node info:
			// W[K+0]       -   variable number        (-1 for leaf mode)
			// W[K+1]       -   threshold              (class/value for leaf node)
			// W[K+2]       -   ">=" branch index      (absent for leaf node)
			//
			//
			df.NVars = nvars;
			df.NClasses = nclasses;
			df.NTrees = ntrees;

			//
			// Build forest
			//
			int offs = 0;
			for (i = 0; i <= ntrees - 1; i++)
			{
				//
				// Prepare sample
				//
				for (k = 0; k <= samplesize - 1; k++)
				{
					j = k + RDFMath.RandomInteger(npoints - k);
					tmpi = permbuf[k];
					permbuf[k] = permbuf[j];
					permbuf[j] = tmpi;
					j = permbuf[k];
					for (i1 = 0; i1 <= nvars; i1++)
					{
						xys[k, i1] = xy[j, i1];
					}
				}

				//
				// build tree, copy
				//
				BuildTree(xys, samplesize, nvars, nclasses, nfeatures, nvarsinpool, flags, bufs);
				j = (int) Math.Round(bufs.TreeBuffer[0]);
				int i2 = (0) - (offs);
				for (i1 = offs; i1 <= offs + j - 1; i1++)
				{
					df.Trees[i1] = bufs.TreeBuffer[i1 + i2];
				}
				int lasttreeoffs = offs;
				offs = offs + j;

				//
				// OOB estimates
				//
				for (k = samplesize; k <= npoints - 1; k++)
				{
					for (j = 0; j <= nclasses - 1; j++)
					{
						y[j] = 0;
					}
					j = permbuf[k];
					for (i1 = 0; i1 <= nvars - 1; i1++)
					{
						x[i1] = xy[j, i1];
					}
					df.ProcessInternal(lasttreeoffs, x, ref y);
					i2 = (0) - (j*nclasses);
					for (i1 = j*nclasses; i1 <= (j + 1)*nclasses - 1; i1++)
					{
						oobbuf[i1] = oobbuf[i1] + y[i1 + i2];
					}
					oobcntbuf[j] = oobcntbuf[j] + 1;
				}
			}
			df.BufferSize = offs;

			//
			// Normalize OOB results
			//
			for (i = 0; i <= npoints - 1; i++)
			{
				if (oobcntbuf[i] != 0)
				{
					v = 1/(double) oobcntbuf[i];
					for (i1 = i*nclasses; i1 <= i*nclasses + nclasses - 1; i1++)
					{
						oobbuf[i1] = v*oobbuf[i1];
					}
				}
			}

			//
			// Calculate training set estimates
			//
			rep.RelativeClassificationError = df.RelativeClassificationError(xy, npoints);
			rep.AverageCrossEntropy = df.AverageCrossEntropy(xy, npoints);
			rep.RootMeanSquareError = df.RootMeanSquareError(xy, npoints);
			rep.AverageError = df.AverageError(xy, npoints);
			rep.AverageRelativeError = df.AverageRelativeError(xy, npoints);

			//
			// Calculate OOB estimates.
			//
			rep.OOBRelativeClassificationError = 0;
			rep.OOBAverageCrossEntropy = 0;
			rep.OOBRootMeanSquareError = 0;
			rep.OOBAverageError = 0;
			rep.OOBAverageRelativeError = 0;
			int oobcnt = 0;
			int oobrelcnt = 0;
			for (i = 0; i <= npoints - 1; i++)
			{
				if (oobcntbuf[i] != 0)
				{
					int ooboffs = i*nclasses;
					if (nclasses > 1)
					{
						//
						// classification-specific code
						//
						k = (int) Math.Round(xy[i, nvars]);
						tmpi = 0;
						for (j = 1; j <= nclasses - 1; j++)
						{
							if ((oobbuf[ooboffs + j]) > (oobbuf[ooboffs + tmpi]))
							{
								tmpi = j;
							}
						}
						if (tmpi != k)
						{
							rep.OOBRelativeClassificationError = rep.OOBRelativeClassificationError + 1;
						}
						if (Math.Abs((oobbuf[ooboffs + k]) - (0)) > RDFMath.Epsilon)
						{
							rep.OOBAverageCrossEntropy = rep.OOBAverageCrossEntropy - Math.Log(oobbuf[ooboffs + k]);
						}
						else
						{
							rep.OOBAverageCrossEntropy = rep.OOBAverageCrossEntropy - Math.Log(RDFMath.MinRealNumber);
						}
						for (j = 0; j <= nclasses - 1; j++)
						{
							if (j == k)
							{
								rep.OOBRootMeanSquareError = rep.OOBRootMeanSquareError + RDFMath.Sqr(oobbuf[ooboffs + j] - 1);
								rep.OOBAverageError = rep.OOBAverageError + Math.Abs(oobbuf[ooboffs + j] - 1);
								rep.OOBAverageRelativeError = rep.OOBAverageRelativeError + Math.Abs(oobbuf[ooboffs + j] - 1);
								oobrelcnt = oobrelcnt + 1;
							}
							else
							{
								rep.OOBRootMeanSquareError = rep.OOBRootMeanSquareError + RDFMath.Sqr(oobbuf[ooboffs + j]);
								rep.OOBAverageError = rep.OOBAverageError + Math.Abs(oobbuf[ooboffs + j]);
							}
						}
					}
					else
					{
						//
						// regression-specific code
						//
						rep.OOBRootMeanSquareError = rep.OOBRootMeanSquareError + RDFMath.Sqr(oobbuf[ooboffs] - xy[i, nvars]);
						rep.OOBAverageError = rep.OOBAverageError + Math.Abs(oobbuf[ooboffs] - xy[i, nvars]);
						if (Math.Abs((xy[i, nvars]) - (0)) > RDFMath.Epsilon)
						{
							rep.OOBAverageRelativeError = rep.OOBAverageRelativeError + Math.Abs((oobbuf[ooboffs] - xy[i, nvars])/xy[i, nvars]);
							oobrelcnt = oobrelcnt + 1;
						}
					}

					//
					// update OOB estimates count.
					//
					oobcnt = oobcnt + 1;
				}
			}
			if (oobcnt > 0)
			{
				rep.OOBRelativeClassificationError = rep.OOBRelativeClassificationError/oobcnt;
				rep.OOBAverageCrossEntropy = rep.OOBAverageCrossEntropy/oobcnt;
				rep.OOBRootMeanSquareError = Math.Sqrt(rep.OOBRootMeanSquareError/(oobcnt*nclasses));
				rep.OOBAverageError = rep.OOBAverageError/(oobcnt*nclasses);
				if (oobrelcnt > 0)
				{
					rep.OOBAverageRelativeError = rep.OOBAverageRelativeError/oobrelcnt;
				}
			}
		}


		/*************************************************************************
		Procesing

		INPUT PARAMETERS:
			DF      -   decision forest model
			X       -   input vector,  array[0..NVars-1].

		OUTPUT PARAMETERS:
			Y       -   result. Regression estimate when solving regression  task,
						vector of posterior probabilities for classification task.

		See also DFProcessI.

		  -- ALGLIB --
			 Copyright 16.02.2009 by Bochkanov Sergey
		*************************************************************************/

		public static void Process(this DecisionForest df,
		                           double[] x,
		                           ref double[] y)
		{
			//
			// Proceed
			//
			if (y.Length < df.NClasses)
			{
				y = new double[df.NClasses];
			}
			int offs = 0;
			for (int i = 0; i <= df.NClasses - 1; i++)
			{
				y[i] = 0;
			}
			for (int i = 0; i <= df.NTrees - 1; i++)
			{
				//
				// Process basic tree
				//
				df.ProcessInternal(offs, x, ref y);

				//
				// Next tree
				//
				offs = offs + (int) Math.Round(df.Trees[offs]);
			}
			double v = 1/(double) df.NTrees;
			for (int i = 0; i <= df.NClasses - 1; i++)
			{
				y[i] = v*y[i];
			}
		}


		/*************************************************************************
		'interactive' variant of DFProcess for languages like Python which support
		constructs like "Y = DFProcessI(DF,X)" and interactive mode of interpreter

		This function allocates new array on each call,  so  it  is  significantly
		slower than its 'non-interactive' counterpart, but it is  more  convenient
		when you call it from command line.

		  -- ALGLIB --
			 Copyright 28.02.2010 by Bochkanov Sergey
		*************************************************************************/

		public static void Processi(this DecisionForest df,
		                            double[] x,
		                            out double[] y)
		{
			y = new double[0];

			df.Process(x, ref y);
		}


		/*************************************************************************
		Relative classification error on the test set

		INPUT PARAMETERS:
			DF      -   decision forest model
			XY      -   test set
			NPoints -   test set size

		RESULT:
			percent of incorrectly classified cases.
			Zero if model solves regression task.

		  -- ALGLIB --
			 Copyright 16.02.2009 by Bochkanov Sergey
		*************************************************************************/

		public static double RelativeClassificationError(this DecisionForest df,
		                                                 double[,] xy,
		                                                 int npoints)
		{
			double result = df.ClassificationError(xy, npoints)/(double) npoints;
			return result;
		}


		/*************************************************************************
		Average cross-entropy (in bits per element) on the test set

		INPUT PARAMETERS:
			DF      -   decision forest model
			XY      -   test set
			NPoints -   test set size

		RESULT:
			CrossEntropy/(NPoints*LN(2)).
			Zero if model solves regression task.

		  -- ALGLIB --
			 Copyright 16.02.2009 by Bochkanov Sergey
		*************************************************************************/

		public static double AverageCrossEntropy(this DecisionForest df,
		                                         double[,] xy,
		                                         int npoints)
		{
			var x = new double[df.NVars - 1 + 1];
			var y = new double[df.NClasses - 1 + 1];
			double result = 0;
			for (int i = 0; i <= npoints - 1; i++)
			{
				for (int j = 0; j <= df.NVars - 1; j++)
				{
					x[j] = xy[i, j];
				}
				df.Process(x, ref y);
				if (df.NClasses > 1)
				{
					//
					// classification-specific code
					//
					var k = (int) Math.Round(xy[i, df.NVars]);
					int tmpi = 0;
					for (int j = 1; j <= df.NClasses - 1; j++)
					{
						if ((y[j]) > (y[tmpi]))
						{
							tmpi = j;
						}
					}
					if (Math.Abs((y[k]) - (0)) > RDFMath.Epsilon)
					{
						result = result - Math.Log(y[k]);
					}
					else
					{
						result = result - Math.Log(RDFMath.MinRealNumber);
					}
				}
			}
			result = result/npoints;
			return result;
		}


		/*************************************************************************
		RMS error on the test set

		INPUT PARAMETERS:
			DF      -   decision forest model
			XY      -   test set
			NPoints -   test set size

		RESULT:
			root mean square error.
			Its meaning for regression task is obvious. As for
			classification task, RMS error means error when estimating posterior
			probabilities.

		  -- ALGLIB --
			 Copyright 16.02.2009 by Bochkanov Sergey
		*************************************************************************/

		public static double RootMeanSquareError(this DecisionForest df,
		                                         double[,] xy,
		                                         int npoints)
		{
			var x = new double[df.NVars - 1 + 1];
			var y = new double[df.NClasses - 1 + 1];
			double result = 0;
			for (int i = 0; i <= npoints - 1; i++)
			{
				for (int j = 0; j <= df.NVars - 1; j++)
				{
					x[j] = xy[i, j];
				}
				df.Process(x, ref y);
				if (df.NClasses > 1)
				{
					//
					// classification-specific code
					//
					var k = (int) Math.Round(xy[i, df.NVars]);
					int tmpi = 0;
					for (int j = 1; j <= df.NClasses - 1; j++)
					{
						if ((y[j]) > (y[tmpi]))
						{
							tmpi = j;
						}
					}
					for (int j = 0; j <= df.NClasses - 1; j++)
					{
						if (j == k)
						{
							result = result + RDFMath.Sqr(y[j] - 1);
						}
						else
						{
							result = result + RDFMath.Sqr(y[j]);
						}
					}
				}
				else
				{
					//
					// regression-specific code
					//
					result = result + RDFMath.Sqr(y[0] - xy[i, df.NVars]);
				}
			}
			result = Math.Sqrt(result/(npoints*df.NClasses));
			return result;
		}


		/*************************************************************************
		Average error on the test set

		INPUT PARAMETERS:
			DF      -   decision forest model
			XY      -   test set
			NPoints -   test set size

		RESULT:
			Its meaning for regression task is obvious. As for
			classification task, it means average error when estimating posterior
			probabilities.

		  -- ALGLIB --
			 Copyright 16.02.2009 by Bochkanov Sergey
		*************************************************************************/

		public static double AverageError(this DecisionForest df,
		                                  double[,] xy,
		                                  int npoints)
		{
			var x = new double[df.NVars - 1 + 1];
			var y = new double[df.NClasses - 1 + 1];
			double result = 0;
			for (int i = 0; i <= npoints - 1; i++)
			{
				for (int j = 0; j <= df.NVars - 1; j++)
				{
					x[j] = xy[i, j];
				}
				df.Process(x, ref y);
				if (df.NClasses > 1)
				{
					//
					// classification-specific code
					//
					var k = (int) Math.Round(xy[i, df.NVars]);
					for (int j = 0; j <= df.NClasses - 1; j++)
					{
						if (j == k)
						{
							result = result + Math.Abs(y[j] - 1);
						}
						else
						{
							result = result + Math.Abs(y[j]);
						}
					}
				}
				else
				{
					//
					// regression-specific code
					//
					result = result + Math.Abs(y[0] - xy[i, df.NVars]);
				}
			}
			result = result/(npoints*df.NClasses);
			return result;
		}


		/*************************************************************************
		Average relative error on the test set

		INPUT PARAMETERS:
			DF      -   decision forest model
			XY      -   test set
			NPoints -   test set size

		RESULT:
			Its meaning for regression task is obvious. As for
			classification task, it means average relative error when estimating
			posterior probability of belonging to the correct class.

		  -- ALGLIB --
			 Copyright 16.02.2009 by Bochkanov Sergey
		*************************************************************************/

		public static double AverageRelativeError(this DecisionForest df,
		                                          double[,] xy,
		                                          int npoints)
		{
			var x = new double[df.NVars - 1 + 1];
			var y = new double[df.NClasses - 1 + 1];
			double result = 0;
			int relcnt = 0;
			for (int i = 0; i <= npoints - 1; i++)
			{
				for (int j = 0; j <= df.NVars - 1; j++)
				{
					x[j] = xy[i, j];
				}
				df.Process(x, ref y);
				if (df.NClasses > 1)
				{
					//
					// classification-specific code
					//
					var k = (int) Math.Round(xy[i, df.NVars]);
					for (int j = 0; j <= df.NClasses - 1; j++)
					{
						if (j == k)
						{
							result = result + Math.Abs(y[j] - 1);
							relcnt = relcnt + 1;
						}
					}
				}
				else
				{
					//
					// regression-specific code
					//
					if (Math.Abs((xy[i, df.NVars]) - (0)) > RDFMath.Epsilon)
					{
						result = result + Math.Abs((y[0] - xy[i, df.NVars])/xy[i, df.NVars]);
						relcnt = relcnt + 1;
					}
				}
			}
			if (relcnt > 0)
			{
				result = result/relcnt;
			}
			return result;
		}


		/*************************************************************************
		Copying of DFExtensions strucure

		INPUT PARAMETERS:
			DF1 -   original

		OUTPUT PARAMETERS:
			DF2 -   copy

		  -- ALGLIB --
			 Copyright 13.02.2009 by Bochkanov Sergey
		*************************************************************************/

		public static void Copy(this DecisionForest df1,
		                        DecisionForest df2)
		{
			df2.NVars = df1.NVars;
			df2.NClasses = df1.NClasses;
			df2.NTrees = df1.NTrees;
			df2.BufferSize = df1.BufferSize;
			df2.Trees = new double[df1.BufferSize - 1 + 1];
			for (int i = 0; i <= df1.BufferSize - 1; i++)
			{
				df2.Trees[i] = df1.Trees[i];
			}
		}


		/*************************************************************************
		Serializer: allocation

		  -- ALGLIB --
			 Copyright 14.03.2011 by Bochkanov Sergey
		*************************************************************************/

		private static void Alloc(this DecisionForest forest,
		                         Serializer s)
		{
			s.AllocEntry();
			s.AllocEntry();
			s.AllocEntry();
			s.AllocEntry();
			s.AllocEntry();
			s.AllocEntry();
			s.AllocRealArray(forest.Trees, forest.BufferSize);
		}


		/*************************************************************************
		Serializer: serialization

		  -- ALGLIB --
			 Copyright 14.03.2011 by Bochkanov Sergey
		*************************************************************************/

		public static void Serialize(this DecisionForest obj, out string sOut)
		{
			var s = new Serializer();
			s.AllocStart();
			obj.Alloc(s);
			s.SStartStr();
			obj.Serialize(s);
			sOut = s.GetString();
		}

		private static void Serialize(this DecisionForest forest,
		                             Serializer s)
		{
			s.SerializeInt(serializationCode);
			s.SerializeInt(firstVersion);
			s.SerializeInt(forest.NVars);
			s.SerializeInt(forest.NClasses);
			s.SerializeInt(forest.NTrees);
			s.SerializeInt(forest.BufferSize);
			s.SerializeRealArray(forest.Trees, forest.BufferSize);
		}

		/*************************************************************************
		Serializer: unserialization

		  -- ALGLIB --
			 Copyright 14.03.2011 by Bochkanov Sergey
		*************************************************************************/

		public static void Unserialize(string sIn, out DecisionForest obj)
		{
			var s = new Serializer();
			obj = new DecisionForest();
			s.UStartStr(sIn);
			obj.Unserialize(s);
		}

		private static void Unserialize(this DecisionForest forest,
		                               Serializer s)
		{
			//
			// check correctness of header
			//
			if (s.UnserializeInt() != serializationCode)
				throw new Exception("DFUnserialize: stream header corrupted");
			if (s.UnserializeInt() != firstVersion)
				throw new Exception("DFUnserialize: stream header corrupted");

			//
			// Unserialize data
			//
			forest.NVars = s.UnserializeInt();
			forest.NClasses = s.UnserializeInt();
			forest.NTrees = s.UnserializeInt();
			forest.BufferSize = s.UnserializeInt();
			s.UnserializeRealArray(out forest.Trees);
		}


		/*************************************************************************
		Classification error
		*************************************************************************/

		private static int ClassificationError(this DecisionForest df,
		                                       double[,] xy,
		                                       int npoints)
		{
			int result;

			if (df.NClasses <= 1)
			{
				result = 0;
				return result;
			}
			var x = new double[df.NVars - 1 + 1];
			var y = new double[df.NClasses - 1 + 1];
			result = 0;
			for (int i = 0; i <= npoints - 1; i++)
			{
				for (int j = 0; j <= df.NVars - 1; j++)
				{
					x[j] = xy[i, j];
				}
				df.Process(x, ref y);
				var k = (int) Math.Round(xy[i, df.NVars]);
				int tmpi = 0;
				for (int j = 1; j <= df.NClasses - 1; j++)
				{
					if ((y[j]) > (y[tmpi]))
					{
						tmpi = j;
					}
				}
				if (tmpi != k)
				{
					result = result + 1;
				}
			}
			return result;
		}


		/*************************************************************************
		Internal subroutine for processing one decision tree starting at Offs
		*************************************************************************/

		private static void ProcessInternal(this DecisionForest df,
		                                    int offs,
		                                    double[] x,
		                                    ref double[] y)
		{
			//
			// Set pointer to the root
			//
			int k = offs + 1;

			//
			// Navigate through the tree
			//
			while (true)
			{
				if (df.Trees[k] < 0)
				{
					if (df.NClasses == 1)
					{
						y[0] = y[0] + df.Trees[k + 1];
					}
					else
					{
						var idx = (int) Math.Round(df.Trees[k + 1]);
						y[idx] = y[idx] + 1;
					}
					break;
				}
				if ((x[(int) Math.Round(df.Trees[k])]) < (df.Trees[k + 1]))
				{
					k = k + innerNodeWidth;
				}
				else
				{
					k = offs + (int) Math.Round(df.Trees[k + 2]);
				}
			}
		}


		/*************************************************************************
		Builds one decision tree. Just a wrapper for the DFBuildTreeRec.
		*************************************************************************/

		private static void BuildTree(double[,] xy,
		                              int npoints,
		                              int nvars,
		                              int nclasses,
		                              int nfeatures,
		                              int nvarsinpool,
		                              int flags,
		                              InternalBuffers bufs)
		{
			if (!(npoints > 0))
				throw new Exception("Assertion failed");

			//
			// Prepare IdxBuf. It stores indices of the training set elements.
			// When training set is being split, contents of IdxBuf is
			// correspondingly reordered so we can know which elements belong
			// to which branch of decision tree.
			//
			for (int i = 0; i <= npoints - 1; i++)
			{
				bufs.IdxBuffer[i] = i;
			}

			//
			// Recursive procedure
			//
			int numprocessed = 1;
			if (!(npoints > 0))
				throw new Exception("Assertion Failed");
			BuildTreeRecursive(xy, nvars, nclasses, nfeatures, nvarsinpool, flags, ref numprocessed, 0, npoints - 1, bufs);
			bufs.TreeBuffer[0] = numprocessed;
		}


		/*************************************************************************
		Builds one decision tree (internal recursive subroutine)

		Parameters:
			TreeBuf     -   large enough array, at least TreeSize
			IdxBuf      -   at least NPoints elements
			TmpBufR     -   at least NPoints
			TmpBufR2    -   at least NPoints
			TmpBufI     -   at least NPoints
			TmpBufI2    -   at least NPoints+1
		*************************************************************************/

		private static void BuildTreeRecursive(double[,] xy,
		                                       int nvars,
		                                       int nclasses,
		                                       int nfeatures,
		                                       int nvarsinpool,
		                                       int flags,
		                                       ref int numprocessed,
		                                       int idx1,
		                                       int idx2,
		                                       InternalBuffers bufs)
		{
			int i;
			int j;
			double ebest;
			double v;


			//
			// these initializers are not really necessary,
			// but without them compiler complains about uninitialized locals
			//
			double tbest = 0;

			//
			// Prepare
			//
			if (!(idx2 >= idx1))
				throw new Exception("BuildTreeRecursive failed - idx2 < idx1. Maybe you should use smaller RDFMath.Epsilon.");
			bool useevs = flags/useEvs%2 != 0;

			//
			// Leaf node
			//
			if (idx2 == idx1)
			{
				bufs.TreeBuffer[numprocessed] = -1;
				bufs.TreeBuffer[numprocessed + 1] = xy[bufs.IdxBuffer[idx1], nvars];
				numprocessed = numprocessed + leafNodeWidth;
				return;
			}

			//
			// Non-leaf node.
			// Select random variable, prepare split:
			// 1. prepare default solution - no splitting, class at random
			// 2. investigate possible splits, compare with default/best
			//
			int idxbest = -1;
			if (nclasses > 1)
			{
				//
				// default solution for classification
				//
				for (i = 0; i <= nclasses - 1; i++)
				{
					bufs.ClassIBuffer[i] = 0;
				}
				double s = idx2 - idx1 + 1;
				for (i = idx1; i <= idx2; i++)
				{
					j = (int) Math.Round(xy[bufs.IdxBuffer[i], nvars]);
					bufs.ClassIBuffer[j] = bufs.ClassIBuffer[j] + 1;
				}
				ebest = 0;
				for (i = 0; i <= nclasses - 1; i++)
				{
					ebest = ebest + bufs.ClassIBuffer[i]*RDFMath.Sqr(1 - bufs.ClassIBuffer[i]/s) +
					        (s - bufs.ClassIBuffer[i])*RDFMath.Sqr(bufs.ClassIBuffer[i]/s);
				}
				ebest = Math.Sqrt(ebest/(nclasses*(idx2 - idx1 + 1)));
			}
			else
			{
				//
				// default solution for regression
				//
				v = 0;
				for (i = idx1; i <= idx2; i++)
				{
					v = v + xy[bufs.IdxBuffer[i], nvars];
				}
				v = v/(idx2 - idx1 + 1);
				ebest = 0;
				for (i = idx1; i <= idx2; i++)
				{
					ebest = ebest + RDFMath.Sqr(xy[bufs.IdxBuffer[i], nvars] - v);
				}
				ebest = Math.Sqrt(ebest/(idx2 - idx1 + 1));
			}

			// Check if all vectors have same class
			if (Math.Abs(ebest - 0) < 1e-10)
			{
				//
				// Leaf Node.
				//
				bufs.TreeBuffer[numprocessed] = -1;
				if (nclasses > 1)
				{
					bufs.TreeBuffer[numprocessed + 1] =
						(int)Math.Round(xy[bufs.IdxBuffer[idx1], nvars]);
				}
				else
				{
					bufs.TreeBuffer[numprocessed + 1] = xy[bufs.IdxBuffer[i], nvars];
				}
				numprocessed = numprocessed + leafNodeWidth;
				return;
			}

			i = 0;
			while (i <= Math.Min(nfeatures, nvarsinpool) - 1)
			{
				//
				// select variables from pool
				//
				j = i + RDFMath.RandomInteger(nvarsinpool - i);
				int k = bufs.VarPool[i];
				bufs.VarPool[i] = bufs.VarPool[j];
				bufs.VarPool[j] = k;
				int varcur = bufs.VarPool[i];

				//
				// load variable values to working array
				//
				// apply EVS preprocessing: if all variable values are same,
				// variable is excluded from pool.
				//
				// This is necessary for binary pre-splits (see later) to work.
				//
				for (j = idx1; j <= idx2; j++)
				{
					bufs.TempBufferR[j - idx1] = xy[bufs.IdxBuffer[j], varcur];
				}
				if (useevs)
				{
					bool bflag = false;
					v = bufs.TempBufferR[0];
					for (j = 0; j <= idx2 - idx1; j++)
					{
						if (Math.Abs((bufs.TempBufferR[j]) - (v)) > RDFMath.Epsilon)
						{
							bflag = true;
							break;
						}
					}
					if (!bflag)
					{
						//
						// exclude variable from pool,
						// go to the next iteration.
						// I is not increased.
						//
						k = bufs.VarPool[i];
						bufs.VarPool[i] = bufs.VarPool[nvarsinpool - 1];
						bufs.VarPool[nvarsinpool - 1] = k;
						nvarsinpool = nvarsinpool - 1;
						continue;
					}
				}

				//
				// load labels to working array
				//
				if (nclasses > 1)
				{
					for (j = idx1; j <= idx2; j++)
					{
						bufs.TempBufferI[j - idx1] = (int) Math.Round(xy[bufs.IdxBuffer[j], nvars]);
					}
				}
				else
				{
					for (j = idx1; j <= idx2; j++)
					{
						bufs.TempBufferR2[j - idx1] = xy[bufs.IdxBuffer[j], nvars];
					}
				}

				//
				// calculate split
				//
				double currms;
				double threshold;
				int info;
				if (useevs && bufs.EvsBin[varcur])
				{
					//
					// Pre-calculated splits for binary variables.
					// Threshold is already known, just calculate RMS error
					//
					threshold = bufs.EvsSplits[varcur];
					double sr;
					double sl;
					if (nclasses > 1)
					{
						//
						// classification-specific code
						//
						for (j = 0; j <= 2*nclasses - 1; j++)
						{
							bufs.ClassIBuffer[j] = 0;
						}
						sl = 0;
						sr = 0;
						for (j = 0; j <= idx2 - idx1; j++)
						{
							k = bufs.TempBufferI[j];
							if ((bufs.TempBufferR[j]) < (threshold))
							{
								bufs.ClassIBuffer[k] = bufs.ClassIBuffer[k] + 1;
								sl = sl + 1;
							}
							else
							{
								bufs.ClassIBuffer[k + nclasses] = bufs.ClassIBuffer[k + nclasses] + 1;
								sr = sr + 1;
							}
						}
						if ((Math.Abs((sl) - (0)) < RDFMath.Epsilon || Math.Abs((sr) - (0)) < RDFMath.Epsilon))
							throw new Exception("DFBuildTreeRec: something strange!");
						currms = 0;
						for (j = 0; j <= nclasses - 1; j++)
						{
							double w = bufs.ClassIBuffer[j];
							currms = currms + w*RDFMath.Sqr(w/sl - 1);
							currms = currms + (sl - w)*RDFMath.Sqr(w/sl);
							w = bufs.ClassIBuffer[nclasses + j];
							currms = currms + w*RDFMath.Sqr(w/sr - 1);
							currms = currms + (sr - w)*RDFMath.Sqr(w/sr);
						}
						currms = Math.Sqrt(currms/(nclasses*(idx2 - idx1 + 1)));
					}
					else
					{
						//
						// regression-specific code
						//
						sl = 0;
						sr = 0;
						double v1 = 0;
						double v2 = 0;
						for (j = 0; j <= idx2 - idx1; j++)
						{
							if ((bufs.TempBufferR[j]) < (threshold))
							{
								v1 = v1 + bufs.TempBufferR2[j];
								sl = sl + 1;
							}
							else
							{
								v2 = v2 + bufs.TempBufferR2[j];
								sr = sr + 1;
							}
						}
						if ((Math.Abs((sl) - (0)) < RDFMath.Epsilon || Math.Abs((sr) - (0)) < RDFMath.Epsilon))
							throw new Exception("DFBuildTreeRec: something strange!");
						v1 = v1/sl;
						v2 = v2/sr;
						currms = 0;
						for (j = 0; j <= idx2 - idx1; j++)
						{
							if ((bufs.TempBufferR[j]) < (threshold))
							{
								currms = currms + RDFMath.Sqr(v1 - bufs.TempBufferR2[j]);
							}
							else
							{
								currms = currms + RDFMath.Sqr(v2 - bufs.TempBufferR2[j]);
							}
						}
						currms = Math.Sqrt(currms/(idx2 - idx1 + 1));
					}
					info = 1;
				}
				else
				{
					//
					// Generic splits
					//
					if (nclasses > 1)
					{
						SplitC(ref bufs.TempBufferR, ref bufs.TempBufferI, ref bufs.ClassIBuffer, idx2 - idx1 + 1, nclasses, useStrongSplits,
						       out info, out threshold, out currms, ref bufs.SortRBuffer, ref bufs.SortIBuffer);
					}
					else
					{
						SplitR(ref bufs.TempBufferR, ref bufs.TempBufferR2, idx2 - idx1 + 1, useStrongSplits, out info, out threshold,
						       out currms, ref bufs.SortRBuffer, ref bufs.SortRBuffer2);
					}
				}
				if (info > 0)
				{
					if ((currms) <= (ebest))
					{
						ebest = currms;
						idxbest = varcur;
						tbest = threshold;
					}
				}

				//
				// Next iteration
				//
				i = i + 1;
			}

			//
			// to split or not to split
			//
			if (idxbest < 0)
			{
				//
				// All values are same, cannot split.
				//
				bufs.TreeBuffer[numprocessed] = -1;
				if (nclasses > 1)
				{
					//
					// Select random class label (randomness allows us to
					// approximate distribution of the classes)
					//
					bufs.TreeBuffer[numprocessed + 1] =
						(int) Math.Round(xy[bufs.IdxBuffer[idx1 + RDFMath.RandomInteger(idx2 - idx1 + 1)], nvars]);
				}
				else
				{
					//
					// Select average (for regression task).
					//
					v = 0;
					for (i = idx1; i <= idx2; i++)
					{
						v = v + xy[bufs.IdxBuffer[i], nvars]/(idx2 - idx1 + 1);
					}
					bufs.TreeBuffer[numprocessed + 1] = v;
				}
				numprocessed = numprocessed + leafNodeWidth;
			}
			else
			{
				//
				// we can split
				//
				bufs.TreeBuffer[numprocessed] = idxbest;
				bufs.TreeBuffer[numprocessed + 1] = tbest;
				int i1 = idx1;
				int i2 = idx2;
				while (i1 <= i2)
				{
					//
					// Reorder indices so that left partition is in [Idx1..I1-1],
					// and right partition is in [I2+1..Idx2]
					//
					if ((xy[bufs.IdxBuffer[i1], idxbest]) < (tbest))
					{
						i1 = i1 + 1;
						continue;
					}
					if ((xy[bufs.IdxBuffer[i2], idxbest]) >= (tbest))
					{
						i2 = i2 - 1;
						continue;
					}
					j = bufs.IdxBuffer[i1];
					bufs.IdxBuffer[i1] = bufs.IdxBuffer[i2];
					bufs.IdxBuffer[i2] = j;
					i1 = i1 + 1;
					i2 = i2 - 1;
				}
				int oldnp = numprocessed;
				numprocessed = numprocessed + innerNodeWidth;
				BuildTreeRecursive(xy, nvars, nclasses, nfeatures, nvarsinpool, flags, ref numprocessed, idx1, i1 - 1, bufs);
				bufs.TreeBuffer[oldnp + 2] = numprocessed;
				BuildTreeRecursive(xy, nvars, nclasses, nfeatures, nvarsinpool, flags, ref numprocessed, i2 + 1, idx2, bufs);
			}
		}


		/*************************************************************************
		Makes split on attribute
		*************************************************************************/

		private static void SplitC(ref double[] x,
		                           ref int[] c,
		                           ref int[] cntbuf,
		                           int n,
		                           int nc,
		                           int flags,
		                           out int info,
		                           out double threshold,
		                           out double e,
		                           ref double[] sortrbuf,
		                           ref int[] sortibuf)
		{
			int q;
			int qmin;
			int qmax;
			int qcnt;

			TagSort.TagSortFastI(ref x, ref c, ref sortrbuf, ref sortibuf, n);
			e = RDFMath.MaxRealNumber;
			threshold = 0.5*(x[0] + x[n - 1]);
			info = -3;
			if (flags/useStrongSplits%2 == 0)
			{
				//
				// weak splits, split at half
				//
				qcnt = 2;
				qmin = 1;
				qmax = 1;
			}
			else
			{
				//
				// strong splits: choose best quartile
				//
				qcnt = 4;
				qmin = 1;
				qmax = 3;
			}
			for (q = qmin; q <= qmax; q++)
			{
				double cursplit = x[n*q/qcnt];
				int neq = 0;
				int nless = 0;
				int ngreater = 0;
				for (int i = 0; i <= n - 1; i++)
				{
					if ((x[i]) < (cursplit))
					{
						nless = nless + 1;
					}
					if (Math.Abs((x[i]) - (cursplit)) < RDFMath.Epsilon)
					{
						neq = neq + 1;
					}
					if ((x[i]) > (cursplit))
					{
						ngreater = ngreater + 1;
					}
				}
				if (neq == 0)
					throw new Exception("DFSplitR: NEq=0, something strange!!!");
				if (nless != 0 || ngreater != 0)
				{
					//
					// set threshold between two partitions, with
					// some tweaking to avoid problems with floating point
					// arithmetics.
					//
					// The problem is that when you calculates C = 0.5*(A+B) there
					// can be no C which lies strictly between A and B (for example,
					// there is no floating point number which is
					// greater than 1 and less than 1+eps). In such situations
					// we choose right side as theshold (remember that
					// points which lie on threshold falls to the right side).
					//
					int nleft;
					if (nless < ngreater)
					{
						cursplit = 0.5*(x[nless + neq - 1] + x[nless + neq]);
						nleft = nless + neq;
						if ((cursplit) <= (x[nless + neq - 1]))
						{
							cursplit = x[nless + neq];
						}
					}
					else
					{
						cursplit = 0.5*(x[nless - 1] + x[nless]);
						nleft = nless;
						if ((cursplit) <= (x[nless - 1]))
						{
							cursplit = x[nless];
						}
					}
					info = 1;
					for (int i = 0; i <= 2*nc - 1; i++)
					{
						cntbuf[i] = 0;
					}
					for (int i = 0; i <= nleft - 1; i++)
					{
						cntbuf[c[i]] = cntbuf[c[i]] + 1;
					}
					for (int i = nleft; i <= n - 1; i++)
					{
						cntbuf[nc + c[i]] = cntbuf[nc + c[i]] + 1;
					}
					double sl = nleft;
					double sr = n - nleft;
					double v = 0;
					for (int i = 0; i <= nc - 1; i++)
					{
						double w = cntbuf[i];
						v = v + w*RDFMath.Sqr(w/sl - 1);
						v = v + (sl - w)*RDFMath.Sqr(w/sl);
						w = cntbuf[nc + i];
						v = v + w*RDFMath.Sqr(w/sr - 1);
						v = v + (sr - w)*RDFMath.Sqr(w/sr);
					}
					double cure = Math.Sqrt(v/(nc*n));
					if ((cure) < (e))
					{
						threshold = cursplit;
						e = cure;
					}
				}
			}
		}


		/*************************************************************************
		Makes split on attribute
		*************************************************************************/

		private static void SplitR(ref double[] x,
		                           ref double[] y,
		                           int n,
		                           int flags,
		                           out int info,
		                           out double threshold,
		                           out double e,
		                           ref double[] sortrbuf,
		                           ref double[] sortrbuf2)
		{
			int q;
			int qmin;
			int qmax;
			int qcnt;

			TagSort.TagSortFastR(ref x, ref y, ref sortrbuf, ref sortrbuf2, n);
			e = RDFMath.MaxRealNumber;
			threshold = 0.5*(x[0] + x[n - 1]);
			info = -3;
			if (flags/useStrongSplits%2 == 0)
			{
				//
				// weak splits, split at half
				//
				qcnt = 2;
				qmin = 1;
				qmax = 1;
			}
			else
			{
				//
				// strong splits: choose best quartile
				//
				qcnt = 4;
				qmin = 1;
				qmax = 3;
			}
			for (q = qmin; q <= qmax; q++)
			{
				double cursplit = x[n*q/qcnt];
				int neq = 0;
				int nless = 0;
				int ngreater = 0;
				for (int i = 0; i <= n - 1; i++)
				{
					if ((x[i]) < (cursplit))
					{
						nless = nless + 1;
					}
					if (Math.Abs((x[i]) - (cursplit)) < RDFMath.Epsilon)
					{
						neq = neq + 1;
					}
					if ((x[i]) > (cursplit))
					{
						ngreater = ngreater + 1;
					}
				}
				if (neq == 0)
					throw new Exception("DFSplitR: NEq=0, something strange!!!");
				if (nless != 0 || ngreater != 0)
				{
					//
					// set threshold between two partitions, with
					// some tweaking to avoid problems with floating point
					// arithmetics.
					//
					// The problem is that when you calculates C = 0.5*(A+B) there
					// can be no C which lies strictly between A and B (for example,
					// there is no floating point number which is
					// greater than 1 and less than 1+eps). In such situations
					// we choose right side as theshold (remember that
					// points which lie on threshold falls to the right side).
					//
					int nleft;
					if (nless < ngreater)
					{
						cursplit = 0.5*(x[nless + neq - 1] + x[nless + neq]);
						nleft = nless + neq;
						if ((cursplit) <= (x[nless + neq - 1]))
						{
							cursplit = x[nless + neq];
						}
					}
					else
					{
						cursplit = 0.5*(x[nless - 1] + x[nless]);
						nleft = nless;
						if ((cursplit) <= (x[nless - 1]))
						{
							cursplit = x[nless];
						}
					}
					info = 1;
					double cure = 0;
					double v = 0;
					for (int i = 0; i <= nleft - 1; i++)
					{
						v = v + y[i];
					}
					v = v/nleft;
					for (int i = 0; i <= nleft - 1; i++)
					{
						cure = cure + RDFMath.Sqr(y[i] - v);
					}
					v = 0;
					for (int i = nleft; i <= n - 1; i++)
					{
						v = v + y[i];
					}
					v = v/(n - nleft);
					for (int i = nleft; i <= n - 1; i++)
					{
						cure = cure + RDFMath.Sqr(y[i] - v);
					}
					cure = Math.Sqrt(cure/n);
					if ((cure) < (e))
					{
						threshold = cursplit;
						e = cure;
					}
				}
			}
		}
	}
}