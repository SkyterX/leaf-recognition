using System;

namespace LeafRecognition.Recognition.Classification.RDFLib
{
	internal static class TagSort
	{
		/*************************************************************************
		Same as TagSort, but optimized for real keys and integer labels.

		A is sorted, and same permutations are applied to B.

		NOTES:
		1.  this function assumes that A[] is finite; it doesn't checks that
			condition. All other conditions (size of input arrays, etc.) are not
			checked too.
		2.  this function uses two buffers, BufA and BufB, each is N elements large.
			They may be preallocated (which will save some time) or not, in which
			case function will automatically allocate memory.

		  -- ALGLIB --
			 Copyright 11.12.2008 by Bochkanov Sergey
		*************************************************************************/

		public static void TagSortFastI(ref double[] a,
		                                ref int[] b,
		                                ref double[] bufa,
		                                ref int[] bufb,
		                                int n)
		{
			//
			// Special case
			//
			if (n <= 1)
			{
				return;
			}

			//
			// Test for already sorted set
			//
			bool isascending = true;
			bool isdescending = true;
			for (int i = 1; i <= n - 1; i++)
			{
				isascending = isascending && a[i] >= a[i - 1];
				isdescending = isdescending && a[i] <= a[i - 1];
			}
			if (isascending)
			{
				return;
			}
			if (isdescending)
			{
				for (int i = 0; i <= n - 1; i++)
				{
					int j = n - 1 - i;
					if (j <= i)
					{
						break;
					}
					double tmpr = a[i];
					a[i] = a[j];
					a[j] = tmpr;
					int tmpi = b[i];
					b[i] = b[j];
					b[j] = tmpi;
				}
				return;
			}

			//
			// General case
			//
			if (bufa.Length < n)
			{
				bufa = new double[n];
			}
			if (bufb.Length < n)
			{
				bufb = new int[n];
			}
			TagSortFastIRecursive(ref a, ref b, ref bufa, ref bufb, 0, n - 1);
		}


		/*************************************************************************
		Same as TagSort, but optimized for real keys and real labels.

		A is sorted, and same permutations are applied to B.

		NOTES:
		1.  this function assumes that A[] is finite; it doesn't checks that
			condition. All other conditions (size of input arrays, etc.) are not
			checked too.
		2.  this function uses two buffers, BufA and BufB, each is N elements large.
			They may be preallocated (which will save some time) or not, in which
			case function will automatically allocate memory.

		  -- ALGLIB --
			 Copyright 11.12.2008 by Bochkanov Sergey
		*************************************************************************/

		public static void TagSortFastR(ref double[] a,
		                                ref double[] b,
		                                ref double[] bufa,
		                                ref double[] bufb,
		                                int n)
		{
			//
			// Special case
			//
			if (n <= 1)
			{
				return;
			}

			//
			// Test for already sorted set
			//
			bool isascending = true;
			bool isdescending = true;
			for (int i = 1; i <= n - 1; i++)
			{
				isascending = isascending && a[i] >= a[i - 1];
				isdescending = isdescending && a[i] <= a[i - 1];
			}
			if (isascending)
			{
				return;
			}
			if (isdescending)
			{
				for (int i = 0; i <= n - 1; i++)
				{
					int j = n - 1 - i;
					if (j <= i)
					{
						break;
					}
					double tmpr = a[i];
					a[i] = a[j];
					a[j] = tmpr;
					tmpr = b[i];
					b[i] = b[j];
					b[j] = tmpr;
				}
				return;
			}

			//
			// General case
			//
			if (bufa.Length < n)
			{
				bufa = new double[n];
			}
			if (bufb.Length < n)
			{
				bufb = new double[n];
			}
			TagSortFastRRecursive(ref a, ref b, ref bufa, ref bufb, 0, n - 1);
		}

		/*************************************************************************
		Internal TagSortFastI: sorts A[I1...I2] (both bounds are included),
		applies same permutations to B.

		  -- ALGLIB --
			 Copyright 06.09.2010 by Bochkanov Sergey
		*************************************************************************/

		private static void TagSortFastIRecursive(ref double[] a,
		                                    ref int[] b,
		                                    ref double[] bufa,
		                                    ref int[] bufb,
		                                    int i1,
		                                    int i2)
		{
			int i;
			int j;
			int k;
			double tmpr;


			//
			// Fast exit
			//
			if (i2 <= i1)
			{
				return;
			}

			//
			// Non-recursive sort for small arrays
			//
			if (i2 - i1 <= 16)
			{
				for (j = i1 + 1; j <= i2; j++)
				{
					//
					// Search elements [I1..J-1] for place to insert Jth element.
					//
					// This code stops immediately if we can leave A[J] at J-th position
					// (all elements have same value of A[J] larger than any of them)
					//
					tmpr = a[j];
					int tmpi = j;
					for (k = j - 1; k >= i1; k--)
					{
						if (a[k] <= tmpr)
						{
							break;
						}
						tmpi = k;
					}
					k = tmpi;

					//
					// Insert Jth element into Kth position
					//
					if (k != j)
					{
						tmpr = a[j];
						tmpi = b[j];
						for (i = j - 1; i >= k; i--)
						{
							a[i + 1] = a[i];
							b[i + 1] = b[i];
						}
						a[k] = tmpr;
						b[k] = tmpi;
					}
				}
				return;
			}

			//
			// Quicksort: choose pivot
			// Here we assume that I2-I1>=2
			//
			double v0 = a[i1];
			double v1 = a[i1 + (i2 - i1)/2];
			double v2 = a[i2];
			if (v0 > v1)
			{
				tmpr = v1;
				v1 = v0;
				v0 = tmpr;
			}
			if (v1 > v2)
			{
				tmpr = v2;
/*
				v2 = v1;
*/
				v1 = tmpr;
			}
			if (v0 > v1)
			{
/*
				tmpr = v1;
*/
				v1 = v0;
/*
				v0 = tmpr;
*/
			}
			double vp = v1;

			//
			// now pass through A/B and:
			// * move elements that are LESS than VP to the left of A/B
			// * move elements that are EQUAL to VP to the right of BufA/BufB (in the reverse order)
			// * move elements that are GREATER than VP to the left of BufA/BufB (in the normal order
			// * move elements from the tail of BufA/BufB to the middle of A/B (restoring normal order)
			// * move elements from the left of BufA/BufB to the end of A/B
			//
			int cntless = 0;
			int cnteq = 0;
			int cntgreater = 0;
			for (i = i1; i <= i2; i++)
			{
				v0 = a[i];
				if (v0 < vp)
				{
					//
					// LESS
					//
					k = i1 + cntless;
					if (i != k)
					{
						a[k] = v0;
						b[k] = b[i];
					}
					cntless = cntless + 1;
					continue;
				}
				if (Math.Abs(v0 - vp) < RDFMath.Epsilon)
				{
					//
					// EQUAL
					//
					k = i2 - cnteq;
					bufa[k] = v0;
					bufb[k] = b[i];
					cnteq = cnteq + 1;
					continue;
				}

				//
				// GREATER
				//
				k = i1 + cntgreater;
				bufa[k] = v0;
				bufb[k] = b[i];
				cntgreater = cntgreater + 1;
			}
			for (i = 0; i <= cnteq - 1; i++)
			{
				j = i1 + cntless + cnteq - 1 - i;
				k = i2 + i - (cnteq - 1);
				a[j] = bufa[k];
				b[j] = bufb[k];
			}
			for (i = 0; i <= cntgreater - 1; i++)
			{
				j = i1 + cntless + cnteq + i;
				k = i1 + i;
				a[j] = bufa[k];
				b[j] = bufb[k];
			}

			//
			// Sort left and right parts of the array (ignoring middle part)
			//
			TagSortFastIRecursive(ref a, ref b, ref bufa, ref bufb, i1, i1 + cntless - 1);
			TagSortFastIRecursive(ref a, ref b, ref bufa, ref bufb, i1 + cntless + cnteq, i2);
		}


		/*************************************************************************
		Internal TagSortFastR: sorts A[I1...I2] (both bounds are included),
		applies same permutations to B.

		  -- ALGLIB --
			 Copyright 06.09.2010 by Bochkanov Sergey
		*************************************************************************/

		private static void TagSortFastRRecursive(ref double[] a,
		                                    ref double[] b,
		                                    ref double[] bufa,
		                                    ref double[] bufb,
		                                    int i1,
		                                    int i2)
		{
			int i;
			int j;
			int k;
			double tmpr;


			//
			// Fast exit
			//
			if (i2 <= i1)
			{
				return;
			}

			//
			// Non-recursive sort for small arrays
			//
			if (i2 - i1 <= 16)
			{
				for (j = i1 + 1; j <= i2; j++)
				{
					//
					// Search elements [I1..J-1] for place to insert Jth element.
					//
					// This code stops immediatly if we can leave A[J] at J-th position
					// (all elements have same value of A[J] larger than any of them)
					//
					tmpr = a[j];
					int tmpi = j;
					for (k = j - 1; k >= i1; k--)
					{
						if (a[k] <= tmpr)
						{
							break;
						}
						tmpi = k;
					}
					k = tmpi;

					//
					// Insert Jth element into Kth position
					//
					if (k != j)
					{
						tmpr = a[j];
						double tmpr2 = b[j];
						for (i = j - 1; i >= k; i--)
						{
							a[i + 1] = a[i];
							b[i + 1] = b[i];
						}
						a[k] = tmpr;
						b[k] = tmpr2;
					}
				}
				return;
			}

			//
			// Quicksort: choose pivot
			// Here we assume that I2-I1>=16
			//
			double v0 = a[i1];
			double v1 = a[i1 + (i2 - i1)/2];
			double v2 = a[i2];
			if (v0 > v1)
			{
				tmpr = v1;
				v1 = v0;
				v0 = tmpr;
			}
			if (v1 > v2)
			{
				tmpr = v2;
/*
				v2 = v1;
*/
				v1 = tmpr;
			}
			if (v0 > v1)
			{
/*
				tmpr = v1;
*/
				v1 = v0;
/*
				v0 = tmpr;
*/
			}
			double vp = v1;

			//
			// now pass through A/B and:
			// * move elements that are LESS than VP to the left of A/B
			// * move elements that are EQUAL to VP to the right of BufA/BufB (in the reverse order)
			// * move elements that are GREATER than VP to the left of BufA/BufB (in the normal order
			// * move elements from the tail of BufA/BufB to the middle of A/B (restoring normal order)
			// * move elements from the left of BufA/BufB to the end of A/B
			//
			int cntless = 0;
			int cnteq = 0;
			int cntgreater = 0;
			for (i = i1; i <= i2; i++)
			{
				v0 = a[i];
				if (v0 < vp)
				{
					//
					// LESS
					//
					k = i1 + cntless;
					if (i != k)
					{
						a[k] = v0;
						b[k] = b[i];
					}
					cntless = cntless + 1;
					continue;
				}
				if (Math.Abs(v0 - vp) < RDFMath.Epsilon)
				{
					//
					// EQUAL
					//
					k = i2 - cnteq;
					bufa[k] = v0;
					bufb[k] = b[i];
					cnteq = cnteq + 1;
					continue;
				}

				//
				// GREATER
				//
				k = i1 + cntgreater;
				bufa[k] = v0;
				bufb[k] = b[i];
				cntgreater = cntgreater + 1;
			}
			for (i = 0; i <= cnteq - 1; i++)
			{
				j = i1 + cntless + cnteq - 1 - i;
				k = i2 + i - (cnteq - 1);
				a[j] = bufa[k];
				b[j] = bufb[k];
			}
			for (i = 0; i <= cntgreater - 1; i++)
			{
				j = i1 + cntless + cnteq + i;
				k = i1 + i;
				a[j] = bufa[k];
				b[j] = bufb[k];
			}

			//
			// Sort left and right parts of the array (ignoring middle part)
			//
			TagSortFastRRecursive(ref a, ref b, ref bufa, ref bufb, i1, i1 + cntless - 1);
			TagSortFastRRecursive(ref a, ref b, ref bufa, ref bufb, i1 + cntless + cnteq, i2);
		}
	}
}