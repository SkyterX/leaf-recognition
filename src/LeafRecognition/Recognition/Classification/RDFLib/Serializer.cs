using System;

namespace LeafRecognition.Recognition.Classification.RDFLib
{
	internal class Serializer
	{
		private const int serEntriesPerRow = 5;
		private const int serEntryLength = 11;

		private static readonly char[] sixbits2CharTbl = new[]
		                                                 	{
		                                                 		'0', '1', '2', '3', '4', '5', '6', '7',
		                                                 		'8', '9', 'A', 'B', 'C', 'D', 'E', 'F',
		                                                 		'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N',
		                                                 		'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V',
		                                                 		'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd',
		                                                 		'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
		                                                 		'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
		                                                 		'u', 'v', 'w', 'x', 'y', 'z', '-', '_'
		                                                 	};

		private static readonly int[] char2SixbitsTbl = new[]
		                                                	{
		                                                		-1, -1, -1, -1, -1, -1, -1, -1,
		                                                		-1, -1, -1, -1, -1, -1, -1, -1,
		                                                		-1, -1, -1, -1, -1, -1, -1, -1,
		                                                		-1, -1, -1, -1, -1, -1, -1, -1,
		                                                		-1, -1, -1, -1, -1, -1, -1, -1,
		                                                		-1, -1, -1, -1, -1, 62, -1, -1,
		                                                		0, 1, 2, 3, 4, 5, 6, 7,
		                                                		8, 9, -1, -1, -1, -1, -1, -1,
		                                                		-1, 10, 11, 12, 13, 14, 15, 16,
		                                                		17, 18, 19, 20, 21, 22, 23, 24,
		                                                		25, 26, 27, 28, 29, 30, 31, 32,
		                                                		33, 34, 35, -1, -1, -1, -1, 63,
		                                                		-1, 36, 37, 38, 39, 40, 41, 42,
		                                                		43, 44, 45, 46, 47, 48, 49, 50,
		                                                		51, 52, 53, 54, 55, 56, 57, 58,
		                                                		59, 60, 61, -1, -1, -1, -1, -1
		                                                	};

		private int bytesAsked;
		private int bytesRead;
		private int bytesWritten;
		private int entriesNeeded;
		private int entriesSaved;
		private char[] inStr;
		private SerializationMode mode;
		private char[] outStr;

		public Serializer()
		{
			mode = SerializationMode.Default;
			entriesNeeded = 0;
			bytesAsked = 0;
		}

		public void AllocStart()
		{
			entriesNeeded = 0;
			bytesAsked = 0;
			mode = SerializationMode.Alloc;
		}

		public void AllocEntry()
		{
			if (mode != SerializationMode.Alloc)
				throw new Exception("ALGLIB: internal error during (un)serialization");
			entriesNeeded++;
		}

		private int GetAllocSize()
		{
			// check and change mode
			if (mode != SerializationMode.Alloc)
				throw new Exception("ALGLIB: internal error during (un)serialization");

			// if no entries needes (degenerate case)
			if (entriesNeeded == 0)
			{
				bytesAsked = 1;
				return bytesAsked;
			}

			// non-degenerate case
			int rows = entriesNeeded/serEntriesPerRow;
			int lastrowsize = serEntriesPerRow;
			if (entriesNeeded%serEntriesPerRow != 0)
			{
				lastrowsize = entriesNeeded%serEntriesPerRow;
				rows++;
			}

			// calculate result size
			int result = ((rows - 1)*serEntriesPerRow + lastrowsize)*serEntryLength;
			result += (rows - 1)*(serEntriesPerRow - 1) + (lastrowsize - 1);
			result += rows*2;
			bytesAsked = result;
			return result;
		}

		public void SStartStr()
		{
			int allocsize = GetAllocSize();

			// check and change mode
			if (mode != SerializationMode.Alloc)
				throw new Exception("ALGLIB: internal error during (un)serialization");
			mode = SerializationMode.SToString;

			// other preparations
			outStr = new char[allocsize];
			entriesSaved = 0;
			bytesWritten = 0;
		}

		public void UStartStr(string s)
		{
			// check and change mode
			if (mode != SerializationMode.Default)
				throw new Exception("ALGLIB: internal error during (un)serialization");
			mode = SerializationMode.SFromString;

			inStr = s.ToCharArray();
			bytesRead = 0;
		}

		public void SerializeInt(int v)
		{
			if (mode != SerializationMode.SToString)
				throw new Exception("ALGLIB: internal error during (un)serialization");
			Int2Str(v, outStr, ref bytesWritten);
			entriesSaved++;
			if (entriesSaved%serEntriesPerRow != 0)
			{
				outStr[bytesWritten] = ' ';
				bytesWritten++;
			}
			else
			{
				outStr[bytesWritten + 0] = '\r';
				outStr[bytesWritten + 1] = '\n';
				bytesWritten += 2;
			}
		}

		public void SerializeDouble(double v)
		{
			if (mode != SerializationMode.SToString)
				throw new Exception("ALGLIB: internal error during (un)serialization");
			Double2Str(v, outStr, ref bytesWritten);
			entriesSaved++;
			if (entriesSaved%serEntriesPerRow != 0)
			{
				outStr[bytesWritten] = ' ';
				bytesWritten++;
			}
			else
			{
				outStr[bytesWritten + 0] = '\r';
				outStr[bytesWritten + 1] = '\n';
				bytesWritten += 2;
			}
		}

		public int UnserializeInt()
		{
			if (mode != SerializationMode.SFromString)
				throw new Exception("ALGLIB: internal error during (un)serialization");
			return Str2Int(inStr, ref bytesRead);
		}

		public double UnserializeDouble()
		{
			if (mode != SerializationMode.SFromString)
				throw new Exception("ALGLIB: internal error during (un)serialization");
			return Str2Double(inStr, ref bytesRead);
		}

		public string GetString()
		{
			return new string(outStr, 0, bytesWritten);
		}


		/************************************************************************
		This function converts six-bit value (from 0 to 63)  to  character  (only
		digits, lowercase and uppercase letters, minus and underscore are used).

		If v is negative or greater than 63, this function returns '?'.
		************************************************************************/

		private static char Sixbits2Char(int v)
		{
			if (v < 0 || v > 63)
				return '?';
			return sixbits2CharTbl[v];
		}

		/************************************************************************
		This function converts character to six-bit value (from 0 to 63).

		This function is inverse of ae_sixbits2char()
		If c is not correct character, this function returns -1.
		************************************************************************/

		private static int Char2Sixbits(char c)
		{
			return (c >= 0 && c < 127) ? char2SixbitsTbl[c] : -1;
		}

		/************************************************************************
		This function converts three bytes (24 bits) to four six-bit values 
		(24 bits again).

		src         array
		src_offs    offset of three-bytes chunk
		dst         array for ints
		dst_offs    offset of four-ints chunk
		************************************************************************/

		private static void Threebytes2Foursixbits(byte[] src, int srcOffs, int[] dst, int dstOffs)
		{
			dst[dstOffs + 0] = src[srcOffs + 0] & 0x3F;
			dst[dstOffs + 1] = (src[srcOffs + 0] >> 6) | ((src[srcOffs + 1] & 0x0F) << 2);
			dst[dstOffs + 2] = (src[srcOffs + 1] >> 4) | ((src[srcOffs + 2] & 0x03) << 4);
			dst[dstOffs + 3] = src[srcOffs + 2] >> 2;
		}

		/************************************************************************
		This function converts four six-bit values (24 bits) to three bytes
		(24 bits again).

		src         pointer to four ints
		src_offs    offset of the chunk
		dst         pointer to three bytes
		dst_offs    offset of the chunk
		************************************************************************/

		private static void Foursixbits2Threebytes(int[] src, int srcOffs, byte[] dst, int dstOffs)
		{
			dst[dstOffs + 0] = (byte) (src[srcOffs + 0] | ((src[srcOffs + 1] & 0x03) << 6));
			dst[dstOffs + 1] = (byte) ((src[srcOffs + 1] >> 2) | ((src[srcOffs + 2] & 0x0F) << 4));
			dst[dstOffs + 2] = (byte) ((src[srcOffs + 2] >> 4) | (src[srcOffs + 3] << 2));
		}

		/************************************************************************
		This function serializes integer value into buffer

		v           integer value to be serialized
		buf         buffer, at least 11 characters wide 
		offs        offset in the buffer
        
		after return from this function, offs points to the char's past the value
		being read.

		This function raises an error in case unexpected symbol is found
		************************************************************************/

		private static void Int2Str(int v, char[] buf, ref int offs)
		{
			int i;
			byte[] oldBytes = BitConverter.GetBytes(v);
			var newBytes = new byte[9];
			var sixbits = new int[12];

			//
			// copy v to array of bytes, sign extending it and 
			// converting to little endian order. Additionally, 
			// we set 9th byte to zero in order to simplify 
			// conversion to six-bit representation
			//
			if (!BitConverter.IsLittleEndian)
				Array.Reverse(oldBytes);
			byte c = v < 0 ? (byte) 0xFF : (byte) 0x00;
			for (i = 0; i < sizeof (int); i++)
				newBytes[i] = oldBytes[i];
			for (i = sizeof (int); i < 8; i++)
				newBytes[i] = c;
			newBytes[8] = 0;

			//
			// convert to six-bit representation, output
			//
			// NOTE: last 12th element of sixbits is always zero, we do not output it
			//
			Threebytes2Foursixbits(newBytes, 0, sixbits, 0);
			Threebytes2Foursixbits(newBytes, 3, sixbits, 4);
			Threebytes2Foursixbits(newBytes, 6, sixbits, 8);
			for (i = 0; i < serEntryLength; i++)
				buf[offs + i] = Sixbits2Char(sixbits[i]);
			offs += serEntryLength;
		}

		/************************************************************************
		This function unserializes integer value from string

		buf         buffer which contains value; leading spaces/tabs/newlines are 
					ignored, traling spaces/tabs/newlines are treated as  end  of
					the integer value.
		offs        offset in the buffer
        
		after return from this function, offs points to the char's past the value
		being read.

		This function raises an error in case unexpected symbol is found
		************************************************************************/

		private static int Str2Int(char[] buf, ref int offs)
		{
			const string errorMessage = "ALGLIB: unable to read integer value from stream";
			const string errorMeseage3264 = "ALGLIB: unable to read integer value from stream (value does not fit into 32 bits)";
			var sixbits = new int[12];
			var oldBytes = new byte[9];
			var newBytes = new byte[sizeof (int)];
			int i;

			// 
			// 1. skip leading spaces
			// 2. read and decode six-bit digits
			// 3. set trailing digits to zeros
			// 4. convert to little endian 64-bit integer representation
			// 5. check that we fit into int
			// 6. convert to big endian representation, if needed
			//
			int sixbitsread = 0;
			while (buf[offs] == ' ' || buf[offs] == '\t' || buf[offs] == '\n' || buf[offs] == '\r')
				offs++;
			while (buf[offs] != ' ' && buf[offs] != '\t' && buf[offs] != '\n' && buf[offs] != '\r' && buf[offs] != 0)
			{
				int d = Char2Sixbits(buf[offs]);
				if (d < 0 || sixbitsread >= serEntryLength)
					throw new Exception(errorMessage);
				sixbits[sixbitsread] = d;
				sixbitsread++;
				offs++;
			}
			if (sixbitsread == 0)
				throw new Exception(errorMessage);
			for (i = sixbitsread; i < 12; i++)
				sixbits[i] = 0;
			Foursixbits2Threebytes(sixbits, 0, oldBytes, 0);
			Foursixbits2Threebytes(sixbits, 4, oldBytes, 3);
			Foursixbits2Threebytes(sixbits, 8, oldBytes, 6);
			byte c = (oldBytes[sizeof (int) - 1] & 0x80) != 0 ? (byte) 0xFF : (byte) 0x00;
			for (i = sizeof (int); i < 8; i++)
				if (oldBytes[i] != c)
					throw new Exception(errorMeseage3264);
			for (i = 0; i < sizeof (int); i++)
				newBytes[i] = oldBytes[i];
			if (!BitConverter.IsLittleEndian)
				Array.Reverse(newBytes);
			return BitConverter.ToInt32(newBytes, 0);
		}


		/************************************************************************
		This function serializes double value into buffer

		v           double value to be serialized
		buf         buffer, at least 11 characters wide 
		offs        offset in the buffer
        
		after return from this function, offs points to the char's past the value
		being read.
		************************************************************************/

		private static void Double2Str(double v, char[] buf, ref int offs)
		{
			int i;
			var sixbits = new int[12];
			var newBytes = new byte[9];

			//
			// handle special quantities
			//
			if (Double.IsNaN(v))
			{
				buf[offs + 0] = '.';
				buf[offs + 1] = 'n';
				buf[offs + 2] = 'a';
				buf[offs + 3] = 'n';
				buf[offs + 4] = '_';
				buf[offs + 5] = '_';
				buf[offs + 6] = '_';
				buf[offs + 7] = '_';
				buf[offs + 8] = '_';
				buf[offs + 9] = '_';
				buf[offs + 10] = '_';
				offs += serEntryLength;
				return;
			}
			if (Double.IsPositiveInfinity(v))
			{
				buf[offs + 0] = '.';
				buf[offs + 1] = 'p';
				buf[offs + 2] = 'o';
				buf[offs + 3] = 's';
				buf[offs + 4] = 'i';
				buf[offs + 5] = 'n';
				buf[offs + 6] = 'f';
				buf[offs + 7] = '_';
				buf[offs + 8] = '_';
				buf[offs + 9] = '_';
				buf[offs + 10] = '_';
				offs += serEntryLength;
				return;
			}
			if (Double.IsNegativeInfinity(v))
			{
				buf[offs + 0] = '.';
				buf[offs + 1] = 'n';
				buf[offs + 2] = 'e';
				buf[offs + 3] = 'g';
				buf[offs + 4] = 'i';
				buf[offs + 5] = 'n';
				buf[offs + 6] = 'f';
				buf[offs + 7] = '_';
				buf[offs + 8] = '_';
				buf[offs + 9] = '_';
				buf[offs + 10] = '_';
				offs += serEntryLength;
				return;
			}

			//
			// process general case:
			// 1. copy v to array of chars
			// 2. set 9th byte to zero in order to simplify conversion to six-bit representation
			// 3. convert to little endian (if needed)
			// 4. convert to six-bit representation
			//    (last 12th element of sixbits is always zero, we do not output it)
			//
			byte[] oldBytes = BitConverter.GetBytes(v);
			if (!BitConverter.IsLittleEndian)
				Array.Reverse(oldBytes);
			for (i = 0; i < sizeof (double); i++)
				newBytes[i] = oldBytes[i];
			for (i = sizeof (double); i < 9; i++)
				newBytes[i] = 0;
			Threebytes2Foursixbits(newBytes, 0, sixbits, 0);
			Threebytes2Foursixbits(newBytes, 3, sixbits, 4);
			Threebytes2Foursixbits(newBytes, 6, sixbits, 8);
			for (i = 0; i < serEntryLength; i++)
				buf[offs + i] = Sixbits2Char(sixbits[i]);
			offs += serEntryLength;
		}

		/************************************************************************
		This function unserializes double value from string

		buf         buffer which contains value; leading spaces/tabs/newlines are 
					ignored, traling spaces/tabs/newlines are treated as  end  of
					the double value.
		offs        offset in the buffer
        
		after return from this function, offs points to the char's past the value
		being read.

		This function raises an error in case unexpected symbol is found
		************************************************************************/

		private static double Str2Double(char[] buf, ref int offs)
		{
			const string errorMessage = "ALGLIB: unable to read double value from stream";
			var sixbits = new int[12];
			var oldBytes = new byte[9];
			var newBytes = new byte[sizeof (double)];


			// 
			// skip leading spaces
			//
			while (buf[offs] == ' ' || buf[offs] == '\t' || buf[offs] == '\n' || buf[offs] == '\r')
				offs++;


			//
			// Handle special cases
			//
			if (buf[offs] == '.')
			{
				var s = new string(buf, offs, serEntryLength);
				if (s == ".nan_______")
				{
					offs += serEntryLength;
					return Double.NaN;
				}
				if (s == ".posinf____")
				{
					offs += serEntryLength;
					return Double.PositiveInfinity;
				}
				if (s == ".neginf____")
				{
					offs += serEntryLength;
					return Double.NegativeInfinity;
				}
				throw new Exception(errorMessage);
			}

			// 
			// General case:
			// 1. read and decode six-bit digits
			// 2. check that all 11 digits were read
			// 3. set last 12th digit to zero (needed for simplicity of conversion)
			// 4. convert to 8 bytes
			// 5. convert to big endian representation, if needed
			//
			int sixbitsread = 0;
			while (buf[offs] != ' ' && buf[offs] != '\t' && buf[offs] != '\n' && buf[offs] != '\r' && buf[offs] != 0)
			{
				int d = Char2Sixbits(buf[offs]);
				if (d < 0 || sixbitsread >= serEntryLength)
					throw new Exception(errorMessage);
				sixbits[sixbitsread] = d;
				sixbitsread++;
				offs++;
			}
			if (sixbitsread != serEntryLength)
				throw new Exception(errorMessage);
			sixbits[serEntryLength] = 0;
			Foursixbits2Threebytes(sixbits, 0, oldBytes, 0);
			Foursixbits2Threebytes(sixbits, 4, oldBytes, 3);
			Foursixbits2Threebytes(sixbits, 8, oldBytes, 6);
			for (int i = 0; i < sizeof (double); i++)
				newBytes[i] = oldBytes[i];
			if (!BitConverter.IsLittleEndian)
				Array.Reverse(newBytes);
			return BitConverter.ToDouble(newBytes, 0);
		}

		public void AllocRealArray(double[] v, int n)
		{
			if (n < 0)
			{
				n = v.Length;
			}
			AllocEntry();
			for (int i = 0; i <= n - 1; i++)
			{
				AllocEntry();
			}
		}

		public void SerializeRealArray(double[] v, int n)
		{
			if (n < 0)
			{
				n = v.Length;
			}
			SerializeInt(n);
			for (int i = 0; i <= n - 1; i++)
			{
				SerializeDouble(v[i]);
			}
		}

		public void UnserializeRealArray(out double[] v)
		{
			v = new double[0];

			int n = UnserializeInt();
			if (n == 0)
			{
				return;
			}
			v = new double[n];
			for (int i = 0; i <= n - 1; i++)
			{
				v[i] = UnserializeDouble();
			}
		}

		private enum SerializationMode
		{
			Default,
			Alloc,
			SToString,
			SFromString
		};
	}
}