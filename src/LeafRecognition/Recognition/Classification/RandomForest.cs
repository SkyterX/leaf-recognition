﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using LeafRecognition.Recognition.Classification.RDFLib;

namespace LeafRecognition.Recognition.Classification
{
	[DataContract(Namespace = "LeafRecognition")]
	public class RandomForest : IClassifier
	{
		private DecisionForest forest;

		[DataMember] private readonly string forestData;
		[DataMember] private readonly double trainToTestRatio;
		[DataMember] private readonly double featuresPerSplitRatio;
		[DataMember] private readonly int numberOfTrees;
		[DataMember] private readonly DFReport buildReport;

		private RandomForest(DecisionForest forest)
		{
			this.forest = forest;
		}

		public RandomForest(IList<ClassifiedVector> input, int nClasses, int randomSeed = 482308529,
			int numberOfTrees = 50, double featuresPerSplitRatio = 0.5, double trainToTestRatio = 0.632)
		{
			this.numberOfTrees = numberOfTrees;
			this.trainToTestRatio = trainToTestRatio;
			this.featuresPerSplitRatio = featuresPerSplitRatio;
			RDFMath.RandomSeed = randomSeed;
			var nPoints = input.Count;
			var numberOfVariables = input.Max(md => md.Vector.Length);
			var trainVectorSet = new double[nPoints, numberOfVariables + 1];
			for (var i = 0; i < nPoints; i++)
			{
				for (var j = 0; j < numberOfVariables; j++)
				{
					trainVectorSet[i, j] = Math.Round(input[i].Vector[j], RDFMath.VectorMaxPrecision);
				}
				trainVectorSet[i, numberOfVariables] = input[i].Label;
			}
			int info;
			DFExtensions.BuildRandomDecisionForestX1(
				trainVectorSet,
				nPoints, numberOfVariables, nClasses,
				numberOfTrees, (int) Math.Max(numberOfVariables*featuresPerSplitRatio + 1e-3, 1), trainToTestRatio,
				out info, out forest, out buildReport
				);
			forest.Serialize(out forestData);
		}

		public ClassificationResult Classify(double[] input)
		{
			var result = new double[forest.NClasses];
			forest.Process(input, ref result);
			return new ClassificationResult(result);
		}

		public string Name
		{
			get
			{
				return string.Format("RandomForest-{0}-{1}-{2}", numberOfTrees,
					(int) Math.Ceiling(featuresPerSplitRatio*forest.NVars),
					trainToTestRatio.ToString(CultureInfo.InvariantCulture).Replace(".", ""));
			}
		}

		[OnDeserialized]
		public void Deserialize(StreamingContext context)
		{
			DFExtensions.Unserialize(forestData, out forest);
		}

		public string Description
		{
			get
			{
				return
					string.Format(
						"Random Forest. NClasses = {0}, NVars = {1}, NTrees = {2}, FeaturesPerSplit = {3}, TrainToTestRatio = {4}",
						forest.NClasses, forest.NVars, numberOfTrees, (int) Math.Ceiling(featuresPerSplitRatio*forest.NVars),
						trainToTestRatio.ToString(CultureInfo.InvariantCulture));
			}
		}
	}
}