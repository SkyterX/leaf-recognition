﻿using System.Linq;
using System.Runtime.Serialization;
using LeafRecognition.Util;

namespace LeafRecognition.Recognition.Classification
{
	[DataContract(Namespace = "LeafRecognition")]
	public class ClassificationResult
	{
		[DataMember] public readonly int Label;
		[DataMember] public readonly double[] Confidence;

		public ClassificationResult(double[] confidence)
			:this(confidence.IndexOfMax(x => x), confidence)
		{
		}

		public ClassificationResult(int label, double[] confidence)
		{
			Label = label;
			Confidence = confidence;
		}

		public override string ToString()
		{
			return string.Format("{{{0}, [{1}]}}", Label,
				Confidence.Select(c => c.ToString("#00.0") + "%").AggregateToString(","));
		}
	}
}