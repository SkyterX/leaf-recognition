﻿using System.Runtime.Serialization;
using LeafRecognition.Images;
using LeafRecognition.Recognition.Classification;
using LeafRecognition.Recognition.Vectorization;
using LeafRecognition.Util;

namespace LeafRecognition.Recognition
{
	[DataContract(Namespace = "LeafRecognition")]
	public class ImageRecognizer : IDescribable
	{
		[DataMember] public readonly IClassifier Classifier;
		[DataMember] public readonly IVectorizer Vectorizer;

		public ImageRecognizer(IClassifier classifier, IVectorizer vectorizer)
		{
			Classifier = classifier;
			Vectorizer = vectorizer;
			Description = string.Format("{{ Classifier : {0}; Vectorizer : {1}}}",
				classifier.Description, vectorizer.Description);
		}

		public ClassificationResult Recognize(RgbImage image)
		{
			var imageInfo = new ImageInfo(image);
			var vector = Vectorizer.ToVector(imageInfo);
			return Classifier.Classify(vector);
		}

		public string Description { get; private set; }
	}
}