﻿using System.Drawing;
using System.Runtime.Serialization;
using LeafRecognition.ImageCharacteristics;

namespace LeafRecognition.Recognition
{
	[DataContract(Namespace = "LeafRecognition")]
	[KnownType(typeof (Characteristic<double>))]
	public class CharacteristicsInfo
	{
		[DataMember] private Characteristic<double> leafArea;
		[DataMember] private Characteristic<double> leafDiameter;
		[DataMember] private Characteristic<double> leafPerimeter;
		[DataMember] private Characteristic<double> convexHullArea;
		[DataMember] private Characteristic<double> smoothedLeafArea;

		[DataMember] private Characteristic<double[]> shapeHuMoments; 
		[DataMember] private Characteristic<double[]> borderHuMoments; 
		[DataMember] private Characteristic<double[]> cssFeatures; 

		[DataMember] private Characteristic<SizeF> fitEllipseSize;

		public Characteristic<double> LeafArea { get { return leafArea ?? (leafArea = new LeafArea()); } }
		public Characteristic<double> LeafDiameter { get { return leafDiameter ?? (leafDiameter = new LeafDiameter()); } }
		public Characteristic<double> LeafPerimeter { get { return leafPerimeter ?? (leafPerimeter = new LeafPerimeter()); } }
		public Characteristic<double> ConvexHullArea { get { return convexHullArea ?? (convexHullArea = new ConvexHullArea()); } }
		public Characteristic<double> SmoothedLeafArea { get { return smoothedLeafArea ?? (smoothedLeafArea = new SmoothedLeafArea()); } }

		public Characteristic<double[]> ShapeHuMoments { get { return shapeHuMoments ?? (shapeHuMoments = new ShapeHuMoments()); } }
        public Characteristic<double[]> BorderHuMoments { get { return borderHuMoments ?? (borderHuMoments = new BorderHuMoments()); } }
        public Characteristic<double[]> CssFeatures { get { return cssFeatures ?? (cssFeatures = new CssFeatures()); } }

		public Characteristic<SizeF> FitEllipseSize { get { return fitEllipseSize ?? (fitEllipseSize = new FitEllipseSize()); } }

		public void Initialize(ImageInfo imageInfo)
		{
			LeafArea.Initialize(imageInfo);
			LeafDiameter.Initialize(imageInfo);
			LeafPerimeter.Initialize(imageInfo);
			ConvexHullArea.Initialize(imageInfo);
			SmoothedLeafArea.Initialize(imageInfo);

			ShapeHuMoments.Initialize(imageInfo);
			BorderHuMoments.Initialize(imageInfo);
            CssFeatures.Initialize(imageInfo);

			FitEllipseSize.Initialize(imageInfo);
		}
	}
}