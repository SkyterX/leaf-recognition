﻿using System;
using System.Collections.Generic;
using System.Linq;
using LeafRecognition.Util;

namespace LeafRecognition.Recognition.Tools
{
	public class VectorStatisticsAggregator
	{
		private readonly int length;
		private readonly double[] sum;
		private readonly double[] sum2;
		private int n;
		private readonly Func<double[], string> formatValue;

		public VectorStatisticsAggregator(int length, Func<double[], string> formatValue = null)
		{
			this.length = length;
			sum = new double[length];
			sum2 = new double[length];
			MaxValue = new double[length].Fill(double.MinValue);
			MinValue = new double[length].Fill(double.MaxValue);
			this.formatValue = formatValue ?? ((vals) =>
				string.Format("[{0}]", vals
					.Select(val => string.Format("{0, -8:0.00}", val))
					.AggregateToString(", ")));
		}

		public VectorStatisticsAggregator AddVector(double[] vector)
		{
			for (var i = 0; i < length; i++)
			{
				sum[i] += vector[i];
				sum2[i] += vector[i]*vector[i];
				MaxValue[i] = Math.Max(MaxValue[i], vector[i]);
				MinValue[i] = Math.Min(MinValue[i], vector[i]);
			}
			n++;
			return this;
		}

		public VectorStatisticsAggregator AddVectors(IEnumerable<double[]> vectors)
		{
			foreach (var vector in vectors)
			{
				AddVector(vector);
			}
			return this;
		}

		public double[] MaxValue { get; private set; }
		public double[] MinValue { get; private set; }

		public double[] Mean
		{
			get { return new double[length].Fill(i => sum[i]/n); }
		}

		public double[] Variance
		{
			get { return new double[length].Fill(i => (sum2[i] - sum[i]*sum[i]/n)/(n - 1)); }
		}

		public double[] StandardDeviation
		{
			get { return Variance.ConvertThis(Math.Sqrt); }
		}

		public string FullInfo
		{
			get
			{
				return string.Format("Mean  : {0}{4}Sigma : {1}{4}Max   : {2}{4}Min   : {3}",
					formatValue(Mean), formatValue(StandardDeviation),
					formatValue(MaxValue), formatValue(MinValue),
					Environment.NewLine);
			}
		}
	}
}