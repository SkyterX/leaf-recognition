﻿using System;
using System.Collections.Generic;

namespace LeafRecognition.Recognition.Tools
{
	public class StatisticsAggregator
	{
		private double sum = 0;
		private double sum2 = 0;
		private int n = 0;
		private readonly Func<double, string> FormatValue;

		public StatisticsAggregator(Func<double, string> formatValue = null)
		{
			FormatValue = formatValue ?? ((val) => string.Format("{0, -8:0.00}", val));
		}

		public StatisticsAggregator AddValue(double val)
		{
			sum += val;
			sum2 += val*val;
			n++;
			MaxValue = Math.Max(MaxValue, val);
			MinValue = Math.Min(MinValue, val);
			return this;
		}

		public StatisticsAggregator AddValues(IEnumerable<double> values)
		{
			foreach (var value in values)
			{
				AddValue(value);
			}
			return this;
		}

		public double MaxValue { get; private set; }
		public double MinValue { get; private set; }

		public double Mean
		{
			get { return sum/n; }
		}

		public double Variance
		{
			get { return (sum2 - sum*sum/n)/(n - 1); }
		}

		public double StandardDeviation
		{
			get { return Math.Sqrt(Variance); }
		}

		public string FullInfo
		{
			get
			{
				return string.Format("Mean : {0}, Sigma : {1}", FormatValue(Mean), FormatValue(StandardDeviation)) +
					   Environment.NewLine +
					   string.Format("Max : {0}, Min : {1}", FormatValue(MaxValue), FormatValue(MinValue));
			}
		}
	}
}