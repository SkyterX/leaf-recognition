﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using LeafRecognition.Util;

namespace LeafRecognition.Recognition.Tools
{
	[DataContract(Namespace = "LeafRecognition")]
	public class MeanNormalizer
	{
		private const double Epsilon = 1e-12;

		[DataMember] private readonly double[] mean;
		[DataMember] private readonly double[] standardDeviation;

		public MeanNormalizer(double[] mean, double[] standardDeviation)
		{
			this.mean = mean;
			this.standardDeviation = Array.ConvertAll(standardDeviation, var => var < Epsilon ? 1 : var);
		}

		public MeanNormalizer(IList<double[]> vectors)
		{
			if (vectors.Count == 0)
				throw new ArgumentException("Source is empty", "vectors");
			var stats = new VectorStatisticsAggregator(vectors[0].Length)
				.AddVectors(vectors);
			mean = stats.Mean;
			standardDeviation = stats.StandardDeviation;
			standardDeviation.ConvertThis(var => var < Epsilon ? 1 : var);
		}

		public double[] Normalize(double[] vector)
		{
			var normalized = new double[vector.Length];
			for (var i = 0; i < vector.Length; i++)
			{
				normalized[i] = (vector[i] - mean[i])/standardDeviation[i];
			}
			return normalized;
		}
	}
}