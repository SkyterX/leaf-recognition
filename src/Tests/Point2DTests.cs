﻿using System;
using LeafRecognition.Geometry;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class Point2DTests
    {
        [Test]
        public void CompareToTest()
        {
            Assert.Less(new Point2D(2, 3).CompareTo(new Point2D(4, 3)), 0);
            Assert.Less(new Point2D(2, 3).CompareTo(new Point2D(2, 7)), 0);

            Assert.AreEqual(new Point2D(2, 3).CompareTo(new Point2D(2, 3)), 0);

            Assert.Greater(new Point2D(4, 3).CompareTo(new Point2D(2, 3)), 0);
            Assert.Greater(new Point2D(2, 7).CompareTo(new Point2D(2, 3)), 0);
        }

        [Test]
        public void CrossProductTest()
        {
            var a = new Point2D(4, 7);
            var b = new Point2D(-3, 2);
            int c = Point2D.CrossProduct(a, b);
            Assert.AreEqual(c, 29);
        }

        [Test]
        public void DotProductTest()
        {
            var a = new Point2D(4, 7);
            var b = new Point2D(-3, 2);
            int c = Point2D.DotProduct(a, b);
            Assert.AreEqual(c, 2);
        }

        [Test]
        public void LengthTest()
        {
            var a = new Point2D(4, -5);
            Assert.AreEqual(a.SquaredLength(), 41);
            Assert.AreEqual(a.Length(), Math.Sqrt(41));
        }

        [Test]
        public void MinusTest()
        {
            var a = new Point2D(4, 7);
            var b = new Point2D(-3, 2);
            Point2D c = a - b;
            Assert.AreEqual(c, new Point2D(7, 5));
        }

        [Test]
        public void PlusTest()
        {
            var a = new Point2D(4, 7);
            var b = new Point2D(-3, 2);
            Point2D c = a + b;
            Assert.AreEqual(c, new Point2D(1, 9));
        }
    }
}