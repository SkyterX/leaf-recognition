﻿using System;
using System.IO;
using System.Linq;
using LeafRecognition.Filters;
using NUnit.Framework;
using Tests.Tools;

namespace Tests
{
	[TestFixture]
	public class ImageFiltersTests
	{
		private const string OutputDir = Database.Samples + Database.TestResults;

		[SetUp]
		public void Setup()
		{
			if (!Directory.Exists(OutputDir))
				Directory.CreateDirectory(OutputDir);
		}

		[Test]
		public void GrayscaleTest()
		{
			TestFilter(GrayscaleYFilter.Default, SampleImage.SampleMaple);
		}

		[Test]
		public void UnsharpTest()
		{
			TestFilter(KernelFilter.Unsharp, SampleImage.SampleMaple);
		}

		[Test]
		public void GaussianBlurTest()
		{
			TestFilter(KernelFilter.GaussianBlur, SampleImage.SampleMaple);
		}

		[Test]
		public void SharpenTest()
		{
			TestFilter(KernelFilter.Sharpen, SampleImage.SampleMaple);
		}

		[Test]
		public void FastLeafBorderTest()
		{
			TestFilter(LeafBorderGetter.Instance, SampleImage.Halftones);
		}

		[Test]
		public void SlowLeafBorderTest()
		{
			TestFilter(LeafBorderGetter.Instance, SampleImage.SampleMaple);
		}

		[Test]
		public void LeafShapeTest()
		{
			TestFilter(LeafShapeGetter.Instance, SampleImage.DarkImage);
		}

		[Test]
		public void InverseTest()
		{
			TestFilter(Inverse.Default, SampleImage.Halftones);
		}

		[Test]
		public void HalftonesTest()
		{
			TestFilter(Binarization.Default, SampleImage.SampleMaple);
		}

		[Test]
		public void LeafBorderAllTest()
		{
			TestFilter(LeafBorderGetter.Instance);
		}

		private void TestFilter(IFilter filter)
		{
			var filterdImages = Directory
				.GetFiles(LearningBase.TrainingSet)
				.Select(LazyImage.Loader.Load)
				.Select(filter.Apply);

			var i = 0;
			foreach (var image in filterdImages)
			{
				++i;
				image.Image.Save(Path.Combine(OutputDir, string.Format("{0}.jpg", i)));
			}
		}

		private void TestFilter(IFilter filter, string imageName, string filterName = null)
		{
			filterName = filterName ?? filter.GetType().Name;
			Console.Out.WriteLine("Loading...");
			var imagePath = Path.Combine(Database.Samples, imageName);
			var loading = Measure.Time(() => LazyImage.Loader.Load(imagePath));
			var image = loading.Result;
			Console.Out.WriteLine("Loading time : {0}", loading.TimeElapsed.ToSeconds());

			Console.Out.WriteLine("Applying {0}", filterName);
			Console.Out.WriteLine("Processing...");
			var filtered = Measure.Time(() => filter.Apply(image));
			Console.Out.WriteLine("Processing time : {0}", filtered.TimeElapsed.ToSeconds());

			Console.Out.WriteLine("Saving resulting image...");
			var name = Path.GetFileNameWithoutExtension(imageName);
			var ext = Path.GetExtension(imageName);
			var targetName = string.Format("{0} + {1}.{2}", name, filterName, ext);
			var saving = Measure.Time(() => filtered.Result.Image.Save(Path.Combine(OutputDir, targetName)));
			Console.Out.WriteLine("Loading time : {0}", saving.ToSeconds());
		}
	}
}