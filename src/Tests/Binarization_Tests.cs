﻿using System;
using System.Drawing;
using System.IO;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using LeafRecognition.Filters;
using LeafRecognition.Images;
using NUnit.Framework;
using Tests.Tools;

namespace Tests
{
	[TestFixture]
	public class Binarization_Tests
	{
		[Test]
		[Explicit]
		public void Binarize_TrainingSet()
		{
			var files = Directory.GetFiles(LearningBase.TrainingSet);
			var outDir = Path.Combine(LearningBase.TrainingSet, "Contour");
			Directory.CreateDirectory(outDir);
			var time = Measure.Time(() =>
			{
				foreach (var file in files)
				{
					var fileName = Path.GetFileName(file);
					var imageFromPath = Path.Combine(LearningBase.TrainingSet, fileName);
					var imageToPath = Path.Combine(outDir, fileName);
					var image = new Image<Rgb, byte>(imageFromPath);
					ProcessImage(image).Save(imageToPath);
				}
			});
			Console.Out.WriteLine("Time elapsed : {0}", time.ToSeconds());
			Console.Out.WriteLine("Average time : {0}", new TimeSpan(time.Ticks/files.Length).ToMilliseconds());
		}

		[Test]
		[Explicit]
		public void Binarize_Single()
		{
//			var imageFile = "DSC_0033.TypeC.jpg";
//			var imageFile = "DSC_0069.TypeI.jpg";
//			var imageFile = "DSC_0099.TypeK.jpg";
			var imageFile = "DSC_0095.TypeK.jpg";
//			var imageFile = "DSC_0116.TypeM.jpg";
			var imagePath = Path.Combine(LearningBase.TrainingSet, imageFile);

			var baseImage = new Image<Rgb, byte>(imagePath);
			baseImage = baseImage.Resize(500.0/baseImage.Width, INTER.CV_INTER_LANCZOS4);
			var res = Measure.Time(() => ProcessImage(baseImage));
			Console.Out.WriteLine("Time elapsed : {0}", res.TimeElapsed.ToMilliseconds());
			var imageE = res.Result;
			var resImage = baseImage.ConcateHorizontal(imageE);
			resImage.Draw(
				new LineSegment2D(new Point(baseImage.Width, 0), new Point(baseImage.Width, baseImage.Height)),
				new Rgb(Color.Black), 3);
//			ImageViewer.Show(resImage.Resize(1600.0/resImage.Width, INTER.CV_INTER_LANCZOS4));
			ImageViewer.Show(resImage);
		}

		private static Image<Rgb, byte> ProcessImage(Image<Rgb, byte> baseImage)
		{
			return FloodFillBinarization.Instance.Apply(baseImage.ToRgbImage()).Image;
		}
	}
}