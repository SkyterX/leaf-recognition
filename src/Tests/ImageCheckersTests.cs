﻿using System;
using System.Diagnostics;
using System.IO;
using LeafRecognition.Checkers;
using LeafRecognition.Images;
using NUnit.Framework;

namespace Tests
{
	[TestFixture]
    public class ImageCheckersTests
    {
        [Test]
        public void HalftonesCheckerTest1()
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            Console.WriteLine("Processing started...");
            string fileName = Path.Combine(ImageBase.RootPath, "NonHalftonesImage.JPG");
            IImage simpleBitmap = PlainBitmap.Load(fileName);

            long readTicks = stopwatch.ElapsedTicks;
            bool result = HalftonesChecker.Instance.Check(simpleBitmap);
            Console.WriteLine("Leaf shape calculated. Elapsed time: {0} seconds",
                              new TimeSpan(stopwatch.ElapsedTicks - readTicks).TotalSeconds);

            Assert.That(result, Is.False);
        }

        [Test]
        public void HalftonesCheckerTest2()
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            Console.WriteLine("Processing started...");
            string fileName = Path.Combine(ImageBase.RootPath, "HalftonesImage.jpg");
            IImage simpleBitmap = PlainBitmap.Load(fileName);

            long readTicks = stopwatch.ElapsedTicks;
            bool result = HalftonesChecker.Instance.Check(simpleBitmap);
            Console.WriteLine("Leaf shape calculated. Elapsed time: {0} seconds",
                              new TimeSpan(stopwatch.ElapsedTicks - readTicks).TotalSeconds);

            Assert.That(result, Is.True);
        }

        [Test]
        public void SingleComponentTest1()
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            Console.WriteLine("Processing started...");
            string fileName = Path.Combine(ImageBase.RootPath, "HalftonesImage.jpg");
            IImage simpleBitmap = PlainBitmap.Load(fileName);

            long readTicks = stopwatch.ElapsedTicks;
            bool result = SingleComponentChecker.Instance.Check(simpleBitmap);
            Console.WriteLine("Leaf shape calculated. Elapsed time: {0} seconds",
                              new TimeSpan(stopwatch.ElapsedTicks - readTicks).TotalSeconds);

            Assert.That(result, Is.True);
        }

        [Test]
        public void SingleComponentTest2()
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            Console.WriteLine("Processing started...");
            string fileName = Path.Combine(ImageBase.RootPath, "MultipleComponents.jpg");
            IImage simpleBitmap = PlainBitmap.Load(fileName);

            long readTicks = stopwatch.ElapsedTicks;
            bool result = SingleComponentChecker.Instance.Check(simpleBitmap);
            Console.WriteLine("Leaf shape calculated. Elapsed time: {0} seconds",
                              new TimeSpan(stopwatch.ElapsedTicks - readTicks).TotalSeconds);

			Assert.That(result, Is.False);

        }
    }
}