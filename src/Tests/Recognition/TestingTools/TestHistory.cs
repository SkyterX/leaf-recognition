﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Tests.Recognition.TestingTools
{
	[DataContract(Namespace = "LeafRecognition")]
	public class TestHistory<T> : IEnumerable<T>
		where T : TimedTestingResult
	{
		[DataMember] protected readonly List<T> Results;

		public TestHistory()
		{
			Results = new List<T>();
		}

		public IEnumerator<T> GetEnumerator()
		{
			return Results.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return Results.GetEnumerator();
		}

		public void Add(T item)
		{
			Results.Add(item);
		}

		public override string ToString()
		{
			return Results.Aggregate(new StringBuilder("History (in chronological order):" + Environment.NewLine),
				(builder, testingResult) => builder.AppendLine(testingResult.ShortInfo))
				.ToString();
		}

		public T this[int idx]
		{
			get
			{
				if (idx < 0)
					return Results[Results.Count - idx];
				if (idx > Results.Count - 1)
					return Results[Results.Count - 1];
				return Results[idx];
			}
		}
	}
}