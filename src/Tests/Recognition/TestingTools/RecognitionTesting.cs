﻿using System;
using System.Collections.Generic;
using System.IO;
using LeafRecognition.Recognition;
using LeafRecognition.Recognition.Classification;
using Tests.Recognition.Data;
using Tests.Tools;

namespace Tests.Recognition.TestingTools
{
	public static class RecognitionTesting
	{
		public static TestingResult TestOn(this ImageRecognizer recognizer, IEnumerable<ClassifiedVector> testVectors, ClassifiedVectorLoader loader, bool log)
		{
			var classifier = recognizer.Classifier;
			var labelProcessor = loader.LabelProcessor;
			var testingResult = new TestingResult(recognizer);
			testingResult.LabelProcessor = labelProcessor;
			foreach (var vector in testVectors)
			{
				var classificationResult = classifier.Classify(vector.Vector);
				if (classificationResult.Label != vector.Label)
					Log(log, "  {0, 20} \t Expected {1}, but was {2}", vector.Name,
						labelProcessor.GetName(vector.Label), 
						labelProcessor.GetName(classificationResult.Label));
				testingResult.Add(classificationResult, vector.Label, vector.Name);
			}
			return testingResult;
		}

		public static TestingResult TestOn(this ImageRecognizer recognizer, string dir, ClassifiedVectorLoader loader, bool logTesting = false)
		{
			return recognizer.TestOn(loader.LoadFiles(dir), loader, logTesting);
		}

		public static IEnumerable<ClassifiedVector> LoadFiles(this ClassifiedVectorLoader loader,
			string dir)
		{
			var files = Directory.EnumerateFiles(dir, "*" + FileExtensions.Image);
			return loader.LoadFiles(files);
		}

		private static void Log(bool enableLog, string format, params object[] args)
		{
			if (enableLog)
				Console.Out.WriteLine(format, args);
		}
	}
}