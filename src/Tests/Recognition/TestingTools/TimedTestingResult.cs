﻿using System;
using System.Runtime.Serialization;

namespace Tests.Recognition.TestingTools
{
	[DataContract(Namespace = "LeafRecognition")]
	public abstract class TimedTestingResult
	{
		private const string DateFormat = "dd.MM.yyyy HH:mm:ss";

		[DataMember] protected readonly DateTime TestTimeStamp;

		protected TimedTestingResult()
		{
			TestTimeStamp = DateTime.Now;
		}

		protected string TimeInfo
		{
			get { return string.Format("Test time : {0}", TimeStamp); }
		}

		public string TimeStamp
		{
			get { return TestTimeStamp.ToString(DateFormat); }
		}

		public abstract string ShortInfo { get; }
	}
}