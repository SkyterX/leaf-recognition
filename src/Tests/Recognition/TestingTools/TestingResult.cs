﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using LeafRecognition.Recognition;
using LeafRecognition.Recognition.Classification;
using LeafRecognition.Util;
using Tests.Recognition.Data;
using Tests.Tools;

namespace Tests.Recognition.TestingTools
{
	[DataContract(Namespace = "LeafRecognition")]
	public class TestingResult : TimedTestingResult,
		IComparable, IComparable<TestingResult>, IEquatable<TestingResult>
	{
		[DataMember] public readonly string ClassifierName;
		[DataMember] public readonly string ClassifierDescription;
		[DataMember] public readonly string VectorizerDescription;
		[DataMember] public List<TestCase> Results;
		private ILabelProcessor labelProcessor;
		private TestStatistics statistics;

		public TestingResult(ImageRecognizer recognizer)
		{
			ClassifierName = recognizer.Classifier.Name;
			ClassifierDescription = recognizer.Classifier.Description;
			VectorizerDescription = recognizer.Vectorizer.Description;
			Results = new List<TestCase>();
		}

		public void Add(ClassificationResult result, int expectedLabel, string name)
		{
			Results.Add(new TestCase(result.Confidence, result.Label, expectedLabel, name));
			statistics = null;
		}

		public ILabelProcessor LabelProcessor
		{
			get { return labelProcessor ?? LabelProcessorStub.Instance; }
			set
			{
				labelProcessor = value;
				statistics = null;
			}
		}

		public TestStatistics Stats
		{
			get { return statistics ?? (statistics = new TestStatistics(this)); }
		}

		private double Score
		{
			get { return Stats.FScoreMacro; }
		}


		public override string ShortInfo
		{
			get { return string.Format("Score : {0}. {1} - {2}", Score.ToPercentage(), TimeInfo, ClassifierName); }
		}

		public string Info
		{
			get
			{
				return new StringBuilder()
					.AppendFormatLine("Score : {0}. {1}", Score.ToPercentage(), TimeInfo)
					.AppendFormatLine("Classifier : {0}", ClassifierDescription)
					.AppendFormatLine("Vectorizer : {0}", VectorizerDescription)
					.ToString();
			}
		}

		#region IComparable members

		public int CompareTo(object obj)
		{
			if (obj == null)
				return 1;
			if (!(obj is TestingResult))
				throw new ArgumentException("Argument must be TestingResult");
			return CompareTo((TestingResult) obj);
		}

		public int CompareTo(TestingResult other)
		{
			if (other == null) return 1;
			return Math.Abs(Score - other.Score) < 1e-2 ? 0 : Score.CompareTo(other.Score);
		}

		public bool Equals(TestingResult other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return DescriptionEquals(ClassifierDescription, other.ClassifierDescription)
				&& DescriptionEquals(VectorizerDescription, other.VectorizerDescription)
				&& Math.Abs(Score - other.Score) < 1e-2;
		}

		private bool DescriptionEquals(string x, string y)
		{
			x = Regex.Replace(x, @"\s", "");
			y = Regex.Replace(y, @"\s", "");
			return string.Equals(x, y, StringComparison.OrdinalIgnoreCase);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != GetType()) return false;
			return Equals((TestingResult) obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				var hashCode = (ClassifierDescription != null ? ClassifierDescription.GetHashCode() : 0);
				hashCode = (hashCode*397) ^ (VectorizerDescription != null ? VectorizerDescription.GetHashCode() : 0);
				hashCode = (hashCode*397) ^ Score.GetHashCode();
				return hashCode;
			}
		}

		public static bool operator ==(TestingResult left, TestingResult right)
		{
			return Equals(left, right);
		}

		public static bool operator !=(TestingResult left, TestingResult right)
		{
			return !Equals(left, right);
		}

		#endregion

		[DataContract(Namespace = "LeafRecognition")]
		public struct TestCase
		{
			[DataMember] public readonly int ActualLabel;
			[DataMember] public readonly int ExpectedLabel;
			[DataMember] public readonly double[] Confidence;
			[DataMember] public readonly string Name;

			public TestCase(double[] confidence, int actualLabel, int expectedLabel, string name)
			{
				Confidence = confidence;
				ActualLabel = actualLabel;
				ExpectedLabel = expectedLabel;
				Name = name;
			}
		}

		public class TestStatistics
		{
			private readonly ILabelProcessor labelProcessor;
			public string TableSeparator = " ";

			/// <summary> Measures precision impact in FScore </summary>
			public const double FScoreBeta = 1;

			public readonly int NClasses;
			public readonly int TotalCases;

			/// <summary> M[X, Y] = Count of cases of class X classified as class Y. </summary>
			public readonly int[,] ConfusionMatrix;

			/// <summary> Count of correctly recognized cases of class X. </summary>
			public readonly int[] TruePositives;

			/// <summary> Count of cases belonging to other classes recognized as class X. </summary>
			public readonly int[] FalsePositives;

			/// <summary> Count of incorrectly recognized cases of class X. </summary>
			public readonly int[] FalseNegatives;

			/// <summary> Count of correctly recognized (not as X) cases of other classes. </summary>
			public readonly int[] TrueNegatives;

			/// <summary> Average per-class	effectiveness of classifier. </summary>
			public readonly double AverageAccuracy;

			/// <summary> Average per-class	classification error. </summary>
			public readonly double ErrorRate;

			/// <summary> Agreement of the data class labels with those of a classifiers based on sums of per-case decisions. </summary>
			public readonly double PrecisionMicro;

			/// <summary> Effectiveness of a classifier to identify class labels based on sums of per-case decisions. </summary>
			public readonly double RecallMicro;

			/// <summary> Relations between data’s positive labels and those given by a classifier based on sums of per-case decisions. </summary>
			public readonly double FScoreMicro;

			/// <summary> An average per-class agreement of the data class labels with those of a classifiers. </summary>
			public readonly double PrecisionMacro;

			/// <summary> An average per-class effectiveness of a classifier to identify class labels. </summary>
			public readonly double RecallMacro;

			/// <summary> Relations between data’s positive labels and those given by a classifier based on a per-class average. </summary>
			public readonly double FScoreMacro;

			public TestStatistics(TestingResult testingResult)
			{
				labelProcessor = testingResult.LabelProcessor;

				NClasses = 1 + testingResult.Results
					.Max(tc => Math.Max(tc.ActualLabel, tc.ExpectedLabel));
				ConfusionMatrix = new int[NClasses, NClasses];
				TruePositives = new int[NClasses];
				FalsePositives = new int[NClasses];
				FalseNegatives = new int[NClasses];
				TrueNegatives = new int[NClasses];
				TotalCases = testingResult.Results.Count;
				if (TotalCases == 0) return;

				foreach (var testCase in testingResult.Results)
				{
					ConfusionMatrix[testCase.ExpectedLabel, testCase.ActualLabel]++;
					if (testCase.ExpectedLabel == testCase.ActualLabel)
						TruePositives[testCase.ExpectedLabel]++;
					else
					{
						FalsePositives[testCase.ExpectedLabel]++;
						FalseNegatives[testCase.ActualLabel]++;
					}
				}
				for (var x = 0; x < NClasses; x++)
				{
					TrueNegatives[x] = TotalCases - TruePositives[x] - FalsePositives[x] - FalseNegatives[x];
				}
				double nCases = TotalCases;
				double nClasses = NClasses;

				AverageAccuracy = 100/nClasses*Enumerable.Range(0, NClasses)
					.Sum(x => (TruePositives[x] + TrueNegatives[x])/nCases);
				ErrorRate = 100/nClasses*Enumerable.Range(0, NClasses)
					.Sum(x => (FalsePositives[x] + FalseNegatives[x])/nCases);

				PrecisionMicro = 100*((double) TruePositives.Sum())/(TruePositives.Sum() + FalsePositives.Sum());
				RecallMicro = 100*((double) TruePositives.Sum())/(TruePositives.Sum() + FalseNegatives.Sum());
				FScoreMicro = FScore(PrecisionMicro, RecallMicro);

				PrecisionMacro = 1/nClasses*Enumerable.Range(0, NClasses)
					.Sum(x => Precision(x));
				RecallMacro = 1/nClasses*Enumerable.Range(0, NClasses)
					.Sum(x => Recall(x));
				FScoreMacro = FScore(PrecisionMacro, RecallMacro);
			}

			private double FScore(double precision, double recall)
			{
				if (precision + recall < 1e-5) return 0;
				return ((FScoreBeta*FScoreBeta + 1)*precision*recall)/
					   (FScoreBeta*FScoreBeta*precision + recall);
			}

			private double Recall(int x)
			{
				return TruePositives[x] == 0 
					? 0 
					: 100*((double) TruePositives[x])/(TruePositives[x] + FalseNegatives[x]);
			}

			private double Precision(int x)
			{
				return TruePositives[x] == 0 
					? 0 
					: 100*((double) TruePositives[x])/(TruePositives[x] + FalsePositives[x]);
			}

			public string Info
			{
				get
				{
					return new StringBuilder()
						.AppendFormatLine("Average Accuracy : {0}, ErrorRate : {1}",
							AverageAccuracy.ToPercentage(), ErrorRate.ToPercentage())
						.AppendLine("Micro averaged stats")
						.AppendFormatLine("FScore : {0}, Precision : {1}, Recall : {2}",
							FScoreMicro.ToPercentage(), PrecisionMicro.ToPercentage(), RecallMicro.ToPercentage())
						.AppendLine("Macro averaged stats")
						.AppendFormatLine("FScore : {0}, Precision : {1}, Recall : {2}",
							FScoreMacro.ToPercentage(), PrecisionMacro.ToPercentage(), RecallMacro.ToPercentage())
						.AppendLine()
						.ToString();
				}
			}

			public string FullInfo
			{
				get { return Info + PerClassInfo + ConfusionMatrixInfo; }
			}

			public string PerClassInfo
			{
				get
				{
					var sb = new StringBuilder();
					var maxClassNameLength = Enumerable
						.Range(0, labelProcessor.NClasses)
						.Select(id => labelProcessor.GetName(id).Length)
						.Concat(8.Yield())
						.Max();
					var nameFormat = string.Format("{{0, {0}}}", maxClassNameLength);
					sb.AppendLine("Per-class classification statistics");
					sb.AppendFormatLine("{1}{0}|{0}  TP{0}  FP{0}  FN{0}  TN{0}|{0}Precision{0} Recall{0} FScore", 
						TableSeparator, string.Format(nameFormat, "LeafType"));
					for (var i = 0; i < NClasses; i++)
					{
						var precision = Precision(i);
						var recall = Recall(i);
						var fScore = FScore(precision, recall);
						sb.AppendFormatLine("{1,7}{0}|{0}{2, 4}{0}{3, 4}{0}{4, 4}{0}{5, 4}{0}|{0}{6, 9}{0}{7, 7}{0}{8, 7}",  
							TableSeparator, string.Format(nameFormat, labelProcessor.GetName(i)),
							TruePositives[i], FalsePositives[i], FalseNegatives[i], TrueNegatives[i],
							precision.ToPercentage(), recall.ToPercentage(), fScore.ToPercentage());
					}
					sb.AppendLine();
					return sb.ToString();
				}
			}

			public string ConfusionMatrixInfo
			{
				get
				{
					var sb = new StringBuilder();
					sb.AppendLine("Confusion Matrix");

					var maxLen = 0;
					foreach (var x in ConfusionMatrix)
					{
						maxLen = Math.Max(maxLen, x.ToString().Length);
					}
					var cellFormat = string.Format("{{0, {0}}}", maxLen);
					var header = Enumerable
						.Range(0, NClasses)
						.Select(i => labelProcessor.GetShortName(i))
						.Select(type => string.Format(cellFormat, type))
						.AggregateToString(TableSeparator);
					sb.AppendFormatLine("Recognized{0}|{0}" + header, TableSeparator);
					sb.AppendFormatLine("--Expected{0}|{0}-" + new string('-', header.Length), TableSeparator);
					for (var x = 0; x < NClasses; x++)
					{
						var line = string.Format("{1, 10}{0}|{0}", TableSeparator, labelProcessor.GetName(x)) + 
							Enumerable
							.Range(0, NClasses)
							.Select(y => string.Format(cellFormat, ConfusionMatrix[x, y]))
							.AggregateToString(TableSeparator);
						sb.AppendLine(line);

					}
					sb.AppendLine();
					return sb.ToString();
				}
			}
		}
	}
}