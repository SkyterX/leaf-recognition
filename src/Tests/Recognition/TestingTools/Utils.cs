﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using LeafRecognition.Recognition;
using LeafRecognition.Recognition.Classification;
using LeafRecognition.Util;
using Tests.Tools;

namespace Tests.Recognition.TestingTools
{
	public static class Utils
	{
		private const int RandomSeed = 221592432;

		public static ISet<ClassifiedVector> GetProportionalSubset(
			this IEnumerable<ClassifiedVector> vectors, double ratio, Random random = null)
		{
			random = random ?? new Random(RandomSeed);

			var groups = new Dictionary<int, IList<ClassifiedVector>>();

			foreach (var vector in vectors)
			{
				if (!groups.ContainsKey(vector.Label))
					groups.Add(vector.Label, new List<ClassifiedVector>());
				groups[vector.Label].Add(vector);
			}

			return groups.Values
				.SelectMany(list => list
					.Shuffle(random)
					.Take((int) (ratio*list.Count)))
				.ToSet();
		}

		public static string SaveRecognizer(ImageRecognizer recognizer, string tag = null)
		{
			var fileName = string.Format("{2}{0}_{1}", recognizer.Classifier.Name,
				recognizer.Description.MD5(), tag);
			var filePath = Path.Combine(Database.Recognizers, fileName + FileExtensions.Recognizer);
			Serializer.Serialize(recognizer, filePath);
			return fileName;
		}

		public static ImageRecognizer LoadRecognizer(string fileName)
		{
			var filePath = Path.Combine(Database.Recognizers, fileName + FileExtensions.Recognizer);
			return Serializer.Deserialize<ImageRecognizer>(filePath);
		}

		public static TestHistory<TestingResult> LoadHistory(string dir, string testName)
		{
			return Serializer.TryDeserialize<TestHistory<TestingResult>>(GetHistoryFile(dir, testName))
				   ?? new TestHistory<TestingResult>();
		}

		public static void SaveHistory<TResult>(this TestHistory<TResult> history, string dir, string testName)
			where TResult : TimedTestingResult
		{
			var historyFile = GetHistoryFile(dir, testName);
			var historyDir = Path.GetDirectoryName(historyFile);
			if (historyDir != null && !Directory.Exists(historyDir))
				Directory.CreateDirectory(historyDir);
			Serializer.Serialize(history, historyFile);
		}

		private static string GetHistoryFile(string dir, string testName)
		{
			return Path.Combine(dir, Database.TestResults, testName + FileExtensions.History);
		}
	}
}