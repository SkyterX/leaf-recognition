﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using LeafRecognition.Util;
using NUnit.Framework;
using Tests.Tools;

namespace Tests.Recognition.Data.ImageCLEF
{
	public class CLEFClassId : ILabelProcessor
	{
		public static readonly CLEFClassId Processor = new CLEFClassId();
		private readonly IDictionary<string, int> classId;
		private readonly IList<string> classNames; 

		private CLEFClassId()
		{
			classNames = Serializer.Deserialize<IList<string>>(CLEFBase.ClassIdMappings);
			classId = new Dictionary<string, int>(classNames.Count);
			for (var id = 0; id < classNames.Count; id++)
			{
				classId.Add(classNames[id], id);
			}
		}

		public string GetName(int label)
		{
			return classNames[label];
		}

		public string GetShortName(int label)
		{
			return classNames[label];
		}

		public int LoadLabel(string file)
		{
			var markup = CLEFMarkup.Load(file);
			return classId[markup.ClassId];
		}

		public int NClasses { get { return classNames.Count; } }
	}

	[TestFixture]
	public class CLEFClassId_Construction
	{
		[Test]
		public void CreateClassIdMapping()
		{
			var dirs = new []
			{
				CLEFBase.TrainingSet,
				CLEFBase.TestSet
			};
			var classNames = new HashSet<string>();
			foreach (var dir in dirs)
			{
				var files = Directory.EnumerateFiles(dir, "*" + FileExtensions.CLEFMarkup);
				foreach (var file in files)
				{
					var markup = CLEFMarkup.Load(file);
					classNames.Add(markup.ClassId);
				}
			}
			var names = classNames.ToList();
			names.Sort();
			Serializer.Serialize(names, CLEFBase.ClassIdMappings);
		}
	}
}