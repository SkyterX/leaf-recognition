﻿using System.IO;
using System.Xml;
using System.Xml.Serialization;
using Tests.Tools;

namespace Tests.Recognition.Data.ImageCLEF
{
	[XmlRoot("Image")]
	public class CLEFXmlMarkup
	{
		public string FileName;
		public string Date;
		public CLEFXmlType Type;
		public string Author;
		public string Organization;
		public CLEFXmlContent Content;
		public int IndividualPlantId;
		public CLEFXmlTaxon Taxon;
		public string ClassId;
		public string VernacularNames;
		public string Locality;
		public CLEFXmlGPSLocality GPSLocality;
	}

	[XmlRoot("GPSLocality")]
	public class CLEFXmlGPSLocality
	{
		public double Longtitude;
		public double Latitude;
	}

	[XmlRoot("Taxon")]
	public class CLEFXmlTaxon
	{
		public string Regnum;
		public string Class;
		public string Subclass;
		public string Superorder;
		public string Order;
		public string Family;
		public string Genus;
		public string Species;
	}

	public enum CLEFXmlContent
	{
		Leaf,
		[XmlEnum("Picked leaf")] PickedLeaf,
		[XmlEnum("Upper face")] UpperSide,
		[XmlEnum("Lower face")] LowerSide,
		Branch,
		Leafage
	}

	public enum CLEFXmlType
	{
		Scan,
		[XmlEnum("pseudoscan")] Pseudoscan,
		[XmlEnum("photograph")] Photograph
	}

	public static class CLEFMarkup
	{
		public static CLEFXmlMarkup Load(string filePath)
		{
			var serializer = new XmlSerializer(typeof (CLEFXmlMarkup));
			filePath = Path.ChangeExtension(filePath, FileExtensions.CLEFMarkup);
			using (var reader = XmlReader.Create(filePath))
			{
				return serializer.Deserialize(reader) as CLEFXmlMarkup;
			}
		}
	}
}