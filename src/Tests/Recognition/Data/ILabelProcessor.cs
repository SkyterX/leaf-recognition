﻿using System.Globalization;

namespace Tests.Recognition.Data
{
	public interface ILabelProcessor
	{
		string GetName(int label);
		string GetShortName(int label);
		int LoadLabel(string file);
		int NClasses { get; }
	}

	public class LabelProcessorStub : ILabelProcessor
	{
		public static readonly LabelProcessorStub Instance = new LabelProcessorStub();

		private LabelProcessorStub()
		{
		}

		public string GetName(int label)
		{
			return label.ToString(CultureInfo.InvariantCulture);
		}

		public string GetShortName(int label)
		{
			return GetName(label);
		}

		public int LoadLabel(string file)
		{
			return 0;
		}

		public int NClasses
		{
			get { return 1; }
		}
	}
}