﻿using System;
using System.Collections.Generic;
using System.IO;
using LeafRecognition.Images;
using LeafRecognition.Recognition;
using LeafRecognition.Util;
using Tests.Tools;

namespace Tests.Recognition.Data
{
	public class ClassifiedImageLoader
	{
		public static readonly ClassifiedImageLoader Local = new ClassifiedImageLoader(LeafTypes.Processor);
		public static readonly ClassifiedImageLoader CLEF = CLEFImageLoader.All;

		public readonly ILabelProcessor LabelProcessor;

		public ClassifiedImageLoader(ILabelProcessor labelProcessor)
		{
			LabelProcessor = labelProcessor;
		}

		public ClassifiedImage Load(string filePath)
		{
			var lazyImage = new Lazy<RgbImage>(() => LazyImage.Loader.Load(filePath));
			var imageChars = Serializer.TryDeserialize<CharacteristicsInfo>(GetCharacteristicsFile(filePath));
			var imageInfo = new ImageInfo(lazyImage, imageChars);
			return new ClassifiedImage(imageInfo, LabelProcessor.LoadLabel(filePath), filePath);
		}

		protected virtual bool IsAcceptable(string file)
		{
			return true;
		}

		public IEnumerable<ClassifiedImage> LoadFiles(IEnumerable<string> files)
		{
			foreach (var file in files)
			{
				if (IsAcceptable(file))
					yield return Load(file);
			}
		}

		public void Save(ClassifiedImage image)
		{
			var filePath = GetCharacteristicsFile(image.FilePath);
			var dir = Path.GetDirectoryName(filePath);
			if (!Directory.Exists(dir))
				Directory.CreateDirectory(dir);
			Serializer.Serialize(image.ImageInfo.Characteristics, filePath);
		}

		private static string GetCharacteristicsFile(string filePath)
		{
			var fullPath = Path.GetFullPath(filePath);
			var dir = Path.GetDirectoryName(fullPath);
			var fileName = Path.GetFileName(fullPath);
			return Path.Combine(dir, Database.CharacteristicsDir, fileName + FileExtensions.Characteristics);
		}
	}
}