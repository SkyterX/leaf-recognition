﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using LeafRecognition.Recognition.Classification;
using LeafRecognition.Recognition.Vectorization;

namespace Tests.Recognition.Data
{
	public class ClassifiedVectorLoader
	{
		public static readonly ClassifiedVectorLoader LocalDefault =
			new ClassifiedVectorLoader(ClassifiedImageLoader.Local, Vectorizer.Default);
		public static readonly ClassifiedVectorLoader CLEFDefault =
			new ClassifiedVectorLoader(ClassifiedImageLoader.CLEF, Vectorizer.Default);

		public readonly ClassifiedImageLoader ImageLoader;
		public readonly IVectorizer InnerVectorizer;

		public ILabelProcessor LabelProcessor
		{
			get { return ImageLoader.LabelProcessor; }
		}

		public ClassifiedVectorLoader(ClassifiedImageLoader imageLoader, IVectorizer vectorizer)
		{
			ImageLoader = imageLoader;
			InnerVectorizer = vectorizer;
		}

		public ClassifiedVectorLoader(IVectorizer vectorizer, ILabelProcessor labelProcessor)
		{
			InnerVectorizer = vectorizer;
			ImageLoader = new ClassifiedImageLoader(labelProcessor);
		}

		public IEnumerable<ClassifiedVector> LoadFiles(IEnumerable<string> files)
		{
			return ImageLoader.LoadFiles(files).Select(Load);
		}

		public ClassifiedVector Load(string filePath)
		{
			return Load(ImageLoader.Load(filePath));
		}

		public ClassifiedVector Load(ClassifiedImage image)
		{
			var vector = InnerVectorizer.ToVector(image.ImageInfo);
			ImageLoader.Save(image);
			return new ClassifiedVector(vector, image.Label, Path.GetFileName(image.FilePath));
		}
	}
}