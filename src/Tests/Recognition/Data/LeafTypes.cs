﻿using System;
using System.IO;

namespace Tests.Recognition.Data
{
	public class LeafTypes : ILabelProcessor
	{
		public static readonly LeafTypes Processor = new LeafTypes();

		public readonly LeafType[] AllLeafTypes;

		private LeafTypes()
		{
			var values = Enum.GetValues(typeof (LeafType));
			AllLeafTypes = new LeafType[values.Length - 1];
			var cnt = 0;
			foreach (LeafType type in values)
			{
				if (type == LeafType.Unknown) continue;
				AllLeafTypes[cnt++] = type;
			}
		}

		public int NClasses
		{
			get { return AllLeafTypes.Length; }
		}

		public string GetName(int label)
		{
			return ((LeafType) label).ToString();
		}

		public string GetShortName(int label)
		{
			return GetName(label).Replace("Type", "");
		}

		public int LoadLabel(string filePath)
		{
			var fileName = Path.GetFileNameWithoutExtension(filePath);
			var type = Path.GetExtension(fileName) ?? "";
			var leafType = Parse(type.Trim('.'));
			if (leafType == LeafType.Unknown)
				throw new ArgumentException("File has no type extension : " + fileName, "filePath");
			return (int) leafType;
		}

		private LeafType Parse(string type)
		{
			LeafType result;
			return Enum.TryParse(type, true, out result)
				? result
				: LeafType.Unknown;
		}
	}
}