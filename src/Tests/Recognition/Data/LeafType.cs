﻿namespace Tests.Recognition.Data
{
	public enum LeafType
	{
		Unknown = -1,

		TypeA, // Syringa vulgaris
		TypeB, // Black Alder
		TypeC,
		TypeD, // Cherry - Prunus Avium? or some other Prunus
		TypeE,
		TypeF,
		TypeG, // Tilia Cordata
		TypeH,
		TypeI,
		TypeJ, // Oak - Quercus 
		TypeK, // Maple - Acer platanoides
		TypeL,
		TypeM,
		TypeN, // Liquidambar styraciflua
	}
}