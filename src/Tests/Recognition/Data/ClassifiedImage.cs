﻿using LeafRecognition.Recognition;

namespace Tests.Recognition.Data
{
	public class ClassifiedImage
	{
		public readonly ImageInfo ImageInfo;
		public readonly int Label;
		public string FilePath;

		public ClassifiedImage(ImageInfo imageInfo, int label, string filePath)
		{
			ImageInfo = imageInfo;
			Label = label;
			FilePath = filePath;
		}
	}
}