﻿using System;
using Tests.Recognition.Data.ImageCLEF;

namespace Tests.Recognition.Data
{
	public class CLEFImageLoader : ClassifiedImageLoader
	{
		public static readonly CLEFImageLoader All = new CLEFImageLoader(CLEFXmlType.Scan, CLEFXmlType.Pseudoscan);
		public static readonly CLEFImageLoader Scan = new CLEFImageLoader(CLEFXmlType.Scan);
		public static readonly CLEFImageLoader Pseudoscan = new CLEFImageLoader(CLEFXmlType.Pseudoscan);

		private readonly CLEFXmlType[] allowedTypes;

		private CLEFImageLoader(params CLEFXmlType[] allowedTypes) 
			: base(CLEFClassId.Processor)
		{
			this.allowedTypes = allowedTypes;
		}

		protected override bool IsAcceptable(string file)
		{
			var markup = CLEFMarkup.Load(file);
			return Array.IndexOf(allowedTypes, markup.Type) != -1;
		}
	}
}