﻿using System;
using System.Drawing;
using System.IO;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using NUnit.Framework;
using Tests.Tools;

namespace Tests.Recognition.Data
{
	[TestFixture]
	public class ClassifiedImage_Test
	{
		[Test]
		public void LoadingTest()
		{
			var dir = Database.AllImages;
			var subDirs = Directory.EnumerateDirectories(dir);
			var loader = ClassifiedImageLoader.Local;
			foreach (var subDirectory in subDirs)
			{
				var type = Path.GetFileName(subDirectory);
				var files = Directory.EnumerateFiles(subDirectory);
				foreach (var file in files)
				{
					ClassifiedImage classifiedImage = null;
					Assert.DoesNotThrow(() => classifiedImage = loader.Load(file));
					Assert.That(loader.LabelProcessor.GetName(classifiedImage.Label), Is.EqualTo(type));
				}
			}
		}

		[Test]
		public void Save()
		{
			var dirs = new[]
			{
				Database.AllImages,
//				LearningBase.TrainingSet,
//				LearningBase.TestSet
			};
			foreach (var dir in dirs)
			{
				var files = Directory.GetFiles(dir, "*" + FileExtensions.Image, SearchOption.AllDirectories);
				foreach (var file in files)
				{
					var originalFile = Path.GetFileNameWithoutExtension(file);
					originalFile = Path.GetFileNameWithoutExtension(originalFile);
					var originalPath = @"..\..\..\..\Data\" + originalFile + ".jpg";
					if (!File.Exists(originalPath))
					{
						Console.Out.WriteLine("No file : " + originalFile);
						continue;
					}
					var image = new Image<Bgr, byte>(originalPath);
					image.Resize(1280.0 / image.Width, INTER.CV_INTER_AREA).Save(file);
				}
			}
		}
	}
}