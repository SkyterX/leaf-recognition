﻿using System;
using System.Collections.Generic;
using System.Linq;
using LeafRecognition.Recognition;
using LeafRecognition.Recognition.Classification;
using LeafRecognition.Util;
using Tests.Recognition.Data;
using Tests.Recognition.TestingTools;
using Tests.Tools;

namespace Tests.Recognition.Learning
{
	public class Learner
	{
		private readonly IClassifierTrainer trainer;
		private readonly ClassifiedVectorLoader vectorLoader;
		private readonly Random random;
		private readonly string recognizerTag;
		private readonly List<ClassifiedVector> trainingSet;
		private readonly List<ClassifiedVector> validationSet;
		private readonly bool fullLog;

		public Learner(IClassifierTrainer trainer, ClassifiedVectorLoader vectorLoader, Random random, 
			string recognizerTag = null, bool fullLog = true)
		{
			this.trainer = trainer;
			this.vectorLoader = vectorLoader;
			this.random = random;
			this.recognizerTag = recognizerTag;
			this.fullLog = fullLog;
			trainingSet = new List<ClassifiedVector>();
			validationSet = new List<ClassifiedVector>();
		}

		public Learner LearnAndTest()
		{
			Log("");	
			Log("Training set   : {0}", trainingSet.Count);
			Log("Validation set : {0}", validationSet.Count);
			var tr = Measure.Time(() => trainer.Train(trainingSet.Shuffle(random)));
			Log("Building classifier elapsed : {0}", tr.TimeElapsed);
			Log("Testing classifier ...");
			var recognizer = new ImageRecognizer(tr.Result, vectorLoader.InnerVectorizer);
			var testingResult = recognizer.TestOn(validationSet, vectorLoader, fullLog);
			Log(testingResult.Stats.Info);
			Log(testingResult.Stats.PerClassInfo);
			if(fullLog)
				Log(testingResult.Stats.ConfusionMatrixInfo);
			return this;
		}

		public string LearnFinalClassifier()
		{
			Log("");
			Log("Building final classifier");
			var vectors = trainingSet.Concat(validationSet).Shuffle(random);
			Log("Total vectors : {0}", vectors.Length);
			var tr = Measure.Time(() => trainer.Train(vectors));
			Log("Building classifier elapsed : {0}", tr.TimeElapsed);
			var recognizer = new ImageRecognizer(tr.Result, vectorLoader.InnerVectorizer);
			return Utils.SaveRecognizer(recognizer, recognizerTag);
		}

		public Learner WithTrainingSet(string dir)
		{
			trainingSet.AddRange(LoadFiles(dir));
			return this;
		}

		public Learner WithValidationSet(string dir)
		{
			validationSet.AddRange(LoadFiles(dir));
			return this;
		}

		public Learner DivideIntoTrainAndValidationSets(string dir, double trainPart)
		{
			var vectors = LoadFiles(dir).ToList();
			var trainingSetPart = vectors.GetProportionalSubset(trainPart, random);
			var validationSetPart = vectors.Where(cv => !trainingSetPart.Contains(cv));
			trainingSet.AddRange(trainingSetPart);
			validationSet.AddRange(validationSetPart);
			return this;
		}

		public IEnumerable<ClassifiedVector> LoadFiles(string dir)
		{
			Log("Loading {0}", dir);
			var loaded = vectorLoader.LoadFiles(dir).ToList();
			Log("   {0} files loaded", loaded.Count);
			return loaded;
		}
		
		private void Log(string format, params object[] args)
		{
			Console.Out.WriteLine(format, args);
		}
	}
}