﻿using System.Collections.Generic;
using LeafRecognition.Recognition.Classification;

namespace Tests.Recognition.Learning
{
	public class KNearestNeighborsTrainer : IClassifierTrainer
	{
		private readonly int nClasses;
		public int KNeighbors;

		public KNearestNeighborsTrainer(int nClasses)
		{
			KNeighbors = 1;
			this.nClasses = nClasses;
		}

		public IClassifier Train(IList<ClassifiedVector> vectors)
		{
			return new KNearestNeighbors(KNeighbors, nClasses, vectors);
		}
	}
}