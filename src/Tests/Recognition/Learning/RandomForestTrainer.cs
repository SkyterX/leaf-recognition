﻿using System.Collections.Generic;
using LeafRecognition.Recognition.Classification;

namespace Tests.Recognition.Learning
{
	public class RandomForestTrainer : IClassifierTrainer
	{
		private readonly int nClasses;
		public int RandomSeed;
		public int NumberOfTrees;
		public double TrainToTestRatio;
		public double FeaturesPerSplitRatio;

		public RandomForestTrainer(int nClasses)
		{
			this.nClasses = nClasses;
			RandomSeed = 0;
			NumberOfTrees = 50;
			TrainToTestRatio = 0.632;
			FeaturesPerSplitRatio = 0.5;
		}

		public IClassifier Train(IList<ClassifiedVector> vectors)
		{
			return new RandomForest(vectors, nClasses, RandomSeed, NumberOfTrees,
				FeaturesPerSplitRatio, TrainToTestRatio);
		}
	}
}