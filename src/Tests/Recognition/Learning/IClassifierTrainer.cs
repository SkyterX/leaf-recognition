﻿using System.Collections.Generic;
using LeafRecognition.Recognition.Classification;

namespace Tests.Recognition.Learning
{
	public interface IClassifierTrainer
	{
		IClassifier Train(IList<ClassifiedVector> vectors);
	}

	public class BaselineTrainer : IClassifierTrainer
	{
		private readonly int nClasses;

		public BaselineTrainer(int nClasses)
		{
			this.nClasses = nClasses;
		}

		public IClassifier Train(IList<ClassifiedVector> vectors)
		{
			return new BaselineClassifier(nClasses, vectors);
		}
	}
}