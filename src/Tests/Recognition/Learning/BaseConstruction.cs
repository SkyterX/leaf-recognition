﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using LeafRecognition.Util;
using NUnit.Framework;
using Tests.Tools;

namespace Tests.Recognition.Learning
{
	[TestFixture]
	public class BaseConstruction
	{
		private const double TestSetRatio = 0.3;

		[Test]
		public void SplitImageBase()
		{
			var dir = Database.AllImages;
			var innerDirs = Directory.GetDirectories(dir);
			var random = new Random(12412432);
			foreach (string innerDir in innerDirs)
			{
				var suffix = Path.GetFileName(innerDir);
				Console.Out.WriteLine(suffix);
				var images = Directory.GetFiles(innerDir).Shuffle(random);
				var testLen = (int) Math.Ceiling(images.Length*TestSetRatio);
				var testSet = images.Take(testLen);
				var trainingSet = images.Skip(testLen);
				CopyFiles(testSet, LearningBase.TestSet);
				CopyFiles(trainingSet, LearningBase.TrainingSet);
			}
		}

		private void CopyFiles(IEnumerable<string> files, string baseName)
		{
			var dir = Path.Combine(LearningBase.RootPath, baseName);
			if (!Directory.Exists(dir))
				Directory.CreateDirectory(dir);
			foreach (string file in files)
			{
				var fileName = Path.GetFileName(file);
				if (fileName == null)
					Console.Out.WriteLine("Bad file : {0}", file);
				else
					File.Copy(file, Path.Combine(dir, fileName), true);
			}
		}
	}
}