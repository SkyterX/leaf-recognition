﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using LeafRecognition.Util;
using NUnit.Framework;
using Tests.Recognition.Data;
using Tests.Recognition.Data.ImageCLEF;
using Tests.Recognition.TestingTools;
using Tests.Tools;

namespace Tests.Recognition.Tests
{
	[TestFixture]
	public class CLEFBase_Tests
	{
		public enum RecognizerType
		{
			Global,
			RandomForest,
			KNearestNeighbors,
		}

		[Test]
		[Explicit]
		public void Global()
		{
			RunTest(Recognizers.CLEF_RandomForest, RecognizerType.Global);
		}

		[Test]
		[Explicit]
		public void CLEFScore()
		{
			var recognizer = Recognizers.CLEF_RandomForest;
			var scanScore = CalculateCLEFScore(recognizer, CLEFXmlType.Scan) * 100;
			var pseudoscanScore = CalculateCLEFScore(recognizer, CLEFXmlType.Pseudoscan) * 100;
			var averageScore = (scanScore + pseudoscanScore) / 2;
			Console.Out.WriteLine("Scan score       : {0}", scanScore.ToPercentage());
			Console.Out.WriteLine("Pseudoscan score : {0}", pseudoscanScore.ToPercentage());
			Console.Out.WriteLine("Average score    : {0}", averageScore.ToPercentage());
		}

		public double CalculateCLEFScore(string recognizerName, CLEFXmlType imageType)
		{
			var recognizer = Utils.LoadRecognizer(recognizerName);
			var dir = CLEFBase.TestSet;
			var files = Directory.EnumerateFiles(dir, "*" + FileExtensions.Image);
			var scoreById = new Dictionary<int, List<double>>();
			var plantsByUser = new Dictionary<string, List<int>>();
			foreach (var file in files)
			{
				var markup = CLEFMarkup.Load(file);
				var id = markup.IndividualPlantId;
				var user = markup.Author;
				if (markup.Type != imageType) continue;
				
				if(!plantsByUser.ContainsKey(user))
					plantsByUser.Add(user, new List<int>());
				plantsByUser[user].Add(id);

				if(!scoreById.ContainsKey(id))
					scoreById.Add(id, new List<double>());
				var classifiedVector = ClassifiedVectorLoader.CLEFDefault.Load(file);
				var result = recognizer.Classifier.Classify(classifiedVector.Vector);
				var rank = result.Confidence.Count(x => x + 1e-5 > result.Confidence[classifiedVector.Label]);
				var score = 1.0/(rank + 1);
				scoreById[id].Add(score);
			}

			return plantsByUser.Values
				.Select(plants => plants
					.Select(id => scoreById[id]
						.Average())
					.Average())
				.Average();
		}

		[Test]
		[Explicit]
		public void ShowFullHistory()
		{
			var type = RecognizerType.Global;

			var history = Utils.LoadHistory(CLEFBase.RootPath, type.ToString());
			Console.Out.WriteLine(history);
			foreach (var testingResult in history)
			{
				testingResult.LabelProcessor = CLEFClassId.Processor;
				Console.Out.WriteLine(new string('=', 65));
				Console.Out.WriteLine(new string('=', 65));
				Console.Out.WriteLine(new string('=', 65));
				Console.Out.WriteLine(testingResult.Info);
				Console.Out.WriteLine(testingResult.Stats.Info);
				Console.Out.WriteLine(testingResult.Stats.PerClassInfo);
			}
		}

		public static void RunTest(string recognizer, RecognizerType recognizerType)
		{
			var testName = recognizerType.ToString();

			Console.Out.WriteLine("Loading recognizer...");
			var imageRecognizer = Utils.LoadRecognizer(recognizer);
			Console.Out.WriteLine("Testing...");
			var timedResult = Measure.Time(() => imageRecognizer.TestOn(CLEFBase.TestSet, ClassifiedVectorLoader.CLEFDefault));
			Console.Out.WriteLine("Time elapsed : {0}", timedResult.TimeElapsed);
			var testingResult = timedResult.Result;
			Console.Out.WriteLine("");

			var history = Utils.LoadHistory(CLEFBase.RootPath, testName);
			Console.Out.WriteLine(history);
			Console.Out.WriteLine("Now :");
			Console.Out.WriteLine(testingResult.ShortInfo);
			Console.Out.WriteLine(testingResult.Stats.Info);
			Console.Out.WriteLine(testingResult.Stats.PerClassInfo);
			var bestResult = history.MaxOrDefault();
			if (!history.Contains(testingResult))
			{
				history.Add(testingResult);
				history.SaveHistory(CLEFBase.RootPath, testName);
			}
			if (bestResult != null)
				Assert.That(testingResult, Is.GreaterThanOrEqualTo(bestResult),
					"Bad result. WORK HARDER!!!");
		}
	}
}