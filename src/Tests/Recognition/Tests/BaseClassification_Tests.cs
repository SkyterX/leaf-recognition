using System;
using System.Globalization;
using System.IO;
using System.Linq;
using LeafRecognition.Recognition.Tools;
using NUnit.Framework;
using Tests.Recognition.Data.ImageCLEF;
using Tests.Recognition.TestingTools;
using Tests.Tools;

namespace Tests.Recognition.Tests
{
	[TestFixture]
	public class BaseClassification_Tests
	{
		[Test]
		[Explicit]
		public static void ClassifyLearningBase()
		{
			const double minConfidence = 0.05;

			var recognizerName = Recognizers.CLEF_RandomForest;
			var recognizer = Utils.LoadRecognizer(recognizerName);
			var labelProcessor = CLEFClassId.Processor;

			var dir = Database.AllImages;
			var subDirs = Directory.EnumerateDirectories(dir);
			foreach (var subDirectory in subDirs)
			{
				var type = Path.GetFileName(subDirectory);
				var agg = new VectorStatisticsAggregator(labelProcessor.NClasses);
				var files = Directory.EnumerateFiles(subDirectory);
				foreach (var file in files)
				{
					var result = recognizer.Recognize(LazyImage.Loader.Load(file));
					agg.AddVector(result.Confidence);
				}

				var mean = agg.Mean;
				var ranking = Enumerable.Range(0, labelProcessor.NClasses)
					.OrderByDescending(x => mean[x])
					.TakeWhile(x => mean[x] > minConfidence);
				Console.Out.WriteLine("Rankings for type : {0}", type);
				foreach (var id in ranking)
				{
					Console.Out.WriteLine("\t{0} : {1}", (mean[id] * 100).ToPercentage(), labelProcessor.GetName(id));
				}
				Console.Out.WriteLine(new string('=', 50));
			}
		}
	}
}