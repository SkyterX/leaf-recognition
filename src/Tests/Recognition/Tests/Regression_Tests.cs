﻿using System;
using System.Linq;
using LeafRecognition.Util;
using NUnit.Framework;
using Tests.Recognition.Data;
using Tests.Recognition.TestingTools;
using Tests.Tools;

namespace Tests.Recognition.Tests
{
	[TestFixture]
	public class Regression_Tests
	{
		public enum RecognizerType
		{
			Global,
			RandomForest,
			KNearestNeighbors,
		}

		[Test]
		[Explicit]
		public void Global()
		{
			RunTest(Recognizers.RandomForest, RecognizerType.Global);
		}

		[Test]
		[Explicit]
		public void RandomForest()
		{
			RunTest(Recognizers.RandomForest, RecognizerType.RandomForest);
		}

		[Test]
		[Explicit]
		public void KNearestNeighbors()
		{
			RunTest(Recognizers.FiveNearestNeigbors, RecognizerType.KNearestNeighbors);
		}

		[Test]
		[Explicit]
		public void SetBaseline()
		{
			RunTest(Recognizers.Baseline, RecognizerType.RandomForest);
		}

		[Test]
		[Explicit]
		public void ShowFullHistory()
		{
			var type = RecognizerType.Global;

			var history = Utils.LoadHistory(LearningBase.RootPath, type.ToString());
			Console.Out.WriteLine(history);
			foreach (var testingResult in history)
			{
				testingResult.LabelProcessor = LeafTypes.Processor;
				Console.Out.WriteLine(new string('=', 65));
				Console.Out.WriteLine(testingResult.Info);
				Console.Out.WriteLine(testingResult.Stats.FullInfo);
			}
		}

		public static void RunTest(string recognizer, RecognizerType recognizerType)
		{
			var testName = recognizerType.ToString();

			Console.Out.WriteLine("Loading recognizer...");
			var imageRecognizer = Utils.LoadRecognizer(recognizer);
			Console.Out.WriteLine("Testing...");
			var timedResult = Measure.Time(() => imageRecognizer.TestOn(LearningBase.TestSet, ClassifiedVectorLoader.LocalDefault));
			Console.Out.WriteLine("Time elapsed : {0}", timedResult.TimeElapsed);
			var testingResult = timedResult.Result;
			Console.Out.WriteLine("");

			var history = Utils.LoadHistory(LearningBase.RootPath, testName);
			Console.Out.WriteLine(history);
			Console.Out.WriteLine("Now :");
			Console.Out.WriteLine(testingResult.ShortInfo);
			Console.Out.WriteLine(testingResult.Stats.FullInfo);
			var bestResult = history.MaxOrDefault();
			if (!history.Contains(testingResult))
			{
				history.Add(testingResult);
				history.SaveHistory(LearningBase.RootPath, testName);
			}
			if (bestResult != null)
				Assert.That(testingResult, Is.GreaterThanOrEqualTo(bestResult),
					"Bad result. WORK HARDER!!!");
		}
	}
}