﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using LeafRecognition.Features;
using LeafRecognition.Recognition.Tools;
using LeafRecognition.Recognition.Vectorization;
using LeafRecognition.Util;
using NUnit.Framework;
using Tests.Recognition.Data;
using Tests.Tools;

namespace Tests.Recognition.Tests
{
	[TestFixture]
	public class Plotting_Tests
	{
		public const int PlotSize = 600;
		public const int Border = 20;
		public const int DescriptionPanelSize = 300;
		public const int LabelSize = 10;

		[Test]
		[Explicit]
		public void PlotPCATest()
		{
			PlotPCA(Vectorizer.Default, LeafType.TypeA, LeafType.TypeC, LeafType.TypeF, LeafType.TypeH, LeafType.TypeI);
		}

		[Test]
		[Explicit]
		public void PlotFeaturesTest()
		{
			var xAxis = new Ratio(Characteristics.LeafArea, Characteristics.LeafDiameter);
			var yAxis = new Ratio(Characteristics.LeafPerimeter, Characteristics.LeafDiameter);
			PlotFeatures(xAxis, yAxis);
		}

		public void PlotPCA(IVectorizer vectorizer, params LeafType[] leafTypes)
		{
			if (leafTypes.Length == 0)
				leafTypes = LeafTypes.Processor.AllLeafTypes;
			double[][] vectors;
			LeafType[] labels;
			LoadVectors(leafTypes, vectorizer, out vectors, out labels);
			if (vectors.Length == 0) return;

			var n = vectors.Length;
			var m = vectors[0].Length;
			var data = new Matrix<double>(n, m);
			var mean = new Matrix<double>(1, m);
			var eigenValues = new Matrix<double>(1, m);
			var eigenVectors = new Matrix<double>(m, m);
			for (var i = 0; i < n; i++)
			{
				for (var j = 0; j < m; j++)
				{
					data[i, j] = vectors[i][j];
				}
			}
			CvInvoke.cvCalcPCA(data.Ptr, mean.Ptr, eigenValues.Ptr, eigenVectors.Ptr, PCA_TYPE.CV_PCA_DATA_AS_ROW);

			var result = new Matrix<double>(n, 2);
			CvInvoke.cvProjectPCA(data.Ptr, mean.Ptr, eigenVectors.Ptr, result.Ptr);
			var vectors2D = new double[n][];
			for (var i = 0; i < n; i++)
			{
				vectors2D[i] = new[] {result[i, 0], result[i, 1]};
			}
			PlotVectors(vectors2D, labels, "PCAx", "PCAy", leafTypes);
		}

		public void PlotFeatures(IFeature xAxis, IFeature yAxis, params LeafType[] leafTypes)
		{
			if (leafTypes.Length == 0)
				leafTypes = LeafTypes.Processor.AllLeafTypes;
			var vectorizer = new Vectorizer(new[] {xAxis, yAxis});
			double[][] vectors;
			LeafType[] labels;
			LoadVectors(leafTypes, vectorizer, out vectors, out labels);
			if (vectors.Length == 0) return;
			PlotVectors(vectors, labels, xAxis.Description, yAxis.Description, leafTypes);
		}

		private static void LoadVectors(LeafType[] leafTypes, IVectorizer vectorizer, out double[][] vectors,
			out LeafType[] labels)
		{
			var loader = new ClassifiedVectorLoader(vectorizer, LeafTypes.Processor);
			var types = leafTypes.Select(type => (int) type).ToSet();
			var classifiedVectors = Directory
				.EnumerateFiles(LearningBase.TrainingSet)
				.Select(loader.ImageLoader.Load)
				.Where(image => types.Contains(image.Label))
				.Select(loader.Load)
				.Select(cv => Tuple.Create(cv.Vector, (LeafType) cv.Label))
				.ToArray();
			labels = classifiedVectors.Select(vec => vec.Item2).ToArray();
			vectors = classifiedVectors.Select(vec => vec.Item1).ToArray();
		}

		public void PlotVectors(double[][] vectors, LeafType[] labels, string xAxisDescription, string yAxisDescription,
			LeafType[] leafTypes)
		{
			var baseStats = new VectorStatisticsAggregator(2).AddVectors(vectors);
			var normalizer = new MeanNormalizer(baseStats.Mean, baseStats.StandardDeviation);
			vectors.ConvertThis(normalizer.Normalize);
			var fVectors = Array.ConvertAll(vectors, vec => new[] {((float) vec[0]), ((float) vec[1])});
			var stats = new VectorStatisticsAggregator(2).AddVectors(vectors);
			var xMin = ((float) stats.MinValue[0]);
			var xMax = ((float) stats.MaxValue[0]);
			var yMin = ((float) stats.MinValue[1]);
			var yMax = ((float) stats.MaxValue[1]);
			var xLen = xMax - xMin;
			var yLen = yMax - yMin;
			var len = Math.Max(xLen, yLen);
			var factor = PlotSize/len;

			fVectors.ConvertThis(vec => new[] {(vec[0] - xMin)*factor + Border, (yMax - vec[1])*factor + Border});

			var width = (int) Math.Ceiling(factor*xLen) + 3*Border + DescriptionPanelSize;
			var height = (int) Math.Ceiling(factor*yLen) + 2*Border;
			using (var bitmap = new Bitmap(width, height))
			using (var graphics = Graphics.FromImage(bitmap))
			{
				graphics.FillRectangle(Brushes.White, 0, 0, width, height);
				graphics.DrawLine(Pens.Gray, new PointF(-xMin*factor + Border, 0), new PointF(-xMin*factor + Border, height));
				graphics.DrawLine(Pens.Gray, new PointF(0, -yMin*factor + Border), new PointF(width, -yMin*factor + Border));

				var panelOffsetX = width - DescriptionPanelSize - Border;
				graphics.FillRectangle(Brushes.LightGray, panelOffsetX, 0, DescriptionPanelSize + Border, height);
				graphics.DrawLine(Pens.Black, new PointF(panelOffsetX, 0), new PointF(panelOffsetX, height));
				panelOffsetX += Border;

				var font = new Font("Arial", 12);
				float top = Border;
				Action<string> drawString = str =>
				{
					graphics.DrawString(str, font, Brushes.Black,
						new RectangleF(panelOffsetX, top, DescriptionPanelSize, height));
					top += graphics.MeasureString(str, font, new SizeF(DescriptionPanelSize, height)).Height;
				};
				drawString("xAxis : ");
				drawString(string.Format("min={0:0.########}, max={1:0.#######}", baseStats.MinValue[0], baseStats.MaxValue[0]));
				drawString(xAxisDescription);
				top += 5;

				drawString("yAxis : ");
				drawString(string.Format("min={0:0.########}, max={1:0.#######}", baseStats.MinValue[1], baseStats.MaxValue[1]));
				drawString(yAxisDescription);
				top += 20;

				drawString("Label colors :");
				var colors = ColorPaletteGeneration
					.GenerateColors_GoldenRatioRainbow()
					.Take(leafTypes.Length)
					.Select(color => new SolidBrush(color))
					.ToArray();
				for (var i = 0; i < leafTypes.Length; i++)
				{
					top += 5;
					var brush = colors[i];
					graphics.FillEllipse(brush, panelOffsetX, top + LabelSize/2, LabelSize, LabelSize);
					drawString("   " + leafTypes[i]);
				}

				for (var idx = 0; idx < fVectors.Length; idx++)
				{
					var fVector = fVectors[idx];
					var labelId = Array.IndexOf(leafTypes, labels[idx]);
					var brush = colors[labelId];
					graphics.FillEllipse(brush, fVector[0], fVector[1], LabelSize, LabelSize);
				}

				ImageViewer.Show(new Image<Bgr, byte>(bitmap));
			}
		}
	}
}