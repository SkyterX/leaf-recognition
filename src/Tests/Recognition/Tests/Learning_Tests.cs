﻿using System;
using NUnit.Framework;
using Tests.Recognition.Data;
using Tests.Recognition.Data.ImageCLEF;
using Tests.Recognition.Learning;
using Tests.Tools;

namespace Tests.Recognition.Tests
{
	[TestFixture]
	[Explicit]
	public class Learning_Tests
	{
		public const int RandomSeed = 204907015;
		private Random random;

		[SetUp]
		public void SetUp()
		{
			random = new Random(RandomSeed);
		}

		#region ImageCLEF base

		[Test]
		public void RandomForest_CLEF()
		{
			var trainer = new RandomForestTrainer(CLEFClassId.Processor.NClasses)
			{
				RandomSeed = RandomSeed,
				NumberOfTrees = 237,
			};
			var recognizer = new Learner(trainer, ClassifiedVectorLoader.CLEFDefault, random, "CLEF_", false)
				.DivideIntoTrainAndValidationSets(CLEFBase.TrainingSet, 0.8)
				.LearnAndTest()
				.LearnFinalClassifier();
			CLEFBase_Tests.RunTest(recognizer, CLEFBase_Tests.RecognizerType.RandomForest);
		}


		[Test]
		public void KNearestNeighbors_CLEF()
		{
			var trainer = new KNearestNeighborsTrainer(CLEFClassId.Processor.NClasses)
			{
				KNeighbors = 1000
			};
			new Learner(trainer, ClassifiedVectorLoader.CLEFDefault, random, "CLEF_", false)
				.DivideIntoTrainAndValidationSets(CLEFBase.TrainingSet, 0.8)
				.LearnAndTest()
				.LearnFinalClassifier();
		}

		[Test]
		public void Baseline_CLEF()
		{
			var trainer = new BaselineTrainer(CLEFClassId.Processor.NClasses);
			new Learner(trainer, ClassifiedVectorLoader.CLEFDefault, random, "CLEF_", false)
				.WithTrainingSet(CLEFBase.TrainingSet)
				.LearnFinalClassifier();
		}

		#endregion

		#region Local base

		[Test]
		public void RandomForest_Local()
		{
			var trainer = new RandomForestTrainer(LeafTypes.Processor.NClasses)
			{
				RandomSeed = RandomSeed,
				NumberOfTrees = 200,
			};
			new Learner(trainer, ClassifiedVectorLoader.LocalDefault, random)
				.DivideIntoTrainAndValidationSets(LearningBase.TrainingSet, 0.8)
				.LearnAndTest()
				.LearnFinalClassifier();
		}

		[Test]
		public void KNearestNeighbors_Local()
		{
			var trainer = new KNearestNeighborsTrainer(LeafTypes.Processor.NClasses)
			{
				KNeighbors = 5
			};
			new Learner(trainer, ClassifiedVectorLoader.LocalDefault, random)
				.DivideIntoTrainAndValidationSets(LearningBase.TrainingSet, 0.8)
				.LearnAndTest()
				.LearnFinalClassifier();
		}

		[Test]
		public void Baseline_Local()
		{
			var trainer = new BaselineTrainer(LeafTypes.Processor.NClasses);
			new Learner(trainer, ClassifiedVectorLoader.LocalDefault, random)
				.WithTrainingSet(LearningBase.TrainingSet)
				.LearnFinalClassifier();
		}

		#endregion
	}
}