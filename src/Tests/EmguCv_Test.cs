﻿using System;
using System.Drawing;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using NUnit.Framework;

namespace Tests
{
	[TestFixture]
	public class EmguCv_Test
	{
		[Test]
		[Explicit]
		public void PlanarSubdivision_Test()
		{
			float maxY = 600;
			float maxX = 800;

			#region create random points in the range of [0, maxValue]

			var pts = new PointF[50];
			var r = new Random((int) (DateTime.Now.Ticks & 0x0000ffff));
			for (int i = 0; i < pts.Length; i++)
				pts[i] = new PointF((float) r.NextDouble()*maxX, (float) r.NextDouble()*maxY);

			#endregion

			Triangle2DF[] delaunayTriangles;
			VoronoiFacet[] voronoiFacets;
			using (var subdivision = new PlanarSubdivision(pts))
			{
				//Obtain the delaunay's triangulation from the set of points;
				delaunayTriangles = subdivision.GetDelaunayTriangles();

				//Obtain the voronoi facets from the set of points
				voronoiFacets = subdivision.GetVoronoiFacets();
			}

			//create an image for display purpose
			var img = new Image<Bgr, byte>((int) maxX, (int) maxY);

			//Draw the voronoi Facets
			foreach (VoronoiFacet facet in voronoiFacets)
			{
				Point[] points = Array.ConvertAll(facet.Vertices, Point.Round);

				//Draw the facet in color
				img.FillConvexPoly(
					points,
					new Bgr(r.NextDouble()*120, r.NextDouble()*120, r.NextDouble()*120)
					);

				//highlight the edge of the facet in black
				img.DrawPolyline(points, true, new Bgr(Color.Black), 2);

				//draw the points associated with each facet in red
				img.Draw(new CircleF(facet.Point, 5.0f), new Bgr(Color.Red), 0);
			}

			//Draw the Delaunay triangulation
			foreach (Triangle2DF triangles in delaunayTriangles)
			{
				img.Draw(triangles, new Bgr(Color.White), 1);
			}

			//display the image
			ImageViewer.Show(img, "Plannar Subdivision");
		}
	}
}