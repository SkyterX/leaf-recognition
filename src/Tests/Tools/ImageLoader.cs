using System.Collections.Concurrent;
using System.Collections.Generic;
using Emgu.CV;
using Emgu.CV.Structure;
using LeafRecognition.Images;

namespace Tests.Tools
{
	public class LazyImage
	{
		public static readonly LazyImage Loader = new LazyImage(150);

		private readonly IDictionary<string, RgbImage> cache;
		private readonly Queue<string> cacheQueue;
		private readonly int maxItemsInCache;
		private readonly object cacheAddLock = new object();

		private LazyImage(int maxItemsInCache)
		{
			this.maxItemsInCache = maxItemsInCache;
			cache = new ConcurrentDictionary<string, RgbImage>();
			cacheQueue = new Queue<string>(maxItemsInCache);
		}

		public RgbImage Load(string filePath)
		{
			RgbImage image;
			if (cache.TryGetValue(filePath, out image))
				return image;
			image = new Image<Rgb, byte>(filePath).ToRgbImage();

			lock (cacheAddLock)
			{
				if (!cache.ContainsKey(filePath))
				{
					cache[filePath] = image;
					cacheQueue.Enqueue(filePath);
					if (cacheQueue.Count > maxItemsInCache)
						cache.Remove(cacheQueue.Dequeue());
				}
			}

			return image;
		}
	}
}