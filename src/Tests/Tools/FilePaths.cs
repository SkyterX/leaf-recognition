﻿namespace Tests.Tools
{
	public static class Database
	{
		public const string RootPath = @"..\..\Data\";

		public const string CharacteristicsDir = @"Characteristics\";

		public const string TestResults = @"TestResults\";

		public const string Samples = RootPath + @"Samples\";
		public const string AllImages = RootPath + @"AllImages\";

		public const string Recognizers = RootPath + @"Recognizers\";
	}

	public static class LearningBase
	{
		public const string RootPath = Database.RootPath + @"LearningBase\";
		public const string TrainingSet = RootPath + @"TrainingSet\";
		public const string TestSet = RootPath + @"TestSet\";
	}

	public static class CLEFBase
	{
		public const string RootPath = Database.RootPath + @"ImageCLEF\";
		public const string TrainingSet = RootPath + @"Train\";
		public const string TestSet = RootPath + @"Test\";
		public const string ClassIdMappings = RootPath + @"classId.mappings";
	}

	public static class FileExtensions
	{
		public const string Image = ".jpg";
		public const string History = ".history";
		public const string CLEFMarkup = ".xml";
		public const string Recognizer = ".rcz";
		public const string Characteristics = ".chars";
	}

	public static class Recognizers
	{
		public const string Baseline = "Baseline";
		public const string OneNearestNeigbor = "1-NearestNeighbors_F02EB8939F56E36738BEC18D0A1CC39F";
		public const string ThreeNearestNeigbors = "3-NearestNeighbors_0871994370F1D80819B30ED128B61692";
		public const string FiveNearestNeigbors = "5-NearestNeighbors_D56F2BC225DA356EACA185F37A3922A2";
		public const string RandomForest = "RandomForest-100-12-0632_910AD4EF0BC78262118D138A65F60774";

		public const string CLEF_Baseline = "CLEF_Baseline";
		public const string CLEF_RandomForest = "CLEF_RandomForest-239-12-0632_F13937D436426AECB9A095C55E1978EF";
	}

	public static class SampleImage
	{
		public const string Halftones = "HalftonesImage.jpg";
		public const string NonHalftones = "NonHalftonesImage.JPG";
		public const string MultipleComponents = "MultipleComponents.jpg";
		public const string SampleMaple = "SampleMaple.JPG";
		public const string SampleOak = "SampleOak.JPG";
		public const string SmallBitmap = "SmallBitmap.jpg";
        public const string BasicCSS = "BasicCSS.PNG";
        public const string MapleMini = "MapleMini.jpg";
        public const string MapleMiniHalftoned = "MapleMiniHalftoned.jpg";
	    public const string DarkImage = "DarkImage.jpg";
	    public const string TestingPicture1 = "testingPicture1.png";
	}
}