﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace Tests.Tools
{
	public static class ColorPaletteGeneration
	{
		public static IEnumerable<Color> GenerateColors_GoldenRatioRainbow(Random random = null)
		{
			random = random ?? new Random(417591247);
			const float saturation = 1.0f;
			const float luminance = 0.4f;
			const float goldenRatioConjugate = 0.618033988749895f;

			var currentHue = (float) random.NextDouble();
			while (true)
			{
				var hslColor = new HSL(currentHue, saturation, luminance);

				yield return hslColor.Color;

				currentHue += goldenRatioConjugate;
				currentHue %= 1.0f;
			}
		}

		/// <summary>
		/// HSL (Hue/Saturation/Luminance) Class
		/// </summary>
		private class HSL
		{
			private readonly double h;
			private readonly double s;
			private readonly double l;

			/// <summary>
			/// Constructor with HSL
			/// </summary>
			/// <param name="hue">Varies from magenta - red - yellow - green - cyan - blue - magenta, described as an angle around a circle from 0.0 - 360.0 degrees</param>
			/// <param name="saturation">Varies from 0.0 and 1.0 and describes how "grey" the colour is, with 0 being completely unsaturated (grey, white or black) and 1 being completely saturated</param>
			/// <param name="luminance">Varies from 0.0 and 1.0 and ranges from black at 0.0, through the standard colour itself at 0.5 to white at 1.0</param>
			public HSL(double hue, double saturation, double luminance)
			{
				h = hue;
				s = saturation;
				l = luminance;
			}


			/// <summary>
			/// <para>Convert from the current HSL to RGB</para>
			/// <para>http://en.wikipedia.org/wiki/HSV_color_space#Conversion_from_HSL_to_RGB</para>
			/// </summary>
			public Color Color
			{
				get
				{
					double[] t = {0, 0, 0};

					var tH = h;
					var tS = s;
					var tL = l;

					if (tS.Equals(0))
					{
						t[0] = t[1] = t[2] = tL;
					}
					else
					{
						var q = tL < 0.5 ? tL*(1 + tS) : tL + tS - (tL*tS);
						var p = 2*tL - q;

						t[0] = tH + (1.0/3.0);
						t[1] = tH;
						t[2] = tH - (1.0/3.0);

						for (byte i = 0; i < 3; i++)
						{
							t[i] = t[i] < 0 ? t[i] + 1.0 : t[i] > 1 ? t[i] - 1.0 : t[i];

							if (t[i]*6.0 < 1.0)
								t[i] = p + ((q - p)*6*t[i]);
							else if (t[i]*2.0 < 1.0)
								t[i] = q;
							else if (t[i]*3.0 < 2.0)
								t[i] = p + ((q - p)*6*((2.0/3.0) - t[i]));
							else
								t[i] = p;
						}
					}
					return Color.FromArgb((int) (t[0]*255), (int) (t[1]*255), (int) (t[2]*255));
				}
			}
		}
	}
}