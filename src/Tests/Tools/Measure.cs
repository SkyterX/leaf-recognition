﻿using System;
using System.Diagnostics;

namespace Tests.Tools
{
	public static class Measure
	{
		public static TimeSpan Time(Action action)
		{
			var timer = Stopwatch.StartNew();
			action();
			timer.Stop();
			return timer.Elapsed;
		}

		public static TimedResult<TResult> Time<TResult>(Func<TResult> action)
		{
			var timer = Stopwatch.StartNew();
			var result = action();
			timer.Stop();
			return new TimedResult<TResult>(result, timer.Elapsed);
		}

		public class TimedResult<TResult>
		{
			public TResult Result;
			public TimeSpan TimeElapsed;

			public TimedResult(TResult result, TimeSpan timeElapsed)
			{
				Result = result;
				TimeElapsed = timeElapsed;
			}
		}
	}
}