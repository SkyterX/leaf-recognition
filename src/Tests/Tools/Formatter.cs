﻿using System;
using System.Globalization;
using System.Text;

namespace Tests.Tools
{
	public static class Formatter
	{
		public static string ToSeconds(this TimeSpan timeSpan)
		{
			return string.Format("{0}.{1}s", (int)timeSpan.TotalSeconds, timeSpan.ToString("fffffff"));
		}

		public static string ToMilliseconds(this TimeSpan timeSpan)
		{
			return timeSpan.TotalMilliseconds.ToString(CultureInfo.InvariantCulture) + "ms";
		}

		public static StringBuilder AppendFormatLine(this StringBuilder builder, string format, params object[] args)
		{
			return builder.AppendFormat(format, args).AppendLine();
		}

		public static string ToPercentage(this double value)
		{
			return string.Format(CultureInfo.InvariantCulture, "{0, 6:##0.00}%", value);
		}
	}
}