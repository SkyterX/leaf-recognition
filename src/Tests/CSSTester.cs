using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using LeafRecognition.CssInvestivation;
using LeafRecognition.Filters;
using LeafRecognition.Images;
using NUnit.Framework;
using Tests.Recognition.Data;
using Tests.Recognition.Tests;
using Tests.Tools;

namespace Tests
{

    [TestFixture]
    internal  class CSSTester
    {
        public static  Bgr RedBgr = new Bgr(0, 0, 255);
        public static  Bgr BlueBgr = new Bgr(255, 0, 0);
        public static  Bgr GreenBgr = new Bgr(0, 255, 0);
        public static  Bgr BlackBgr = new Bgr(0, 0, 0);
        public static  Bgr PurpleBgr = new Bgr(Color.Purple);



        [Test]
        [Explicit]
        static public void DrawCSSTest()
        {
            //@"c:\Users\Kris\Desktop\LeafRecognition\leaf-recognition\src\Tests\Data\LearningBase\TrainingSet\Contour\";

            var logger = new SimpleLogger("drawcss.log");
            var dir = Database.Samples;
            var fileName = SampleImage.MapleMiniHalftoned;

            var imagePath = Path.Combine(dir, fileName);
            var image = new Image<Bgr, byte>(new Bitmap(imagePath));
            var img1 = image.Convert<Gray, byte>().Resize(0.3, INTER.CV_INTER_CUBIC);
            var img = new Image<Gray, byte>(img1.Size + new Size(2, 2));
            CvInvoke.cvCopyMakeBorder(img1, img, new Point(1, 1), BORDER_TYPE.CONSTANT, new Bgr(Color.White).MCvScalar); // for edge cases
            


            var contourFinder = new ContourFinder(img);
            contourFinder.ChooseTheFirstContour();
            var contourPoints = contourFinder.ChosedContourPoints();
            var lenContour = contourPoints.Length;

            var p = new CssParameters
            {
                AmountOfCriticalPointsTypes = 3,
                ChooseFuncs = new Func<double, bool>[]
                {
                    new LessThen(-0.003).ChooseFunc(),
                    new AbsLessThen(05e-5).ChooseFunc(),
                    new MoreThen(0.006).ChooseFunc()
                },
                IsCssNormalizerUnique = true,
                CssNormailzerGenerateFunc = obj => new NullCssNormalizer(),
//                CssNormailzerGenerateFunc = obj => new MultipleDenumNormalizer((double) obj),
//                CssNormailzerGenerateFunc = obj =>
//                {
//                    return new MultipleDenumNormalizer(
//                        Math.Abs(((double[]) obj).Min())
//                        );
//                    //                var absmaxKs = ks.Select(Math.Abs).Max();
//                    //                var absminKs = ks.Select(Math.Abs).Min();
//                    //                var minKs = Math.Abs(ks.Min());
//                    //                var maxKs = Math.Abs(ks.Max());
//                },

                Maxsigma = lenContour / 2,
                StepSigma = 1,
                StartSigma = 3,
                LenDelta = 3,
                CriticalPointsAbsorbRate = 0.7
            };


            var cssCreator = new CssCreator(p, contourPoints);
            cssCreator.CreateCss();
            cssCreator.FilterCriticalPoints();
            bool[][,] cssMatrixesArray = cssCreator.CSSMatrixesArray;
            HashSet<Point>[] criticalPointsArray = cssCreator.CriticalPointsArray;


            var fileNames = new[]
            {
                "cssLess.jpg",
                "cssZero.jpg",
                "cssMore.jpg"
            };
            var outimg = new Image<Bgr, byte>(img.Width, img.Height, new Bgr(255, 255, 255));
            foreach (var contourPoint in contourPoints) // draw contour
            {
                outimg[contourPoint.Y, contourPoint.X] = BlackBgr;
            }
            var colors = new[]
            {
                RedBgr,
                GreenBgr,
                BlueBgr
            };

            var cssDrawersArray = new[]
            {
                new CssDrawer(lenContour, p.Maxsigma),
                new CssDrawer(lenContour, p.Maxsigma),
                new CssDrawer(lenContour, p.Maxsigma)
            };
            foreach (int i in new[] {0, 1, 2})
            {
                var cssDrawer = cssDrawersArray[i];
                cssDrawer.DrawPoints(cssMatrixesArray[i], RedBgr);
                cssDrawer.DrawPoints(criticalPointsArray[i], GreenBgr, 3);
                cssDrawer.CSSImage
                    .Flip(FLIP.VERTICAL)
                    .Save(Path.Combine(dir, fileNames[i]));
                DrawCriticalPoints(colors[i], criticalPointsArray[i], contourPoints, outimg);
            }

            ImageViewer.Show(outimg, "CriticalPoints");
            outimg.Save(Path.Combine(dir, "criticalPoints.jpg"));

          
        }

        private static void DrawCriticalPoints(Bgr color, IEnumerable<Point> criticalPoints, Point[] contourPoints, Image<Bgr, byte> outimg)
        {
            foreach (var criticalPoint in criticalPoints)
            {
                var u = criticalPoint.X;
                var loc = contourPoints[u];
                var sigma = criticalPoint.Y;
                outimg.Draw(new CircleF(loc, sigma), color, 1);
                outimg.Draw(new CircleF(loc, 0), color, 2);
            }
        }

       


        [Test]
        [Explicit]
        public void SmoothTheContourTest()
        {

            var imagePath = Path.Combine(Database.Samples, SampleImage.MapleMiniHalftoned);
            Image<Bgr, Byte> image = new Image<Bgr, byte>(new Bitmap(imagePath));
            var img = image.Convert<Gray, byte>();

            var outimg = new Image<Bgr, byte>(img.Width, img.Height , new Bgr(255, 255, 255));
            var contourFinder = new ContourFinder(img);
            contourFinder.ChooseTheSecondContour();
            var contourPoints = contourFinder.ChosedContourPoints();
            var ksize = contourPoints.Length.CloseOdd();
            const int sigma = 250;
            var lineGaussianKernel = Helper.CreateLineGaussianKernel(ksize, sigma);
            ConvolutionKernelF gaussKernel =
                new ConvolutionKernelF(lineGaussianKernel).Transpose(); // transpose?
            var xImg = contourPoints.Select(p => p.X).FuncToImgDoubled(contourPoints.Length);
            var yImg = contourPoints.Select(p => p.Y).FuncToImgDoubled(contourPoints.Length);

            var xintensities = xImg.Convolution(gaussKernel)
                .ImgToFuncDoubled(contourPoints.Length)
                .FloatToInt();
            var yintensities = yImg.Convolution(gaussKernel)
                .ImgToFuncDoubled(contourPoints.Length)
                .FloatToInt();
            foreach (var ptuple in xintensities.Zip(yintensities, Tuple.Create))
            {
                outimg[ptuple.Item2, ptuple.Item1] = RedBgr;
            }

            // critical points
            var c = new CurvatureOfClosedLine(contourPoints);
            var ks = c.GetCurveMeasureInSmoothedContour(sigma);
            var zeroIndxs = ks.Select((k, i) => Tuple.Create(k, i))
                    .Where(t => Math.Abs(t.Item1) < 1e-6)
                    .Select(t => new {Point = contourPoints[t.Item2], Color = BlackBgr})
                    .ToArray();
            var bigIndxs = ks.Select((k, i) => Tuple.Create(k, i))
                    .Where(t => t.Item1 > 0.0005)
                    .Select(t => new { Point = contourPoints[t.Item2], Color = GreenBgr})
                    .ToArray();
            var bignegIndxs = ks.Select((k, i) => Tuple.Create(k, i))
                    .Where(t => t.Item1 < -0.005)
                    .Select(t => new { Point = contourPoints[t.Item2], Color = BlueBgr})
                    .ToArray();
            foreach (var ind in zeroIndxs.Concat(bigIndxs).Concat(bignegIndxs))
            {
                outimg.Draw(new CircleF(ind.Point, 2), ind.Color, 2);
            }
            ImageViewer.Show(outimg, "CurvatureScaleSpace");
            outimg.Save(Path.Combine(Database.Samples, "CriticalPoints.jpg"));
        }

        [Test]
        [Explicit]
        public void DrawAllContours()
        {
            var imagePath = Path.Combine(Database.Samples, SampleImage.MapleMiniHalftoned);
            Image<Bgr, Byte> imageoldimge = new Image<Bgr, byte>(imagePath);
            var halftonedImage = imageoldimge.Convert<Gray, byte>().Resize(0.3, INTER.CV_INTER_CUBIC);
            var image = new Image<Gray, byte>(halftonedImage.Size + new Size(2, 2));
            CvInvoke.cvCopyMakeBorder(halftonedImage, image, new Point(1, 1), BORDER_TYPE.CONSTANT, new Bgr(Color.White).MCvScalar); // for edge cases
            
            var outimg = new Image<Bgr, byte>(image.Width, image.Height, new Bgr(255, 255, 255));
            var maxCount = 0;
            var maxwidth = 0;
            var indexOfMax = 0;
            Contour<Point> maxContour = null;
            Point[] contourPoints = {};
            // get contour of the leaf
            // ��� ��������� ������ ������ � ���� ������ ���� ��� ����� ����������?
            var externalColor = new MCvScalar(255, 0, 0);
            var internalColor = new MCvScalar(0, 255, 0);
            using (MemStorage storage = new MemStorage())
            {
                Contour<Point> contourEnum = halftonedImage.FindContours(CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_NONE,
                    RETR_TYPE.CV_RETR_LIST, storage);
                var i = 0;
                while (contourEnum != null)
                {
                    Contour<Point> currentContour = contourEnum.ApproxPoly(contourEnum.Perimeter*0.015, storage);
                    var curContourWidth = currentContour.BoundingRectangle.Width;
                    if (curContourWidth > 20)
                    {
                        outimg.Draw(new CircleF(currentContour[0], 1), new Bgr(Color.Purple), 2);
                        CvInvoke.cvDrawContours(outimg, contourEnum, externalColor,
                            internalColor, -1, 1,
                            Emgu.CV.CvEnum.LINE_TYPE.EIGHT_CONNECTED, new Point(0, 0));
                        var count = contourEnum.Count();
                        if (curContourWidth > maxwidth && curContourWidth < image.Width - 10)
                        {
                            indexOfMax = i;
                            maxCount = count;
                            maxwidth = curContourWidth;
                            contourPoints = contourEnum.ToArray();
                            maxContour = contourEnum;
                        }
                    }
                    contourEnum = contourEnum.HNext;
                }
//
                var cf = new ContourFinder(halftonedImage);
                cf.ChooseTheFirstContour();
                foreach (var contourPoint in maxContour.ToArray())
                {
                    outimg[contourPoint.Y, contourPoint.X] = PurpleBgr;
                }
            }

            ImageViewer.Show(outimg, "Contours");
//            outimg.Save(Path.Combine(Database.Samples, "3.jpg"));
        }


        [Test]
        [Explicit]
        public  void filter2dTest()
        {
            var imagePath = Path.Combine(Database.Samples, SampleImage.MapleMiniHalftoned);

            Image<Bgr, Byte> img = new Image<Bgr, byte>(new Bitmap(imagePath));
            var outimg = new Image<Bgr, byte>(img.Width, img.Height, new Bgr(255, 255, 255));
            ConvolutionKernelF gaussKernel = new ConvolutionKernelF(
                Helper.CreateLineGaussianKernel(img.Width, 1)).Transpose(); // transpose?
            CvInvoke.cvFilter2D(img, outimg, gaussKernel, new Point(-1, -1));

            ImageViewer.Show(outimg, "Filter2d");
        }

        [Test]
        [Explicit]
        public void HalftoneItTest()
        {
            var imageFromPath = Path.Combine(Database.Samples, SampleImage.MapleMini);
            var imageToPath = Path.Combine(Database.Samples, SampleImage.MapleMiniHalftoned);
//            var image = LazyImage.Loader.Load(imageFromPath);
//            var filtered = LeafShapeGetter.Instance.Apply(image);
//            filtered.Save(imageToPath);
            var baseImage = new Image<Rgb, byte>(imageFromPath);
            var image = baseImage.Copy(); 
            var size = 7;
            var center = size / 2;
            var w = image.Width;
            var h = image.Height;
            var tmp = image
                .MorphologyEx(
                    new StructuringElementEx(size, size, center, center, CV_ELEMENT_SHAPE.CV_SHAPE_ELLIPSE),
                    CV_MORPH_OP.CV_MOP_GRADIENT, 7)
                .Convert<Gray, byte>();
            var mask = new Image<Gray, byte>(w + 4, h + 4);
            CvInvoke.cvCopyMakeBorder(tmp.Ptr, mask.Ptr, new Point(2, 2), BORDER_TYPE.CONSTANT,
                new MCvScalar(0, 0, 0));
            mask._ThresholdBinary(new Gray(50), new Gray(255));

            MCvConnectedComp comp;
            var fImage = new Image<Rgb, byte>(w + 2, h + 2);
            CvInvoke.cvCopyMakeBorder(image.Ptr, fImage.Ptr, new Point(1, 1), BORDER_TYPE.CONSTANT,
                new MCvScalar(0, 0, 0));
            fImage = fImage.Sub(new Rgb(1, 1, 1));
            CvInvoke.cvFloodFill(fImage.Ptr, new Point(0, 0),
                new MCvScalar(255, 255, 255), new MCvScalar(255, 255, 255), new MCvScalar(255, 255, 255),
                out comp, CONNECTIVITY.EIGHT_CONNECTED, FLOODFILL_FLAG.DEFAULT, mask.Ptr);
            image = fImage.GetSubRect(new Rectangle(1, 1, w, h));

            var imG = image.Convert<Gray, byte>();
            var blacks = imG.ThresholdBinaryInv(new Gray(254), new Gray(255)).GetSum();
            var div = blacks.Intensity / 255;
            imG._Not();
            var avg = imG.GetSum();
            var avgColor = new Gray(255 - avg.Intensity / div);
            Console.Out.WriteLine(avgColor);
            var dX = imG.Sobel(0, 1, 5);
            var dY = imG.Sobel(1, 0, 5);
            imG = dX.Pow(2).Add(dY.Pow(2)).Pow(0.5).Convert<Gray, byte>();

            
            imG.Save(imageToPath);
        }

        [Test]
        [Explicit]
        public void Test()
        {

            var files = Directory.GetFiles(LearningBase.TrainingSet);
            var outDir = Path.Combine(LearningBase.TrainingSet, "Contour");
            Directory.CreateDirectory(outDir);
            foreach (var file in files)
            {
                var fileName = Path.GetFileName(file);

//                var imageFromPath = Path.Combine(Database.Samples, "Sample2.jpg");
                var imageFromPath = Path.Combine(LearningBase.TrainingSet, fileName);
                var imageToPath = Path.Combine(outDir, fileName);

                //            var image = LazyImage.Loader.Load(imageFromPath);
                var baseImage = new Image<Rgb, byte>(imageFromPath);
//                baseImage = baseImage.Resize(500.0/baseImage.Width, INTER.CV_INTER_LANCZOS4);
//            baseImage = baseImage.Resize(0.5, INTER.CV_INTER_CUBIC);
                var image = baseImage.Copy(); //.Convert<Hsv, byte>().Split()[1];
//            image._EqualizeHist();

//            image._ThresholdBinary(image.GetAverage(), new Gray(255));

                var size = 7;
                var center = size/2;
                var w = image.Width;
                var h = image.Height;
                var tmp = image
                    .MorphologyEx(
                        new StructuringElementEx(size, size, center, center, CV_ELEMENT_SHAPE.CV_SHAPE_ELLIPSE),
                        CV_MORPH_OP.CV_MOP_GRADIENT, 7)
                    .Convert<Gray, byte>();
                var mask = new Image<Gray, byte>(w + 4, h + 4);
                CvInvoke.cvCopyMakeBorder(tmp.Ptr, mask.Ptr, new Point(2, 2), BORDER_TYPE.CONSTANT,
                    new MCvScalar(0, 0, 0));
                mask._ThresholdBinary(new Gray(50), new Gray(255));
//            image._Not();
//
//            var value = image.GetSubRect(new Rectangle(w/2 - 70, h/2 - 70, 140, 140)).GetAverage();
//            image._ThresholdBinary(value, new Gray(255));
//            var mask = image.MorphologyEx(new StructuringElementEx(size, size, center, center, CV_ELEMENT_SHAPE.CV_SHAPE_ELLIPSE), CV_MORPH_OP.CV_MOP_OPEN, 1);
//            image = image.Copy(mask);
//
                var x = image.Copy();
//            image._Dilate(1);
//

                MCvConnectedComp comp;
                var fImage = new Image<Rgb, byte>(w + 2, h + 2);
                CvInvoke.cvCopyMakeBorder(image.Ptr, fImage.Ptr, new Point(1, 1), BORDER_TYPE.CONSTANT,
                    new MCvScalar(0, 0, 0));
                fImage = fImage.Sub(new Rgb(1, 1, 1));
                CvInvoke.cvFloodFill(fImage.Ptr, new Point(0, 0),
                    new MCvScalar(255, 255, 255), new MCvScalar(255, 255, 255), new MCvScalar(255, 255, 255),
                    out comp, CONNECTIVITY.EIGHT_CONNECTED, FLOODFILL_FLAG.DEFAULT, mask.Ptr);
                image = fImage.GetSubRect(new Rectangle(1, 1, w, h));

                var imG = image.Convert<Gray, byte>();
                var binaryInv = imG.ThresholdBinaryInv(new Gray(254), new Gray(255));
                var averageBack = baseImage.Convert<Gray, byte>().GetAverage(binaryInv.Not()).Intensity;
                var averageForg = baseImage.Copy(binaryInv.Erode(10)).Convert<Gray, byte>().GetAverage().Intensity;
                var threshold = new Gray((averageBack -20));
                Console.Out.WriteLine(threshold);
                var newImage = imG.ThresholdBinary(threshold, new Gray(255));

                var blacks = binaryInv.GetSum();
                var div = blacks.Intensity/255;
                imG._Not();
                var avg = imG.GetSum();
                var avgColor = new Gray(255 - avg.Intensity/div);
//                Console.Out.WriteLine(avgColor);
//            imG._ThresholdBinary(new Gray(255 - 50), new Gray(255));
//            imG._Not();

////
//            var imG = image.Convert<Gray, byte>();

//            imG._ThresholdBinary(new Gray(254), new Gray(255));
//            x = x.Copy(imG);

//            image = image.AbsDiff(new Gray(128));
//            image._ThresholdBinaryInv(new Gray(0), new Gray(255));
//            image = x.AbsDiff(new Gray(128)).Copy(image);
//            image._ThresholdBinary(new Gray(0), new Gray(255));

//            imG._MorphologyEx(new StructuringElementEx(size, size, center, center, CV_ELEMENT_SHAPE.CV_SHAPE_ELLIPSE), CV_MORPH_OP.CV_MOP_BLACKHAT, 500);
                var dX = imG.Sobel(0, 1, 5);
                var dY = imG.Sobel(1, 0, 5);
                imG = dX.Pow(2).Add(dY.Pow(2)).Pow(0.5).Convert<Gray, byte>();

                var imageE = newImage.Convert<Rgb, byte>();
                var resImage = baseImage.ConcateHorizontal(imageE);
                resImage.Draw(
                    new LineSegment2D(new Point(baseImage.Width, 0), new Point(baseImage.Width, baseImage.Height)),
                    new Rgb(Color.Black), 3);
//            ImageViewer.Show(resImage.Resize(1600.0 / resImage.Width, INTER.CV_INTER_CUBIC));
//            ImageViewer.Show(resImage);
                newImage.Save(imageToPath);

            }
        }

        [Test]
        [Explicit]
        public void PlotKs()
        {
            // ���������� � SmoothTheContourTest
            var imagePath = Path.Combine(Database.Samples, "s1.jpg"/*SampleImage.MapleMiniHalftoned*/);
            var image = new Image<Bgr, byte>(new Bitmap(imagePath));
            var img = image.Convert<Gray, byte>();
            int sigma = 10;


            var contourFinder = new ContourFinder(img);
            contourFinder.ChooseTheSecondContour();
            var contourPoints = contourFinder.ChosedContourPoints();
            var lenContour = contourPoints.Length;
            var ksize = lenContour.CloseOdd();
            Console.WriteLine(lenContour);

            CurvatureOfClosedLine c = new CurvatureOfClosedLine(contourPoints, ksize);

            var ks = c.GetCurveMeasureInSmoothedContour(sigma);
            var vecs = ks.Select((d, i) => new double[] {i, d}).ToArray();
            var labels = Enumerable.Range(0, ks.Length).Select(i => LeafType.TypeA).ToArray();
            //plot ks
            var plottingTests = new Plotting_Tests();
            plottingTests.PlotVectors(vecs, labels, "u", "curvature", labels);
                        File.WriteAllLines("1.txt", vecs.Select(ds => 
                            String.Join(",", ds.Select(d => d.ToString(CultureInfo.InvariantCulture)))));
        }

        [Test]
        [Explicit]
        public void UseFilter()
        {
            var fname =
                @"C:\Users\Kris\Desktop\LeafRecognition\leaf-recognition\src\Tests\Data\LearningBase\TrainingSet\DSC_0095.TypeK.jpg";
            var foutname = Path.Combine(Database.Samples, SampleImage.MapleMiniHalftoned);
            new RgbImage(new Image<Rgb, byte>(fname)).Apply(LeafShapeGetter.Instance).Image.Save(foutname);

        }
    }
}

