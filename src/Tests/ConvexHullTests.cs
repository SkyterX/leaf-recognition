﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using LeafRecognition.Geometry;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class ConvexHullTests
    {
        [Test]
        public void SimpleTest()
        {
            var polygon = new Polygon2D(new List<Point2D>
            {
                new Point2D(0, 0),
                new Point2D(2, 0),
                new Point2D(1, 1),
                new Point2D(2, 2),
                new Point2D(0, 2)
            });
            List<Point2D> convexHull = polygon.GetConvexHull().ToList();
            convexHull.Sort();

            List<Point2D> answer = new Polygon2D(new List<Point2D>
            {
                new Point2D(0, 0),
                new Point2D(2, 0),
                new Point2D(2, 2),
                new Point2D(0, 2)
            }).ToList();
            answer.Sort();

            Assert.AreEqual(convexHull, answer);
        }

        [Test]
        public void SimpleTest2()
        {
            var polygon = new Polygon2D(new List<Point2D>
            {
                new Point2D(0, 0),
                new Point2D(5, 0),
                new Point2D(0, 1),
                new Point2D(5, 5),
                new Point2D(0, 5)
            });
            List<Point2D> convexHull = polygon.GetConvexHull().ToList();
            convexHull.Sort();

            List<Point2D> answer = new Polygon2D(new List<Point2D>
            {
                new Point2D(0, 0),
                new Point2D(5, 0),
                new Point2D(5, 5),
                new Point2D(0, 5)
            }).ToList();
            answer.Sort();

            Assert.AreEqual(convexHull, answer);
        }

        [Test]
        public void SpeedTest()
        {
            const int maxX = 5000;
            const int maxY = 5000;
            const int n = 100000;

            #region create random points in the range of [0, maxValue]

            var random = new Random((int) (DateTime.Now.Ticks & 0x0000ffff));
            var points = new Point2D[n];
            for (int i = 0; i < n; ++i)
            {
                points[i] = new Point2D(random.Next(maxX), random.Next(maxY));
            }

            #endregion

            var stopwatch = new Stopwatch();
            stopwatch.Start();
            Console.WriteLine("Convex hull area: {0}", new Polygon2D(points).GetConvexHull().Area());
            double time = new TimeSpan(stopwatch.ElapsedTicks).TotalSeconds;
            Console.WriteLine("Ellapsed time: {0}", time);
            Assert.IsTrue(time < 1.0);
        }
    }
}