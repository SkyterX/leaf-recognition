﻿using System;
using System.IO;
using System.Linq;
using LeafRecognition.Geometry;
using NUnit.Framework;
using Tests.Tools;

namespace Tests
{
	[TestFixture]
	public class ImageGeometryExtensinonTests
	{
		[Test]
		public void Test()
		{
			var filename = Path.Combine(Database.Samples, SampleImage.SmallBitmap);
			var image = LazyImage.Loader.Load(filename);

			var pointList = image.ToPoint2DSet().ToList();
			Console.WriteLine(new Polygon2D(pointList).GetConvexHull().Area());
			Assert.AreEqual(pointList.Count, 4);
		}
	}
}