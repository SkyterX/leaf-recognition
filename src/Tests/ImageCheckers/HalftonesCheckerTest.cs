﻿using System;
using System.IO;
using LeafRecognition.Checkers;
using NUnit.Framework;
using Tests.Tools;

namespace Tests.ImageCheckers
{
	[TestFixture]
	public class HalftonesCheckerTest
	{
		private static readonly object[] TestCases =
		{
			new object[] {SampleImage.NonHalftones, false},
			new object[] {SampleImage.Halftones, true}
		};

		[Test]
		[TestCaseSource("TestCases")]
		public void TestCorrectness(string imageName, bool isHalftones)
		{
			Console.Out.WriteLine("Loading...");
			var imagePath = Path.Combine(Database.Samples, imageName);
			var loading = Measure.Time(() => LazyImage.Loader.Load(imagePath));
			var image = loading.Result;
			Console.Out.WriteLine("Loading time : {0}", loading.TimeElapsed.ToSeconds());

			Console.Out.WriteLine("Processing...");
			var tr = Measure.Time(() => HalftonesChecker.Instance.Check(image));
			Console.Out.WriteLine("Processing time : {0}", tr.TimeElapsed.ToSeconds());

			Assert.That(tr.Result, Is.EqualTo(isHalftones));
		}
	}
}