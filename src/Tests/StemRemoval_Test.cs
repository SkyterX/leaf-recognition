using System;
using System.Drawing;
using System.IO;
using System.Linq;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using LeafRecognition.Filters;
using LeafRecognition.Geometry;
using LeafRecognition.Images;
using NUnit.Framework;
using Tests.Tools;

namespace Tests
{
	[TestFixture]
	public class StemRemoval_Test
	{
		[Test]
		[Explicit]
		public void StemRemoval_TrainingSet()
		{
//			var basePath = LearningBase.TrainingSet;
			var basePath = CLEFBase.TrainingSet;
			var files = Directory.EnumerateFiles(basePath, "*" + FileExtensions.Image)
				.Take(200)
				.ToArray();
			var outDir = Path.Combine(basePath, "Stem");
			Directory.CreateDirectory(outDir);
			var time = Measure.Time(() =>
			{
				foreach (var file in files)
				{
					var fileName = Path.GetFileName(file);
					var imageFromPath = Path.Combine(basePath, fileName);
					var imageToPath = Path.Combine(outDir, fileName);
					var image = new Image<Rgb, byte>(imageFromPath);
					ProcessImage(image).Save(imageToPath);
				}
			});
			Console.Out.WriteLine("Time elapsed : {0}", time.ToSeconds());
			Console.Out.WriteLine("Average time : {0}", new TimeSpan(time.Ticks/files.Length).ToMilliseconds());
		}

		[Test]
		public void StemRemoval_Single()
		{
//						var imageFile = "DSC_0035.TypeC.jpg";
//						var imageFile = "DSC_0071.TypeI.jpg";
//						var imageFile = "DSC_0040.TypeE.jpg";
//						var imageFile = "DSC_0099.TypeK.jpg";
//						var imageFile = "DSC_0065.TypeH.jpg";
//						var imageFile = "DSC_0095.TypeK.jpg";
//						var imageFile = "DSC_0116.TypeM.jpg";
						var imageFile = "DSC_0087.TypeN.jpg";
			var imagePath = Path.Combine(LearningBase.TrainingSet, imageFile);

			var baseImage = new Image<Rgb, byte>(imagePath);
//						baseImage = baseImage.Resize(500.0/baseImage.Width, INTER.CV_INTER_LANCZOS4);
			var res = Measure.Time(() => ProcessImage(baseImage));
			Console.Out.WriteLine("Time elapsed : {0}", res.TimeElapsed.ToMilliseconds());
			var imageE = res.Result;
			var resImage = baseImage.ConcateHorizontal(imageE);
			resImage.Draw(
				new LineSegment2D(new Point(baseImage.Width, 0), new Point(baseImage.Width, baseImage.Height)),
				new Rgb(Color.Black), 3);
			ImageViewer.Show(resImage.Resize(1600.0/resImage.Width, INTER.CV_INTER_LANCZOS4));
//			ImageViewer.Show(resImage);
		}

		private static Image<Rgb, byte> ProcessImage(Image<Rgb, byte> baseImage)
		{
			var rgbImage = baseImage.ToRgbImage();
			rgbImage = rgbImage.Apply(LeafShapeGetter.Instance);
			var unstemmed = rgbImage.Apply(StemRemoval.Instance);
			var diff = rgbImage.Image.Xor(unstemmed.Image);
			rgbImage.Image.SetValue(new Rgb(Color.Gray), diff.Convert<Gray, byte>());
			return rgbImage.Image;
		}

		private static void DrawEllipse(RgbImage rgbImage)
		{
			var border = rgbImage.Apply(SimpleEdgeDetection.Default).ToPoint2DSet();
			var hull = new Polygon2D(border).GetConvexHull()
				.Select(pt => new Point(pt.X, pt.Y))
				.ToArray();

			var cvPoints = new Matrix<int>(hull.Length, 2);
			for (var i = 0; i < hull.Length; i++)
			{
				var point = hull[i];
				cvPoints[i, 0] = point.X;
				cvPoints[i, 1] = point.Y;
			}

			var ellipse = CvInvoke.cvFitEllipse2(cvPoints.Ptr);
			ellipse.angle += 90;
			rgbImage.Image.Draw(new Ellipse(ellipse), new Rgb(Color.Gold), 3);
			var c = ellipse.center;
			var ang = ellipse.angle*Math.PI/180.0;
			rgbImage.Image.Draw(
				new LineSegment2DF(c, new PointF((float) (c.X + Math.Cos(ang)*100), (float) (c.Y + Math.Sin(ang)*100))),
				new Rgb(Color.Red), 3);
		}

		private static void DrawConvexHull(RgbImage rgbImage)
		{
			var leafBorder = LeafBorderGetter.Instance.Apply(rgbImage).ToPoint2DSet();
			var convexHull = new Polygon2D(leafBorder)
				.GetConvexHull()
				.Select(pt => new Point(pt.X, pt.Y))
				.ToArray();
			rgbImage.Image.DrawPolyline(convexHull, true, new Rgb(Color.Red), 5);
		}
	}
}