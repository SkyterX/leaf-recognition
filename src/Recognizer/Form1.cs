﻿using System;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.Structure;
using LeafRecognition.Images;
using LeafRecognition.Recognition;
using LeafRecognition.Util;

namespace Recognizer
{
    public partial class Form1 : Form
    {
        private ImageRecognizer recognizer;
        private RgbImage img;
        private string[] labels_clef;
        
        public Form1()
        {
            InitializeComponent();
            labels_clef = File.ReadAllLines("labels.txt");
            LoadClassifier("clef_recognizer.rcz");
        }

        private void splitContainer1_Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void browseButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.Filter = "Image Files|*.BMP;*.GIF;*.JPG;*.JPEG;*.PNG|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                var imageLocation = openFileDialog1.FileName;
                imageText.Text = imageLocation;
                imageText.Visible = true;
                pictureBox1.ImageLocation = imageLocation;
                var opencvImg = new Image<Rgb, byte>(imageLocation);
                img = new RgbImage(opencvImg);
                recognizeButton_Click(null, null);
            }
        }

        private void browseClassifierButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.Filter = "Recognizers|*.rcz|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                LoadClassifier(openFileDialog1.FileName);
            }
        }

        private void LoadClassifier(String fileName)
        {
            classifierText.Text = fileName;
            classifierText.Visible = true;
            recognizer = Serializer.Deserialize<ImageRecognizer>(fileName);
        }

        private void recognizeButton_Click(object sender, EventArgs e)
        {
            if (recognizer == null)
                return;
            if (imageText.Visible == false)
                return;
            var res = recognizer.Recognize(img);
            var topres = res.Confidence
                .Select((conf, i) => new{Ind = i, Conf = conf})
                .OrderByDescending(c => c.Conf)
                .Where(c => c.Conf > 0.05)
                .Take(5)
                .ToArray();
            resulstsTextBox.Text = String.Join("\r\n",
                topres.Select(c =>
                    String.Format("{0:00.0%} {1}", c.Conf, labels_clef[c.Ind])));
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
