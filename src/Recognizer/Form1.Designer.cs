﻿namespace Recognizer
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.resulstsTextBox = new System.Windows.Forms.TextBox();
            this.recognizeButton = new System.Windows.Forms.Button();
            this.classifierText = new System.Windows.Forms.Label();
            this.browseClassifierButton = new System.Windows.Forms.Button();
            this.imageText = new System.Windows.Forms.Label();
            this.browseImageButton = new System.Windows.Forms.Button();
            this.imageLabel = new System.Windows.Forms.Label();
            this.classifierLabel = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.resulstsTextBox);
            this.splitContainer1.Panel1.Controls.Add(this.recognizeButton);
            this.splitContainer1.Panel1.Controls.Add(this.classifierText);
            this.splitContainer1.Panel1.Controls.Add(this.browseClassifierButton);
            this.splitContainer1.Panel1.Controls.Add(this.imageText);
            this.splitContainer1.Panel1.Controls.Add(this.browseImageButton);
            this.splitContainer1.Panel1.Controls.Add(this.imageLabel);
            this.splitContainer1.Panel1.Controls.Add(this.classifierLabel);
            this.splitContainer1.Panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.splitContainer1_Panel1_Paint);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.pictureBox1);
            this.splitContainer1.Size = new System.Drawing.Size(872, 505);
            this.splitContainer1.SplitterDistance = 273;
            this.splitContainer1.TabIndex = 0;
            this.splitContainer1.TabStop = false;
            // 
            // resulstsTextBox
            // 
            this.resulstsTextBox.Location = new System.Drawing.Point(15, 260);
            this.resulstsTextBox.Multiline = true;
            this.resulstsTextBox.Name = "resulstsTextBox";
            this.resulstsTextBox.Size = new System.Drawing.Size(238, 236);
            this.resulstsTextBox.TabIndex = 8;
            // 
            // recognizeButton
            // 
            this.recognizeButton.Location = new System.Drawing.Point(15, 231);
            this.recognizeButton.Name = "recognizeButton";
            this.recognizeButton.Size = new System.Drawing.Size(75, 23);
            this.recognizeButton.TabIndex = 7;
            this.recognizeButton.Text = "Recognize";
            this.recognizeButton.UseVisualStyleBackColor = true;
            this.recognizeButton.Visible = false;
            this.recognizeButton.Click += new System.EventHandler(this.recognizeButton_Click);
            // 
            // classifierText
            // 
            this.classifierText.Location = new System.Drawing.Point(12, 40);
            this.classifierText.Name = "classifierText";
            this.classifierText.Size = new System.Drawing.Size(241, 73);
            this.classifierText.TabIndex = 6;
            this.classifierText.Text = "label1";
            this.classifierText.Visible = false;
            // 
            // browseClassifierButton
            // 
            this.browseClassifierButton.Enabled = false;
            this.browseClassifierButton.Location = new System.Drawing.Point(139, 9);
            this.browseClassifierButton.Name = "browseClassifierButton";
            this.browseClassifierButton.Size = new System.Drawing.Size(75, 23);
            this.browseClassifierButton.TabIndex = 5;
            this.browseClassifierButton.Text = "Browse";
            this.browseClassifierButton.UseVisualStyleBackColor = true;
            this.browseClassifierButton.Visible = false;
            this.browseClassifierButton.Click += new System.EventHandler(this.browseClassifierButton_Click);
            // 
            // imageText
            // 
            this.imageText.Location = new System.Drawing.Point(12, 165);
            this.imageText.Name = "imageText";
            this.imageText.Size = new System.Drawing.Size(241, 53);
            this.imageText.TabIndex = 4;
            this.imageText.Text = "label1";
            this.imageText.Visible = false;
            // 
            // browseImageButton
            // 
            this.browseImageButton.Location = new System.Drawing.Point(139, 130);
            this.browseImageButton.Name = "browseImageButton";
            this.browseImageButton.Size = new System.Drawing.Size(75, 23);
            this.browseImageButton.TabIndex = 3;
            this.browseImageButton.Text = "Browse";
            this.browseImageButton.UseVisualStyleBackColor = true;
            this.browseImageButton.Click += new System.EventHandler(this.browseButton_Click);
            // 
            // imageLabel
            // 
            this.imageLabel.AutoSize = true;
            this.imageLabel.Location = new System.Drawing.Point(12, 135);
            this.imageLabel.Name = "imageLabel";
            this.imageLabel.Size = new System.Drawing.Size(97, 13);
            this.imageLabel.TabIndex = 2;
            this.imageLabel.Text = "Image to recognize";
            // 
            // classifierLabel
            // 
            this.classifierLabel.AutoSize = true;
            this.classifierLabel.Location = new System.Drawing.Point(12, 14);
            this.classifierLabel.Name = "classifierLabel";
            this.classifierLabel.Size = new System.Drawing.Size(48, 13);
            this.classifierLabel.TabIndex = 0;
            this.classifierLabel.Text = "Classifier";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(591, 501);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(872, 505);
            this.Controls.Add(this.splitContainer1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label classifierLabel;
        private System.Windows.Forms.Label imageLabel;
        private System.Windows.Forms.Button browseImageButton;
        private System.Windows.Forms.Label imageText;
        private System.Windows.Forms.Label classifierText;
        private System.Windows.Forms.Button browseClassifierButton;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button recognizeButton;
        private System.Windows.Forms.TextBox resulstsTextBox;
    }
}

